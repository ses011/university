import pymysql

conn = pymysql.connect(host='localhost',
                       user='egor',
                       password='12345',
                       database='social_network',
                       cursorclass=pymysql.cursors.DictCursor)

# prepare a cursor object using cursor() method
cursor = conn.cursor()

# execute SQL query using execute() method.
cursor.execute("SELECT VERSION()")

# Fetch a single row using fetchone() method.
data = cursor.fetchone()
print ("Database version : %s " % data)

# disconnect from server
conn.close()