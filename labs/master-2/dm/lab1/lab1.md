## Лабораторная работа 1 (задания 1,2,3).
### Спиридонов Егор, М24-524.

## Теоретическая информация

1. Операция аппликации ассоциативна слева, атомы применяются слева направо: M N P Q~ (M N P) Q ~ ((M N) P) Q
2. Редукция - процесс преобразования выражения, в котором:
    * Существуют аксиомы: ```(I): I x -> x```, ```(K): K x y -> x```, ```(S): S x y z -> xz (yz)```;
    * Используются правила вывода: ```Дано: P->P', P->Q, Q->R``` ```Правила вывода: RP->RP'; PR -> P'R; P->R```
    * ```I x, K x y, S x y z``` - редексы;
    * ```x, xz (yz)``` - конракты;
3. Процесс прохода аксиом слева направо согласно правилам вывода называется процессом редукции.
4. Если терм N не содержит редексов, то говорят, что этот терм находится в нормальной форме.
5. Символ ```~``` в текущей работе обозначает эквивалентность. 

Комбинаторные характеристики:

1. ```I x = x```
2. ```S x y z = xz (yz)```
3. ```K x y = x```
4. ```C x y z = x z y```
5. ```B f g x = f (gx)```
6. ```W x y = x y y```


## Задание 1

Восстановить пропущенные скобки. Пропущенные скобки восстанавливаются слева направо.

### Решение

```SK(KS)a ~ (SK(KS))a ~((SK)(KS))a```

```S(KS)Kabc ~ ((S(KS)K)a)b)c ~ (((S(KS))K)a)b)c```

```S(BB)S(Ka)b ~ ((S(BB)S)(Ka))b ~ (((S(BB))S)(Ka))b```

```K(KO)a(bO) ~ (K(KO)a)(bO) ~ ((K(KO))a)(bO)```

```Da(bc)K ~ (Da(bc))K ~ ((Da)(bc))K```

```KIa(bz) ~ (KIa)(bz) ~ ((KI)a)(bz)```

```lxy.xy(KI) ~ lxy.(xy(KI))```

```lxy.xyvu ~ lx.(ly.(xyvu)) ~ lx.(ly.(((xy)v)u))```

### Проверка

![lol kek](./images/proof1.png)

## Задание 2

Выполнить 1 шаг редукции.

### Решение

```W(mp)(mk) -> (mp)(mk)(mk)``` - по определению комбинатора W.

```KIa(bz) -> I(bz)``` - применение комбинатора K к аргументам I и a.

```C(BCa)bcd -> (BCa)cbd``` - применяется самый левый комбинатор (C) с аргументами (BCa) b c.

```S(BB)Kab -> (BB)a(Ka)b``` - применяется комбинатор S с аргументами (BB) K a.

```a(Ibc) -> a(bc)``` - по определению I.

```Bab(cx) -> a(b(cx))``` - по определению B.

### Проверка

![lol kek](./images/proof2.png)

![lol kek](./images/proof3.png)

![lol kek](./images/proof4.png)

## Задание 3

Выполнить полную редукцию выражения.

### Решение

```S(KS)Kxyz -> (KS)x(Kx)yz -> S(Kx)yz -> (Kx)y(yz) -> x(yz)``` - применение комбинаторов: самый левый S -> самый левый K -> S -> K.

```S(BBS)(KK)xyz -> (BBS)x(KKx)yz -> B(Sx)(KKx)yz -> (Sx)((KKx)y)z ~ Sx(KKxy)z -> xz(KKxyz) -> xz(Kyz) -> xzy```.

```SS(CK)xy -> Sx(CKx)y -> xy(CKxy) -> xy(Kyx) -> xyy```.

```SS(K(SKK))xy -> Sx(K(SKK)x)y -> xy(K(SKK)xy) -> xy((SKK)y) ~ xy(SKKy) -> xy(Ky(Ky)) -> xyy```.

```CSIxy -> SxIy -> xy(Iy) -> xyy```.

```BBBxyzu -> B(Bx)yzu -> (Bx)(yz)u ~ Bx(yz)u -> x((yz)u) ~ x(yzu)```.

```BC(BC)xyzu -> C(BCx)yzu -> (BCx)zyu ~ BCxzyu -> C(xz)yu -> xzuy```.


```BBBCCxyzu ~ B (B) (B) (C) Cxyzu -> B(BC)Cxyzu ~ B (BC) (C) (x) yzu -> (BC)(Cx)yzu ~ B (C) (Cx) (y) zu -> C((Cx)y)zu ~ C (Cxy) (z) (u) ->  Cxyuz -> xuyz```.

```BB(BB)xyzyu ~ B (B) (BB) (x) yzyu -> B(BBx)yzyu ~ B (BBx) (y) (z) yu -> BBx(yz)yu ~ B (B) (x) (yz) yu -> B(x(yz))yu ~ B (x(yz)) (y) (u) -> x(yz)(yu)```.

```B(BS)Bxyzu ~ B (BS) (B) (x) yzu -> BS(Bx)yzu ~ B (S) (Bx) (y) zu -> S(Bxy)zu ~ S (Bxy) (z) (u) -> Bxyu(zu) ~ B (x) (y) (u) (zu) -> x(yu)(zu)```.

### Проверка

![lol kek](./images/proof5.png)
![lol kek](./images/proof6.png)
![lol kek](./images/proof7.png)
![lol kek](./images/proof8.png)
![lol kek](./images/proof9.png)

## Выводы

1. Запомнил, что операция аппликации лево-ассоциативна.
2. Научился выполнять редукцию для термов комбинаторной логики. В рамках лабораторной работы каждый из термов из задания 3 удалось редуцировать к нормальной форме. 
3. Запомнил определения комбинаторов ```I, S, K, W, C, B```.