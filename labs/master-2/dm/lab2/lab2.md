## Лабораторная работа 2 (задания 4,5).
### Спиридонов Егор, М24-524.

## Теоретическая информация

Вхождение переменной x в объект вида ```lx.M``` считаеся связанным, все прочие вхождения свободными. FV(M) - множество свободных переменных терма M. BV(M) - множество связанных переменных терма M.

Связь комбинаторной логики и l-исчислением задается на основе принятия определения абстракции в комбинаторной логике:

Абстракция объекта M по переменной x ```[x]M``` определяется индуктивно, по построению M:
1. ```[x]x =(экв) I```,
2. ```[x]c =(экв) Kc```,
3. ```[x](MN) =(экв) S([x]M)([x]N)```, где M, N, x, c - атомы, x != c. 

Таким образом, l-терм можнно представить посредством комбинаторного терма. Базисным является набор комбинаторов, необходимый для представления произвольного l-терма. Существует 2 основных базис: {I, K, S}, {I, B, S, C}.

Правила разложения в базисе I, K, S:

1. ```lx.x = I```.
2. ```lx.M = KM``` x ∉ FV(M)
3. ```lx.M N = S (KM) (KN)```

Правила разлежения в базисе I, B, S, C:

1. ```lx.x = I```
2. ```lx. P Q = B P (lx.Q)```, x ∉ FV(P), (x точно нет в P, вероятно есть в Q)
3. ```lx. P Q = C (lx.P) Q```, x ∉ FV(Q), (x вероятно есть в P, точно нет в Q)
4. ```lx P Q = S (lx.P) (lx.Q)```

## Задание 4

### Решение

```lf.fx -> S (lf.f) (lf.x) -> S (lf.f) (Kx) -> S I (Kx)```

```lx.fx -> S (lx.f) (lx.x) -> S (lx.f) I -> S (Kf) I```

```lfx.f ~ lf.(lx.f) ~ lf.(Kf) ~ S (lf.K) (lf.f) ~ S (lf.K) I ~ S (KK) I```

```lfx.x ~ lf.(lx.x) ~ lf.I ~ KI```

```
lfx.fx ~ lf.(lx.(fx)) ~ lf.(S (lx.f) (lx.x)) ~ lf.(S (lx.f) I) ~ lf.(S(Kf)I) ~
~ lf.(S(Kf)) (I) -> S (lf.S(Kf)) (lf.I) ~ S (lf.S(Kf)) (KI) ~ S (S (lf.S) (lf.Kf)) (KI) ~
~ S (S (lf.S) (S (lf.K) (lf.f) )) (KI) ~ S (S (lf.S) (S (lf.K) I )) (KI) ~
~ S (S (lf.S) (S (KK) I )) (KI) ~ S (S KS (S (KK) I )) (KI)
```

```
lfx.xf ~ lf.(lx.(xf)) ~ lf.(S (lx.x) (lx.f)) ~ lf.(S (lx.x) (Kf)) ~
~ lf.SI(Kf) ~ lf.(SI) (Kf) ~ S (lf.SI) (lf.Kf) ~ S (lf.SI) (S (lf.K) (lf.f)) ~
~ S (lf.SI) (S (lf.K) I) ~ S (lf.SI) (S (KK) I) ~ S (K(SI)) (S(KK)I)
```

### Проверка

![lolkek](./images/proof1.png)
![lolkek](./images/proof2.png)
![lolkek](./images/proof3.png)
![lolkek](./images/proof4.png)
![lolkek](./images/proof5.png)
![lolkek](./images/proof6.png)

## Задание 5

### Решение 

```lf.fx ~ C (lf.f) x ~ C I x```

```lx.fx ~ B f (lx.x) ~ B f I```

```lxy.xy ~ lx.(ly.xy) ~ lx.(B x (ly.y)) ~ lx.B x I ~ lx.(Bx)I ~ C (lx.Bx) I ~ C (B B (lx.x)) I ~ C (B B I) I```

```lxy.yx ~ lx.(ly.yx) ~ lx.(C(ly.y)x) ~ lx.CIx ~ lx.(CI)x ~ B (CI) (lx.x) ~ B (CI) I```

```
lfx.fxx ~ lf.(lx.fxx) ~ lf.(lx.(fx)x) ~ lf.(S (lx.fx) (lx.x)) ~ lf.(S (lx.fx) I) ~
~ lf.(S (B f (lx.x)) I) ~ lf.S (B f I) I ~ lf.(S (B f I)) I ~ C (lf.S(BfI)) I ~
~ C (B S (lf.BfI)) I ~ C (B S (lf.(Bf) I)) I ~ C (B S (C (lf.Bf) I)) I ~
~ C (B S (C (B B (lf.f)) I)) I ~ C (B S (C (B B I) I)) I
```

```
lfx.f(fx) ~ lf.(lx.f(fx)) ~ lf.(B f lx.fx) ~ lf.(B f (B f lx.x))) ~ lf.Bf(BfI) ~
~ lf.(Bf)(BfI) ~ S (lf.Bf) (lf.BfI) ~ S (lf.Bf) (C (lf.Bf) I) ~ S (lf.Bf) (C (B B (lf.f)) I) ~ 
~ S (lf.Bf) (C (B B I) I) ~ S (B B lf.f) (C (B B I) I) ~ S (B B I) (C (B B I) I)
```

```
lxyr.rxy ~ lx.(ly.(lr.rxy)) ~ lx.(ly.(C (lr.rx) y)) ~ lx.(ly.(C (C lr.r x) y)) ~ lx.(ly.(C (C I x) y)) ~
~ lx.(ly.C(CIx)y) ~ lx.(B (C(CIx)) ly.y) ~ lx.B(C(CIx))I ~ C (lx.B(C(CIx))) I ~ C (B B (lx.C(CIx))) I ~
~ C (B B (B C lx.CIx)) I ~ C (B B (B C (B (CI) lx.x))) I ~ C (B B (B C (B (CI) I))) I
```

```
lxyz.x(yz) ~ lx.(ly.(lz.x(yz))) ~ lx.(ly.(B x (lz.yz))) ~ lx.(ly.(B x (B y lz.z))) ~ lx.(ly.(B x (B y I))) ~
~ lx.( B (B x) (ly.B y I) ) ~ lx.( B (B x) (ly.(B y) I) ) ~ lx.( B (B x) (C (ly.By) I) ) ~
~ lx.( B (B x) (C (B B (ly.y)) I) ) ~ lx.B (B x) (C (B B I) I) ~ lx.(B (B x)) (C (B B I) I) ~ C (lx.B(Bx)) (C (B B I) I) ~
~ C (B B (lx.Bx)) (C (B B I) I) ~ C (B B (B B lx.x)) (C (B B I) I) ~ C (B B (B B I)) (C (B B I) I)
```

```
lxyz.xzy ~ lx.(ly.(lz.xzy)) ~ lx.(ly.(lz.(xz)y)) ~ lx.(ly.(lz.(xz)y)) ~ lx.(ly.(C (lz.xz) y)) ~ 
~ lx.(ly.(C (B x lz.z) y)) ~ lx.(ly.(C (B x I) y)) ~ lx.(ly.(C (B x I)) y) ~ lx.( B (C (B x I)) ly.y ) ~
~ lx.( B (C (B x I)) I ) ~ lx.(B (C (B x I))) I ~ C (lx.B (C (B x I))) I ~ C (B B lx.C (B x I)) I ~ 
~ C (B B (B C lx.BxI)) I ~ C (B B (B C (C (lx.Bx) I))) I ~ C (B B (B C (C (B B lx.x) I))) I ~ 
~ C (B B (B C (C (B B I) I))) I
```

```
lxyz.yxz ~ lx.(ly.(lz.yxz)) ~ lx.(ly.(lz.(yx)z)) ~ lx.(ly.( B (yx) lz.z )) ~ lx.(ly.( B (yx) I )) ~
~ lx.(ly.(B (yx)) I) ~ lx.( C (ly.B (yx)) I ) ~ lx.( C (ly.B (yx)) I ) ~ lx.( C (B B ly.(yx)) I ) ~
~ lx.( C (B B (C ly.y x)) I ) ~ lx.( C (B B (C I x)) I ) ~ C (lx.C (B B (C I x))) I ~ 
~ C (B C lx.(B B (C I x))) I ~ C (B C lx.((B B) (C I x))) I ~ C (B C (B (B B) (lx.CIx))) I ~ 
~ C (B C (B (B B) (B (CI) lx.x)))) I ~ C (B C (B (B B) (B (CI) I))) I
```

### Проверка 

![lolkek](./images/proof7.png)
![lolkek](./images/proof8.png)
![lolkek](./images/proof9.png)
![lolkek](./images/proof10.png)
![lolkek](./images/proof11.png)
![lolkek](./images/proof12.png)
![lolkek](./images/proof13.png)
![lolkek](./images/proof14.png)
![lolkek](./images/proof15.png)
![lolkek](./images/proof16.png)

## Выводы

1. Произвольный l-терм можно представить комбинаторным термом;
2. Базис в контексте работы - необходимый (не обязательно минимальный) набор комбинаторов, через который можно выразить произвольный l-терм.
3. Комбинатор I выражается через K и S -> базисом также является {K, S} (на занятии обсуждали, что существует базис из вообще 1 комбинатора).
4. Существует конкретный алгоритм преобразования l-терма в комбинаторный терм, от выбранного базиса зависит количество итераций/операций в разложении. 
5. Мне больше не хочется выполнять подобные задачи вручную.