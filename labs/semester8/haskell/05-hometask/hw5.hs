{-# LANGUAGE EmptyDataDecls #-}

module HW5 where

import Data.Functor
import Text.Read
import Control.Applicative

data NeedDefineTypeWith a
data NeedDefineType

----------------------------------------------------------------------------------
-- Упр 1. Приведите примеры функций (с реализацией), который имеют следующие типы

mul3 :: Float -> Float
mul3 x = 3 * x

repeat' :: Int -> String
repeat' x
  | x == 0 = ""
  | otherwise = "e" ++ (repeat' (x-1))

repeatString :: Int -> String -> [String]
repeatString 0 _ = []
repeatString x s 
  | x == 1 = [s]
  | otherwise = s : repeatString (x-1) s

f1 :: (Float -> Float) -> Float
f1 = \f -> f 4 
-- f1 mul3 = 12.0; f1 принимает функцию, возвращающую Float и модифицирует его.

f2 :: Float -> (Float -> Float)
f2 = \x -> (x+)
-- f2 3 6 = 9.0; f2 возвращает функцию, прибавляющую x к следующему аргументу.

f3 :: (Float -> Float) -> (Float -> Float)
f3 = \f x -> f (f x)
-- f3 mul3 5 = 45.0;

f4 :: a -> (a -> b) -> b
f4 = \x f -> f x
-- f4 "abcdef" length = 6; применяет функцию length к переданной строке.

f5 :: a -> b -> b
f5 = \x y -> y
-- f5 "asdasd" 5 = 5; возвращает второй аргумент.

f6 :: (a -> a -> b) -> a -> b
f6 = \f x -> f x x

f7 :: (a -> b) -> (b -> c) -> (a -> c)
f7 = \f g -> g . f
-- f7 length repeat' "asd" = "eee"; определяет длину переданной строки, затем пишет букву е столько же раз, какая длина у строки была изначально

f8 :: (a -> b -> c) -> (a -> b) -> (a -> c)
f8 = \f g x -> f x (g x)
-- f8 repeatString repeat' 2 = ["ee","ee"]; создает список, в котором x строк длины x, состоящих из буквы е.

f9 :: (a -> b -> b) -> b -> [a] -> b
f9 _ x [] = x
f9 f x (y:ys) = f y (f9 f x ys)

f10 :: (b -> a -> b) -> b -> [a] -> b
f10 _ x [] = x
f10 f x (y:ys) = f10 f (f x y) ys

----------------------------------------------------------------------------------
-- Упр 2. Реализуйте функции: 
--  a) curry'   - делает из некаррированной функции каррированную.
--  b) uncurry’ - делает из каррированной функции некаррированную.
--                Используя свёртки определите следующие функции

curry' :: ((a, b) -> c) -> (a -> b -> c)
curry' f = \x y -> f (x, y)

uncurry' :: (a -> b -> c) -> ((a, b) -> c)
uncurry' f = \(x,y) -> f x y

----------------------------------------------------------------------------------
-- Упр 3. Используя свёртки определите функции: 

concat' :: [[a]] -> [a]
concat' = foldr (++) []
inits' :: [a] -> [[a]] -- список начал списка
inits' = foldr (\x y -> [] : (map (x:) y)) [[]]
tails' :: [a] -> [[a]] -- список хвостов списка
tails' = foldr (\x y -> (x : (head y)) : y) [[]]

----------------------------------------------------------------------------------
-- Упр 4. Определите алгебраический тип данных Set a, который определяет 
-- множество элементов типа a. 
--
-- Напоминание:
--    Множество – это совокупность объектов, хорошо различимых нашей интуицией и 
--                мыслимых как единое целое. 
--    Следствие: Любой элемент может входить в множество только один раз!
--
-- Определите функцию subset, которая проверяет, что все элементы первого 
-- множества также являются элементами второго множества.
--
-- Используя функцию subset определите экземпляр класса Eq для типа Set a

data Set a = EmptySet | Set [a]

add :: Eq a => a -> Set a -> Set a
add x EmptySet = Set [x]
add x (Set s)
  | contains x (Set s) = (Set s)
  | otherwise = Set (x:s)

contains :: Eq a => a -> Set a -> Bool
contains a EmptySet = False
contains a (Set (s:ss))
  | a == s = True
  | otherwise = contains a (Set ss) 

subset :: Eq a => Set a -> Set a -> Bool
subset EmptySet _ = True
subset (Set (x:xs)) set = (contains x set) && (subset (Set xs) set)

-- это для следуюшего номера
subsets :: [a] -> [[a]]
subsets [] = [[]]
subsets (x:xs) = subsets xs ++ map (x:) (subsets xs)

instance Eq a => Eq (Set a) where
  (==) (Set x) (Set y) = subset (Set x) (Set y) && subset (Set y) (Set x)

----------------------------------------------------------------------------------
-- Упр 5. Определите класс типов Finite, который имеет только один метод: 
--          получение списка всех элементов заданного типа. Идея в том, чтобы 
--          такой список был конечным.
--
--        Определите экземпляры класса типов Finite для следующих типов:
--        • Bool
--        • (a, b) для конечных типов a и b
--        • Set a (из предыдущего упражнения), где a – конечный тип.
--        • a -> b, для всяких конечных типов a и b, где тип a также поддерживает 
--          равенство. Используя полученное определение создайте также экземпляр 
--          класса Eq для типа a -> b.

class Finite a where
  values :: [a]

instance Finite Bool where
  values = [False, True]
  
instance (Finite a, Finite b) => Finite (a, b) where
  values = [(x, y) | x <- values, y <- values]
  
instance (Finite a, Eq a) => Finite (Set a) where
  values = map Set (subsets values)

instance (Finite a, Finite b, Eq a) => Finite (a -> b) where
  values = map (\xs x -> fromJust $ lookup x xs) env
    where
      env = [[(x, y)] | x <- values, y <- values]
      fromJust (Just x) = x

instance (Finite a, Eq a, Finite b, Eq b) => Eq (a -> b) where
  f == g = all (\x -> f x == g x) values

----------------------------------------------------------------------------------
-- Упр 6. Определите алгебраический тип данных Complex для комплексных чисел. 
--        Создайте селекторы realPart и imagPart, которые возвращают действительную 
--        и мнимую части комплексного числа соответственно. Complex должен быть 
--        экземпляром классов типов Eq и Show.
--
--        Определите экземпляр класс типов Num для типа Complex.

data Complex = Complex {realPart :: Float, imagPart :: Float}

instance Eq Complex where
  (==) = \(Complex a b) (Complex c d) -> a == c && b == d
  (/=) = \x y -> not (x == y)

instance Show Complex where
  show (Complex a b)
    | a == 0 && b == 0 = "0"
    | a == 0 = show b ++ "i"
    | b == 0 = show a
    | b > 0 = show a ++ "+" ++ show b ++ "i"
    | otherwise = show a ++ "-" ++ show b ++ "i"

floatAbs :: Complex -> Float
floatAbs (Complex a b) = sqrt(a^2 + b^2)

instance Num Complex where
  (+) = \(Complex a b) (Complex c d) -> Complex (a+c) (b+d)
  (*) = \(Complex a b) (Complex c d) -> Complex (a*c - b*d) (a*d + b*c)
  abs = \(Complex a b) -> Complex (sqrt(a^2 + b^2)) 0
  signum (Complex a b) = Complex (a/m) (b/m)
    where m = sqrt(a^2 + b^2)
  fromInteger = \x -> Complex (fromInteger x) 0
  negate = \(Complex a b) -> Complex (-a) (-b)

-------------------------------------------------------------------------------
-- Упр 7. Реализуйте функцию IncrementAll, которая получает контейнер-функтор,
-- содержащий числа и увеличивает каждое число в контейнере на 1.
--
-- Примеры:
--   incrementAll [1,2,3]     ==>  [2,3,4]
--   incrementAll (Just 3.0)  ==>  Just 4.0

incrementAll :: (Functor f, Num n) => f n -> f n
incrementAll x = fmap (+1) x

--------------------------------------------------------------------------------
-- Упр. 8. Реализуйте функции fmap2 и fmap3, которые выполняют отображение для 
-- вложенных функторов.
--
-- Примеры:
--   fmap2 on [[Int]]:
--     fmap2 negate [[1,2],[3]]
--       ==> [[-1,-2],[-3]]
--   fmap2 on [Maybe String]:
--     fmap2 head [Just "abcd",Nothing,Just "efgh"]
--       ==> [Just 'a',Nothing,Just 'e']
--   fmap3 on [[[Int]]]:
--     fmap3 negate [[[1,2],[3]],[[4],[5,6]]]
--       ==> [[[-1,-2],[-3]],[[-4],[-5,-6]]]
--   fmap3 on Maybe [Maybe Bool]
--     fmap3 not (Just [Just False, Nothing])
--       ==> Just [Just True,Nothing]

fmap2 :: (Functor f, Functor g) => (a -> b) -> f (g a) -> f (g b)
fmap2 f = (fmap (fmap f))

fmap3 :: (Functor f, Functor g, Functor h) => (a -> b) -> f (g (h a)) -> f (g (h b))
fmap3 f = (fmap (fmap (fmap f)))

------------------------------------------------------------------------------
-- Упр. 9. Напишите функцию, которая складывает два Maybe Int используя 
-- операторы Applicative (liftA2 и pure). Не используйте сравнение с образцом
--
-- Примеры:
--  sumTwoMaybes (Just 1) (Just 2)  ==> Just 3
--  sumTwoMaybes (Just 1) Nothing   ==> Nothing
--  sumTwoMaybes Nothing Nothing    ==> Nothing

sumTwoMaybes :: Maybe Int -> Maybe Int -> Maybe Int
sumTwoMaybes = liftA2 (+)

------------------------------------------------------------------------------
-- Упр. 10. Напишите функцию, которая получает операцию (double или negate) и число
-- в виде строк. Функция должна вычислить результат операции. 
-- Если передано неизвестное имя функции или некорректное число, то функция
-- должна вернуть Nothing
--
-- Подсказка: воспользуйтесь функцией readMaybe из модуля Text.Read
--
-- Примеры:
--  calculator "negate" "3"   ==> Just (-3)
--  calculator "double" "7"   ==> Just 14
--  calculator "doubl" "7"    ==> Nothing
--  calculator "double" "7x"  ==> Nothing

calculator :: String -> String -> Maybe Int
calculator op num
  | op == "double" = fmap (*2) x
  | op == "negate" = fmap (0-) x
  | otherwise = Nothing
  where
    x = readMaybe num :: Maybe Int

-------------------------------------------------------------------------------
-- Упр. 11. Какое выражение эквивалентно do-блоку:
--            do y <- z
--               s y
--               return (f y)
-- Варианты ответа:
--          a.  z >> \y -> s y >> return (f y)
--          b.  z >>= \y -> s y >> return (f y)
--          c.  z >> \y -> s y >>= return (f y)

answer11 :: Char
answer11 = 'b'

------------------------------------------------------------------------------
-- Упр. 12. Какой тип у выражения: \x xs -> return (x : xs)
-- Варианты ответа:
--          a.  Monad m => a -> [a] -> m [a]
--          b.  Monad m => a -> [m a] -> [m a]
--          c.  a -> [a] -> Monad [a]

answer12 :: Char
answer12 = 'a'

------------------------------------------------------------------------------
-- Упр. 13. Тип Result ниже работает почти как Maybe, но в отличие от 
-- последнего имеет два типа для обозначения отсутствия значения: один без и 
-- один с описанием.
--
-- Реализуйте экземпляры классов типов Functor, Monad и Applicative для Result.
--
-- Result ведёт себя как тип Maybe в том смысле, что
-- 1) MkResult - это как Just
-- 2) Если в процессе вычисления возникает NoResult, то всё вычисление 
--    даёт в результате NoResult (как Nothing)
-- 3) Также, если было получено значение Failure "причина", то результат
--    всего вычисления будет Failure "reason"
--
-- Примеры:
--   MkResult 1 >> Failure "boom" >> MkResult 2
--     ==> Failure "boom"
--   MkResult 1 >> NoResult >> Failure "not reached"
--     ==> NoResult
--   MkResult 1 >>= (\x -> MkResult (x+1))
--     ==> MkResult 2

data Result a = MkResult a | NoResult | Failure String
  deriving Show

instance Functor Result where
  fmap _ NoResult = NoResult
  fmap _ (Failure s) = Failure s
  fmap f (MkResult a) = MkResult (f a)


instance Applicative Result where
  pure = MkResult
  NoResult <*> _ = NoResult
  _ <*> NoResult = NoResult
  (Failure s) <*> _ = Failure s
  _ <*> (Failure s) = Failure s
  (MkResult f) <*> (MkResult x) = MkResult (f x)

instance Monad Result where
  return = MkResult
  (NoResult) >>= _ = NoResult
  (Failure s) >>= _ = Failure s
  (MkResult x) >>= f = f x

------------------------------------------------------------------------------
-- Упр. 14. Ниже дана реализациия списков а-ля Haskell. Реализуйте 
-- экземпляры классов типов Functor, Applicative и Monad для List.
--
-- Пример:
--   fmap (+2) (LNode 0 (LNode 1 (LNode 2 Empty)))
--     ==> LNode 2 (LNode 3 (LNode 4 Empty))

data List a = Empty | LNode a (List a)
  deriving Show

concat2 :: List a -> List a -> List a
concat2 Empty l = l
concat2 (LNode x xs) l = LNode x (concat2 xs l)

instance Functor List where
  fmap f Empty = Empty
  fmap f (LNode x ls) = LNode (f x) (fmap f ls)

instance Applicative List where
  pure = \x -> LNode x Empty
  Empty <*> _ = Empty
  _ <*> Empty = Empty
  (LNode f fs) <*> l = concat2 (fmap f l) (fs <*> l)

instance Monad List where
  return = \x -> LNode x Empty
  (LNode x xs) >>= f = concat2 (f x) (xs >>= f)

------------------------------------------------------------------------------
-- Определить экземпляры классов типов Functor, Applicative и Monad для типа 
-- данных Fun.

newtype Fun a b = Fun {getFun :: a -> b}
  
instance Functor (Fun a) where
  fmap f (Fun g) = Fun (f . g)

instance Applicative (Fun a) where
  pure x = Fun (\_ -> x)
  (Fun f) <*> (Fun g) = Fun (\x -> f x (g x))

instance Monad (Fun a) where
  return x = Fun (\_ -> x)
  (Fun f) >>= g = Fun (\x -> getFun (g (f x)) x)

------------------------------------------------------------------------------
-- Упр. 16. Докажите, что любая монада – это также функтор и аппликативный 
-- функтор.
-- Указание: для выполнения задания необходимо, используя функции:
--              return :: a -> m a
--              (>>=)  :: m a -> (a -> m b) -> m b
-- Реализовать следующие функции:

fmap' :: Monad m => (a -> b) -> m a -> m b
fmap' f mx = ap' (return f) mx
pure' :: Monad m => a -> m a
pure' x = return x
ap' :: Monad m => m (a -> b) -> m a -> m b
ap' mf mx = mf >>= (\f -> mx >>= (\x -> return (f x)))

------------------------------------------------------------------------------
-- Упр. 17. Напишите выражение, которое печатает в консоль “Hello world!”.

expression17 :: IO ()
expression17 = putStrLn "Hello world!"

------------------------------------------------------------------------------
--Упр. 18. Напишите функцию, которая запрашивает из консоли "имя", а затем 
--         печатает в консоль:
--              “Good day, имя”

expression18 :: IO ()
expression18 = do putStrLn "Enter your name..."
                  name <- getLine
                  putStrLn ("Good day, " ++ name) 

------------------------------------------------------------------------------
-- Упр. 19. Реализуйте цикл while. while должен выполнять операцию до тех пор,
-- пока условие возвращет значение True.
--
-- Примеры:
--   -- ничего не печатает
--   while (return False) (putStrLn "IMPOSSIBLE")
--
--   -- печатает YAY! до тех пор пока пользователь продолжает вводить Y.
--   while ask (putStrLn "YAY!")


ask :: IO Bool -- используется в примере
ask = do putStrLn "Y/N?"
         line <- getLine
         return $ line == "Y"

while :: IO Bool -> IO () -> IO ()
while cond op = do
  res <- cond
  if res 
    then 
      op >> while cond op
    else return ()

------------------------------------------------------------------------------
