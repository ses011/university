module GuardExpr where

f :: Int -> Int -> Int
f x y
  | x > a = 0
  | x > b = 1
  | x * 2 == a = 2
  | x + 10 == b = 3
  | otherwise = 4
  where 
    a = y + 1
    b = y - 2