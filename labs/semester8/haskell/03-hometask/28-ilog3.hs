module ILog3 where

ilog3 :: Integer -> Integer
ilog3 n 
	| n == 0 = 0
	| n < 3 = 1
	| otherwise = 1 + ilog3 (div n 3)