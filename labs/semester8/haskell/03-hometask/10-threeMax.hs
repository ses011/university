module ThreeMax where

threeMax :: Int -> Int -> Int -> Int
threeMax a b c = max a (max b c)