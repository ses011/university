module AddDouble where

infixl 7 +*
(+*) :: Double -> Double -> Double
(+*) x y = (x+y)*2