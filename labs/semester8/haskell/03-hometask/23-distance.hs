module Distance where

distance :: Double -> Double -> Double -> Double -> Double
distance a b c d = sqrt ((a-c)^2 + (b-d)^2)