module Sums where

sums :: Int -> [Int]
sums n = [x+y | x <- [1..n], y <- [1..n]]