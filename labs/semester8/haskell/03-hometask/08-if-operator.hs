module TestIf where

testIf :: Int -> Int
testIf n = if n > 5 then 0 else 1 