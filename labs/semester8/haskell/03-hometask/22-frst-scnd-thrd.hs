module FrstScndThrd where

frst :: (a, b, c)-> a
frst (a, _, _) = a 

scnd :: (a, b, c)-> b
scnd (_, b, _) = b

thrd :: (a, b, c)-> c
thrd (_, _, c) = c