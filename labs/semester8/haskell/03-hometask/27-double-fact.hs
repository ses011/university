module DoubleFac where

double_fac :: Int -> Int
double_fac 0 = 1
double_fac n = fac n * double_fac (n-1) where fac a = if a == 0 then 1 else a * fac (a-1)