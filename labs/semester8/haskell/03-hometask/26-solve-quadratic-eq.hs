module Solve where

data Root = Two (Double, Double) | One Double | No deriving Show
 
solve :: Double -> Double -> Double -> Root
solve a b c 
	| d > 0.0 = (Two ((((-b) + (sqrt d))/(2.0*a)), (((-b) - (sqrt d))/(2.0*a))))
    | d < 0.0 = No
    | otherwise = (One ((-b)/(2.0*a)))
	where d=b*b-4.0*a*c