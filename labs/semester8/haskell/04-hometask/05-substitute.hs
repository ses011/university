module SUBSTITUTE where

subs :: Char -> Char -> String -> String
subs _ _ "" = ""
subs a b (x:xs) 
  | a==x = b : subs a b xs
  | otherwise = x : (subs a b xs)