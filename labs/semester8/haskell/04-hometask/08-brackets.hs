module BRACKETS where

check :: String -> Int -> Int
check str stack
  | length str == 0 = stack
  | s == '(' = check ss (stack+1)
  | s == ')' && stack == 0 = 666
  | s == ')' = check ss (stack-1)
  | otherwise = check ss stack
  where
    s = head str
    ss = tail str

balance :: String -> Bool
balance s = (check s 0) == 0