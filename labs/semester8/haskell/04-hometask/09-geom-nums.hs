module GEOMNUMS where

triangulars :: Integer -> [Integer]
piram 1 = 1
piram(x) = x + piram(x-1)
triangulars 0 = [ ]
triangulars x = triangulars (x-1) ++ [piram(x)]

pyramidals :: Integer -> [Integer]
pir 1 = 1
pir(x) = x+pir(x-1)
p 1 = 1
p x = pir(x) + p(x-1)
pyramidals 0 = [ ]
pyramidals x = pyramidals (x-1) ++ [p(x)]