module NAT where

data Nat = Zero | PlusOne Nat
  deriving (Show,Eq)

fromNat :: Nat -> Int
fromNat Zero = 0
fromNat (PlusOne n) = 1 + fromNat n

toNat :: Int -> Maybe Nat
toNat z
  | z < 0 = Nothing
  | z == 0 = Just Zero
  | otherwise = case x of
    Nothing -> Nothing
    Just nat -> Just (PlusOne nat)
    where x = toNat (z - 1)
