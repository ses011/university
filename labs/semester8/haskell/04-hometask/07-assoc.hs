module ASSOC where

assoc :: Int -> String -> [(String, Int)] -> Int
assoc def _ [] = def
assoc def key ((k, v):xs)
  | key == k = v
  | key /= k = assoc def key xs
  | otherwise = def