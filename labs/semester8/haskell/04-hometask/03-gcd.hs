module GCDC where

gcdc::Int->Int->Int
gcdc a 0 = a
gcdc a b = gcdc b (rem a b)