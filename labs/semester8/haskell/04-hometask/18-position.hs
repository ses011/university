module POSITION where

data Position = Position Int Int

origin :: Position
origin = Position 0 0

getX :: Position -> Int
getX (Position x _) = x

getY :: Position -> Int
getY (Position _ y) = y

up :: Position -> Position
up (Position x y) = Position x (y + 1)

right :: Position -> Position
right (Position x y) = Position (x + 1) y