module DELETECHAR where

delete :: Char -> String -> String
delete _ "" = ""
delete c (x:xs) 
  | c==x = delete c xs
  | otherwise = x : (delete c xs)