module PASCAL where

import qualified Data.ByteString.Char8 as C

pascal :: Int -> Int -> Int
pascal r c
  | c > r || c < 0 || r < 0 = 0
  | c == 0 || r == 0 = 1
  | otherwise = pascal (r-1) c + pascal (r-1) (c-1)

-- возвращает строковое представление первых n строчек треугольника паскаля.
printIt :: Int -> C.ByteString
printIt n = C.pack $ show $ [pascal x y | x <- [0..n], y <- [0..x]]

-- печатает первые n строчек треугольника паскаля в консоль.
printItIo :: Int -> IO ()
printItIo n = mapM_ print [[pascal x y | y <- [0..x]] | x <- [0..n]]