module CHANGEMONEY where

get :: [Int] -> Int -> Int
get list ind 
  | ind > length list = 0
  | length list == ind = 1
  | otherwise = list!!(ind-1)

aux :: Int -> [Int] -> [Int] -> Int
aux money coins buf
  | length buf == money = head buf
  | otherwise = aux money coins ([sum (map (\x -> get buf x) coins)] ++ buf)

change :: Int -> [Int] -> Int
change money coins
  | money == 0 = 0
  | length coins == 0 = 0
  | otherwise = aux money coins []
