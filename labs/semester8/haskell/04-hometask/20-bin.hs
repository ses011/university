module BIN where

data Bin = End | O Bin | I Bin deriving (Show, Eq)

inc :: Bin -> Bin
inc End   = I End
inc (O b) = I b
inc (I b) = O (inc b)

prettyPrint :: Bin -> String
prettyPrint End = ""
prettyPrint (O bin) = '0' : prettyPrint bin
prettyPrint (I bin) = '1' : prettyPrint bin

fromBin :: Bin -> Int
fromBin End = 0
fromBin (O bin) = 2 * fromBin bin
fromBin (I bin) = 1 + 2 * fromBin bin

toBin :: Int -> Bin
toBin x
  | x == 0 = O End
  | x == 1 = I End
  | even x = O (toBin (x `div` 2))
  | otherwise = I (toBin (x `div` 2))