module REVERSE where

{-# LANGUAGE MultiWayIf #-}
reverseList :: [a] -> [a]
reverseList list = 
  let x = head list; xs = tail list
  in 
  if | length list == 0 -> []
     | otherwise -> reverseList xs ++ [x]
