-- 1 простой
/* Пять самых популярных объявлений в категории одежда, которые еще не проданы */
SELECT 
	product_id,
	title,
	views
FROM 
	products
WHERE
	category = 'clothes' AND status = 'new'
ORDER BY views DESC LIMIT(5);

-- 2 простой
/* Количество сообщений, которые один пользователь отправил другому */
SELECT
	count(*) AS messages_amount
FROM
	users_to_messages
WHERE
	from_user = 6002 AND
	to_user = 6067;

-- 3 простой
/* Все пользователи, которые еще не совершали пополнение баланса */
SELECT 
	users.user_id,
	users.login
FROM
	users LEFT JOIN operations
	ON users.user_id = operations.user_id
WHERE
	operations.user_id IS NULL;

-- 4 простой
/* Все пользователи из Москвы, которые что-то продали */
WITH extract_exp_info AS (
	SELECT 
		login,
		contact_information,
		status
	FROM 
		users INNER JOIN products ON users.user_id = products.user_id
	WHERE 
		status = 'sold' AND contact_information::json->>'city' = 'moscow'
)

SELECT DISTINCT login FROM extract_exp_info;

-- 5 простой
/* Список избранного конкретного пользователя */
SELECT 
	favourites.product_id,
	title
FROM 
	favourites INNER JOIN products
	ON favourites.product_id = products.product_id
WHERE 
	favourites.user_id = 6003;

-- 6 средний
/* Список всех объявлений, добавленных за июль и содержащих более трех фотографий,
   упорядоченный по дате добавления */
SELECT 
	products.product_id,
	products.title,
	products.created_at,
	count(*) AS photos_amount
FROM 
	products INNER JOIN product_to_photos ON products.product_id = product_to_photos.product_id
WHERE 
	EXTRACT(MONTH FROM products.created_at) = 7
GROUP BY products.product_id
HAVING count(*) > 3
ORDER BY products.created_at;

-- 7 простой
/* Обновить баланс пользователя */
BEGIN;

UPDATE 
	users
SET 
	balance = balance + cast(519.87 as money)
WHERE user_id = 6918;

INSERT INTO operations(user_id, operation_type, amount, comment)
VALUES 
	(6918, 'accrual', cast(519.87 as money), 'accrual of balance of this user');

COMMIT;

-- 8 средний
/* Список всех непрочитанных сообщений конкретного пользователя,
   к которым прикреплены фотографии, упорядоченный по дате добавления.
   Если дата совпадает, упорядочить по длине сообщения. */
SELECT 
	U1.login AS from_user,
	messages.content,
	messages.created_at,
	photos.path_to_file
FROM
	users INNER JOIN users_to_messages UTM ON users.user_id = UTM.to_user
	      INNER JOIN messages ON UTM.message_id = messages.message_id
	      INNER JOIN messages_to_photos MTP ON messages.message_id = MTP.message_id
	      INNER JOIN photos ON photos.photo_id = MTP.photo_id
	      INNER JOIN users U1 ON UTM.from_user = U1.user_id
WHERE
	users.user_id = 6429 AND
	messages.is_read = false
ORDER BY 
	messages.created_at ASC,
	char_length(messages.content) DESC;

-- 9 средний
/* Список из 10 самых НЕ просматриваемых в челябинске объявлений,
   если объявления имеют одинаковое число просмотров,
   они сортируются по цене */
SELECT
	login,
	contact_information::json->>'district',
	product_id,
	views,
	title,
	price,
	created_at
FROM
	users INNER JOIN products on users.user_id = products.user_id
WHERE 
	users.contact_information::json->>'city' = 'chelyabinsk'
ORDER BY views ASC, price DESC
LIMIT(10);
 
-- 10 простой
/* Создать view для нахождения рейтинга всех пользователей */
CREATE OR REPLACE VIEW users_with_rating AS 
SELECT DISTINCT
	user_id,
	AVG(rating) OVER (PARTITION BY login) AS r
FROM
	users INNER JOIN users_to_comments UTC ON users.user_id = UTC.to_user
		   INNER JOIN comments ON UTC.comment_id = comments.comment_id;

-- 11 простой
/* Общие траты пользователя */
SELECT
	users.login,
	SUM(amount) OVER (PARTITION BY users.login) AS sum
FROM 
	users INNER JOIN operations on users.user_id = operations.user_id
WHERE
	users.login = 'bear__0' AND
	operations.operation_type in ('debiting', 'transfer');

-- 12 простой
/* Найти количество товаров, проданных в каждой категории.
   Товары без также учитываются */
SELECT DISTINCT
	category,
	count(*) OVER (PARTITION BY category)
FROM
	products
ORDER BY count DESC;

-- 13 средний
/* Город, в котором наибольшее число новых объявлений */
SELECT
	contact_information::json->>'city' AS city,
	count(*)
FROM
	users INNER JOIN products on users.user_id = products.user_id
WHERE 
	products.status = 'new'
GROUP BY contact_information::json->>'city'
ORDER BY count(*) ASC
LIMIT(1);
	

-- 14 средний
/* Отобразить страницу объявления */
WITH photos_of_product AS (
	SELECT ARRAY(
		SELECT photo_id
		FROM product_to_photos
		WHERE product_id = 2078
	) AS a
)

SELECT 
	login,
	contact_information::json->>'city' AS city,
	contact_information::json->>'district' AS district,
	contact_type,
	title,
	description,
	price,
	created_at,
	r AS rating,
	(SELECT a FROM photos_of_product) AS photos_array
FROM
	users INNER JOIN products on users.user_id = products.user_id
		   INNER JOIN users_with_rating UWR ON users.user_id = UWR.user_id
WHERE product_id = 2078;


-- сложные (наверное)
-- 15 (точно сложный)
/* Найти самые худшие копании всех городов */
WITH companies AS (
	SELECT DISTINCT
		login,
		contact_information::json->>'city' AS city,
		r
	FROM
		users INNER JOIN users_with_rating UWR ON users.user_id = UWR.user_id 
	WHERE users.user_type = 'company'
),

min_rating AS (
	SELECT DISTINCT
		city,
		MIN(r) OVER (PARTITION BY city) as min_r
	FROM
		companies
)

SELECT 
	login,
	companies.city,
	min_r
FROM 
	companies INNER JOIN min_rating ON min_rating.city = companies.city
WHERE
	companies.r = min_rating.min_r
ORDER BY companies.city;

-- 16
/* Суммарная стоимость проданных товаров по каждому району каждого города,
	начиная с июля, пользователями с рейтингом больше 3. */
WITH sold_products AS (
	SELECT
		user_id,
		product_id,
		price
	FROM products
	WHERE
		status = 'sold' AND
		EXTRACT(MONTH FROM created_at) > 7
),
n_users AS (
	SELECT
		users.user_id,
		contact_information::json->>'city' AS city,
		contact_information::json->>'district' AS district,
		r
	FROM
		users INNER JOIN users_with_rating ON users.user_id = users_with_rating.user_id
	WHERE r > 3
)

SELECT
	city,
	district,
	SUM(price)
FROM
	n_users LEFT JOIN sold_products ON n_users.user_id = sold_products.user_id
WHERE
	sold_products.user_id IS NOT NULL
GROUP BY n_users.city, n_users.district
ORDER BY city, district ASC, SUM(price) DESC;

-- 17
/* Найти количетсво новых объявлений в каждой категории каждого города, учитывая только
	объявления, добавленные после июля пользователями, не отправившими ни одного сообщения */
WITH n_products AS (
	SELECT
		products.user_id AS user_id,
		products.status AS status,
		products.category AS category
	FROM
		products
	WHERE
		EXTRACT(MONTH FROM products.created_at) > 7
),
n_users AS (
	SELECT
		users.contact_information AS contact_information,
		users.user_id
	FROM
		users LEFT JOIN users_to_messages ON users.user_id = users_to_messages.from_user
	WHERE
		users_to_messages.from_user IS NULL
)

SELECT
	n_users.contact_information::json->>'city' AS city,
	n_products.category AS category,
	count(*) AS cnt
FROM
	n_users INNER JOIN n_products ON n_users.user_id = n_products.user_id
WHERE
	n_products.status = 'new'
GROUP BY
	n_users.contact_information::json->>'city', n_products.category
ORDER BY
	n_users.contact_information::json->>'city', n_products.category ASC,
	cnt DESC;

-- 18
/* Найти пользователей с рейтингом выше среднего по району, которые продали более 1 товара.
	Результат отсортировать по городу, району по убыванию, если они совпадают, то по рейтингу и количеству
	проданных товаров по возрастанию. */
WITH average_city_district AS (
	SELECT
		login,
		contact_information::json->>'city' AS city,
		contact_information::json->>'district' AS district,
		r,
		AVG(r) OVER (PARTITION BY cast(contact_information::json->>'city' AS text) || cast(contact_information::json->>'district' AS text)) AS avg
	FROM users INNER JOIN users_with_rating UWR ON users.user_id = UWR.user_id
	WHERE users.user_type = 'single'
),
sold_amount AS (
	SELECT
		login,
		count(*) AS cnt
	FROM 
		users INNER JOIN products on users.user_id = products.user_id
	WHERE
		products.status = 'sold' AND
		users.user_type = 'single'
	GROUP BY users.user_id, products.status
	HAVING count(*) > 1
)

SELECT 
	ACD.login AS login,
	ACD.city AS city,
	ACD.district AS district,
	ACD.r AS r,
	ACD.avg AS avg,
	SA.cnt AS cnt
FROM 
	average_city_district ACD LEFT JOIN sold_amount SA on SA.login = ACD.login
WHERE
	SA.cnt IS NOT NULL AND
	ACD.r > ACD.avg
ORDER BY
	city, district ASC,
	r DESC,
	cnt DESC;

-- 19
/* Отсортировать города по числу фотографий, прикрепленным к объявлениям. 
	Если число фотографий одинакого, то отсортировать по количеству операций, проводимых пользователями  */
WITH users_products AS (
	SELECT DISTINCT
		users.login AS login,
		contact_information::json->>'city' AS city,
		count(PTP.photo_id) OVER (PARTITION BY login) AS cnt_ph
	FROM
		users RIGHT JOIN products ON products.user_id = users.user_id
				INNER JOIN product_to_photos PTP on products.product_id = PTP.product_id
),
city_photos AS (
	SELECT
	city,
	SUM(cnt_ph) AS scp
FROM
	users_products
GROUP BY
	city
),
users_operations AS (
	SELECT DISTINCT
		users.login AS login,
		contact_information::json->>'city' AS city,
		count(operations.operation_id) OVER (PARTITION BY login) AS cnt_op
	FROM
		users INNER JOIN operations ON operations.user_id = users.user_id
),
city_operations AS (
	SELECT
	city,
	SUM(cnt_op) AS sco
FROM
	users_operations
GROUP BY
	city
)

SELECT 
	city_operations.city,
	scp,
	sco
FROM
	city_photos INNER JOIN city_operations ON city_operations.city = city_photos.city
ORDER BY
	scp DESC,
	sco DESC;

-- 20
/* Извлечь не более 4 самых просматриваемых объявления из каждого города, имеющих ненулевое число просмотров,
	создатели которых отправили более трех сообщений
	и имеют рейтинг выше среднего по городу. */
with messages_users AS (
	SELECT
		users.login,
		count(*) AS cnt
	FROM
		users RIGHT JOIN users_to_messages ON users.user_id = users_to_messages.from_user
	GROUP BY users.login
	HAVING count(*) > 3
),
users_rating AS (
	SELECT
		login,
		r,
		AVG(r) OVER (PARTITION BY cast(contact_information::json->>'city' AS text)) AS avg
	FROM
		users INNER JOIN users_with_rating ON users.user_id = users_with_rating.user_id
),
products_views AS (
	SELECT
		users.login AS login,
		contact_information::json->>'city' AS city,
		products.title AS title,
		products.views AS views,
		rank() OVER (PARTITION BY cast(contact_information::json->>'city' AS text) ORDER BY views DESC) AS rank
	FROM
		users INNER JOIN products on users.user_id = products.user_id
			   INNER JOIN messages_users ON users.login = messages_users.login
			   INNER JOIN users_rating ON users.login = users_rating.login AND users_rating.r > users_rating.avg
)

SELECT
	login,
	city,
	title,
	views
FROM
	products_views
WHERE 
	rank <= 4 AND 
	views != 0
ORDER BY city ASC, views DESC;

-- 21
/* Для каждого города найти месяц, в котором пользователи отправляли наибольшее число сообщений c фотографиями
	и месяц, в котором пользователи отправляли наименьшее число сообщений с фотографиями.
	Если таких месяцев несколько, выбирается с наименьшим порядковым номером. */
WITH n_messages AS (
	SELECT
		contact_information::json->>'city' AS city,
		EXTRACT(MONTH FROM messages.created_at) AS month,
		count(*) AS cnt
	FROM
		users INNER JOIN users_to_messages ON users.user_id = users_to_messages.from_user
				INNER JOIN messages ON messages.message_id = users_to_messages.message_id
			   INNER JOIN messages_to_photos ON messages.message_id = messages_to_photos.message_id
	GROUP BY
		contact_information::json->>'city',
		EXTRACT(MONTH FROM messages.created_at)
	ORDER BY city ASC, EXTRACT(MONTH FROM messages.created_at) ASC
),
min_amount AS (
	SELECT
		city,
		month,
		cnt,
		MIN(cnt) OVER (PARTITION BY city) AS min_cnt
	FROM n_messages
),
max_amount AS (
	SELECT
		city,
		month,
		cnt,
		MAX(cnt) OVER (PARTITION BY city) AS max_cnt
	FROM n_messages
)

SELECT DISTINCT ON (city)
	city,
	month,
	cnt
FROM min_amount
WHERE
	min_cnt = cnt

UNION ALL

SELECT DISTINCT ON (city)
	city,
	month,
	cnt
FROM max_amount
WHERE
	max_cnt = cnt

ORDER BY city ASC, cnt DESC, month ASC;