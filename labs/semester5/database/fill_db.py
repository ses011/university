import psycopg2
import random
import string

AMOUNT_USERS = 1000
AMOUNT_PRODUCTS = 2000


def fill_users():
	LOGIN = ['cat___', 'fox___', 'owl___', 'bear__', 'parrot', 'pen___', 'puma__', 'tiger_', 'lion__', 'ostrich', 'sheep_', 'dog___', 'cow___', 'shrek_', 'shadow_fiend']
	EMAIL = ['@gmail.com', '@yandex.ru', '@mail.ru', '@mephi.ru', '@rambler.ru']
	CITY = ['moscow', 'penza', 'spb', 'saratov', 'perm', 'ryazan', 'kolomna', 'novosibirsk', 'chelyabinsk', 'omsk', 'tomsk']
	DISTRICT = ['leninsky', 'zheleznodorozhny', 'pervomaysky', 'central', 'kekus']
	METRO = ['china-town', 'park of culture', 'kolomenskoe', '1905 year street', 'teatralnaya', 'sokol', 'kuznetskiy most', 'trubnaya', 'barrikadnaya', 'petrovskiy park']
	TYPE = ['single', 'company']
	letters = string.ascii_lowercase
	nums = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]

	query = 'INSERT INTO users(login, password, email, profile_photo, contact_phone, contact_information, balance, user_type) VALUES '

	for i in range(AMOUNT_USERS):
		l = random.choice(LOGIN) + str(i)
		p = ''.join(random.choice(letters) for _ in range(random.randint(10, 80)))
		e = l + random.choice(EMAIL)
		ph = '~/users/' + l
		n = '7' + ''.join(str(random.choice(nums)) for _ in range(9))

		c = random.choice(CITY)
		d = random.choice(DISTRICT)
		contact_inf = {'city': c, 'district': d}

		if c == 'moscow' or c == 'spb':
			contact_inf['metro'] = random.choice(METRO)

		cti = str(contact_inf).replace('\'', '\"')

		b = 0
		if random.randint(0, 100) < 25:
			b = random.randint(1, 10041)

		t = 'single'
		if random.randint(0, 100) > 80:
			t = 'company'

		query += f'(\'{l}\', \'{p}\', \'{e}\', \'{ph}\', \'{n}\', \'{cti}\', {b}, \'{t}\'), '

	query = query[:-2]
	query += ';'

	return query


def generate_timestamp():
	mm = random.randint(1, 11)

	if mm == 2:
		dd = random.randint(1, 27)
	else:
		dd = random.randint(1, 30)

	hh = random.randint(0, 23)
	mi = random.randint(0, 59)
	ss = random.randint(0, 59)
	return f'2021-{mm}-{dd} {hh}:{mi}:{ss}'



def fill_products():
	CATEGORY = ['fashion', 'clothes', 'toys', 'healt', 'sport', 'groceries', 'technology', 'home', 'gifts', 'furniture', 'hobby']

	ADJ = ['enormous', 'pro', 'good', 'cursed', 'magnificent', 'beautiful', 'bad', 'worst', 'angry', 'big', 'small', 'old', 'new', 'rectangular', 'red', 'orange', 'pretty', 'steel', 'Halloween', 'large', 'good looking', 'fabulous', 'adorable', 'gorgeous', 'meaningless', 'wooden', 'biological', 'american', 'russian', 'small', 'funny', 'random', 'round', 'corpulent']

	T = ['flopa', 'Samsung', 'notebook', 'MacBook', 'Pro', 'IMac', 'задержка',
		  'BOSS ds-1', 'shoes', 'тетрадь 12 листов купить оптом', 'video games for pc', 'шоха деда',
		  'Дача недалеко от города', 'Lego 1', 'Lego 2', 'Nurf blaster 1', 'Nurf blaster 2', 'case for iphone',
		  'Pink case for phone', 'scientific microscope', 'microscope toy', 'Professional skateboard',
		  'Skateboard for begginers', 'Fender stratocaster', 'Gibson SG', 'Backpack for men', 'backpack', 'monitor',
		  'phone', 'meme', 'toy', 'blaster', 'kit', 'dota 2', 'anime', 'anime poster', 'cartoon', 'random']

	letters = string.ascii_lowercase
	
	query = 'INSERT INTO products(user_id, created_at, contact_type, views, title, description, price, status, manufacture_information, category) VALUES '

	for i in range(AMOUNT_PRODUCTS):
		u = random.randint(6001, 7000)
		
		created_at = generate_timestamp()

		contact_type = 'message'
		if random.randint(0, 10) > 7:
			contact_type = 'phone'

		views = 0
		if random.randint(0, 100) < 30:
			views = random.randint(1, 1501)

		title = random.choice(ADJ) + ' ' + random.choice(T)
		description = ''.join(random.choice(letters) for _ in range(random.randint(10, random.randint(15, 400))))

		price = random.randint(150, 15000)

		status = 'new'
		if random.randint(0, 100) > 85:
			status = 'sold'

		mi = ''
		if random.randint(0, 100) < 15:
			mi = ''.join(random.choice(letters) for _ in range(random.randint(4, random.randint(8, 50))))

		cat = ''
		if random.randint(0, 100) < 75:
			cat = random.choice(CATEGORY)

		query += f'({u}, to_timestamp(\'{created_at}\', \'yyyy-mm-dd hh24:mi:ss\'), \'{contact_type}\', {views}, \'{title}\', \'{description}\', {price}, \'{status}\', \'{mi}\', \'{cat}\'), '

	query = query[:-2]
	query += ';'

	return query


def fill_photos_for_products():
	query1 = 'INSERT INTO photos(path_to_file) VALUES '
	k = 0

	for i in range(AMOUNT_PRODUCTS):
		amount_of_photos = random.randint(1, 5)
		for j in range(amount_of_photos):
			query1 += f'(\'~/photos/products/{i}_{j}\'), '
			k += 1

	query1 = query1[:-2]
	query1 += ';'

	return query1

def connect_photos_for_products():
	query2 = 'INSERT INTO product_to_photos(product_id, photo_id) VALUES '
	k = 0

	for i in range(AMOUNT_PRODUCTS):
		amount_of_photos = random.randint(1, 5)
		for j in range(amount_of_photos):
			query2 += f'({2077+i}, {17950+k}), '
			k += 1

	query2 = query2[:-2]
	query2 += ';'

	return query2

def fill_favourites():
	query = 'INSERT INTO favourites(user_id, product_id) VALUES '
	for i in range(6001, 7000):
		if random.randint(0, 99) > 57:
			continue

		for j in range(1, random.randint(2, 4)):
			query += f'({i}, {random.randint(2077, 4076)}), '

	query = query[:-2]
	query += ';'
	
	return query


def fill_messages():
	INT = ['Hello!', 'HEllo. ', 'hello ', 'HeLlO ', 'Hi ', 'HI! ', 'hI. ', 'Can I ask about ', 'I want to know ', 'It is very interesting for me ', 'And what about ', 'Kek', 'Why are you', 'Nice to meet you! ']
	CONT = ['why this product is so expensive?', 'I want to buy it.', 'I want to buy it right now.', 'something', 'I have already sold it.', 'Kek', 'Random']
	EMOJI = ['=(', '=)', 'KEKW', ':/', 'o_0', 'UwU', 'BibleThump']

	query = 'INSERT INTO messages(content, created_at, is_read) VALUES '
	
	for i in range(AMOUNT_USERS + 432):
		content = random.choice(INT) + random.choice(CONT)
		if random.randint(0, 1) == 1:
			content += random.choice(EMOJI) 
		created_at = generate_timestamp()
		if random.randint(0, 99) > 66:
			is_read = 'true'
		else:
			is_read = 'false'

		query += f'(\'{content}\', to_timestamp(\'{created_at}\', \'yyyy-mm-dd hh24:mi:ss\'), {is_read}), '

	query = query[:-2]
	query += ';'
	
	return query


def fill_operations(conn):
	DEB_COM = 'debiting of money to external source'
	ACC_COM = 'accrual of balance of this user'
	TR_TO   = 'transfer money to user with id'
	TR_FROM = 'transfer money from user with id'

	query = 'INSERT INTO operations(user_id, created_at, operation_type, amount, comment) VALUES '
	already = []
	should_handle = []
	balances = []

	cur = conn.cursor()
	cur.execute('SELECT user_id, balance::numeric(6,2) FROM users WHERE balance::numeric > 0;')
	for i in cur.fetchall():
		u_id = i[0]
		amount = i[1]
		if random.randint(0, 99) < 47:
			should_handle.append(u_id)
			balances.append(amount)
		else:
			created_at = generate_timestamp()
			query += f'({u_id}, to_timestamp(\'{created_at}\', \'yyyy-mm-dd hh24:mi:ss\'), \'accrual\', {amount}, \'{ACC_COM}\'), '
		already.append(u_id)
	
	for i in range(6001, 7000):
		if i in already:
			continue

		created_at = generate_timestamp()

		if i in should_handle:
			already.append(i)
			balance = balances[already.index(i)]
			from_u = random.randint(6001, 7000)
			if from_u in already:
				query += f'({i}, to_timestamp(\'{created_at}\', \'yyyy-mm-dd hh24:mi:ss\'), \'accrual\', {balance}, \'{ACC_COM}\'), '
			else:
				query += f'({from_u}, to_timestamp(\'{created_at}\', \'yyyy-mm-dd hh24:mi:ss\'), \'accrual\', {balance}, \'{ACC_COM}\'), '
				query += f'({i}, to_timestamp(\'{created_at}\', \'yyyy-mm-dd hh24:mi:ss\'), \'transfer\', {balance}, \'{TR_FROM}{from_u}\'), '
				query += f'({from_u}, to_timestamp(\'{created_at}\', \'yyyy-mm-dd hh24:mi:ss\'), \'transfer\', {balance}, \'{TR_TO}{i}\'), '

		else:
			if random.randint(0, 99) > 78:
				continue
			already.append(i)
			am = random.randint(9, 4728)
			query += f'({i}, to_timestamp(\'{created_at}\', \'yyyy-mm-dd hh24:mi:ss\'), \'accrual\', {am}, \'{ACC_COM}\'), '
			query += f'({i}, to_timestamp(\'{created_at}\', \'yyyy-mm-dd hh24:mi:ss\'), \'debiting\', {am}, \'{DEB_COM}\'), '

	query = query[:-2]
	query += ';'

	print(query)
	
	return query


def fill_comments():
	CONT = ['Nice ', 'Good ', 'Bad ', 'The worst seller in the world ', 'ok ', 'I dont know ', 'Random ', 'I can use it in every day life! ', 'Sooooo.... ', 'Ok ', 'Ok ', 'Ok', 'Ok', 'Ok', 'Ok', 'Nice', 'Good job! ', 'Well done ', 'Bad', 'uuhhh so bad i cant describe it ']
	EMOJI = ['=(', '=)', 'KEKW', ':/', 'o_0', 'UwU', 'BibleThump']
	query = 'INSERT INTO comments(created_at, content, rating) VALUES '
	for i in range(1848):
		content = random.choice(CONT)
		if random.randint(0, 99) > 54:
			content += random.choice(EMOJI)
		created_at = generate_timestamp()
		rating = random.randint(1, 5)
		query += f'(to_timestamp(\'{created_at}\', \'yyyy-mm-dd hh24:mi:ss\'), \'{content}\', {rating}), '

	query = query[:-2]
	query += ';'
	
	return query


def connect_users_to_comments():
	query = 'INSERT INTO users_to_comments(comment_id, from_user, to_user) VALUES '
	for i in range(1, 1848):
		from_u = 1
		to_u = 1
		while from_u == to_u:
			from_u = random.randint(6001, 7000)
			to_u = random.randint(6001, 7000)

		query += f'({i}, {from_u}, {to_u}), '

	query = query[:-2]
	query += ';'
	
	return query


def connect_users_to_messages():
	query = 'INSERT INTO users_to_messages(message_id, from_user, to_user) VALUES '
	for i in range(1, 1432):
		from_u = 1
		to_u = 1
		while from_u == to_u:
			from_u = random.randint(6001, 7000)
			to_u = random.randint(6001, 7000)

		query += f'({i}, {from_u}, {to_u}), '

	query = query[:-2]
	query += ';'
	
	return query


def fill_photos_for_messages():
	query1 = 'INSERT INTO photos(path_to_file) VALUES '
	k = 0

	for i in range(326):
		query1 += f'(\'~/photos/messages/{i}\'), '

	query1 = query1[:-2]
	query1 += ';'

	return query1

# 23913 - 24238

def connect_messages_to_photos():
	query = 'INSERT INTO messages_to_photos(message_id, photo_id) VALUES '
	already = [-1]
	for i in range(23913, 24238):
		msg = -1
		while msg in already:
			msg = random.randint(1, 1432)
		query += f'({msg}, {i}), '
		already.append(msg)

	query = query[:-2]
	query += ';'
	
	return query

def main():
	conn = psycopg2.connect(dbname='avito', user='egor', password='###', host='localhost', port='5432')

	cur = conn.cursor()
	
	cur.execute(fill_users())
	conn.commit()

	cur.close()
	conn.close()

main()
