BEGIN;
CREATE TABLE IF NOT EXISTS users (
	user_id serial PRIMARY KEY,
	login varchar(80) CHECK (char_length(login) >= 6) UNIQUE NOT NULL,
	password varchar(100) CHECK (char_length(password) >= 8) NOT NULL,
	email varchar(80) CHECK (email LIKE '%@%.%') UNIQUE NOT NULL,
	profile_photo text,
	contact_phone char(10) CHECK (contact_phone ~ '^[0-9]'),
	contact_information jsonb,
	balance money NOT NULL DEFAULT 0,
	user_type varchar(7) CHECK (user_type in ('single', 'company')) NOT NULL DEFAULT 'single'
);

CREATE TABLE IF NOT EXISTS photos (
	photo_id serial PRIMARY KEY,
	path_to_file text NOT NULL
);

CREATE TABLE IF NOT EXISTS products (
	product_id serial PRIMARY KEY,
	user_id integer REFERENCES users(user_id) ON DELETE CASCADE,
	created_at TIMESTAMP DEFAULT current_timestamp,
	contact_type varchar(7) CHECK (contact_type in ('phone', 'message')) NOT NULL DEFAULT 'message',
	views integer CHECK (views >= 0) NOT NULL DEFAULT 0,
	title varchar(200) NOT NULL,
	description text NOT NULL,
	price money NOT NULL,
	status varchar(4) CHECK (status in ('new', 'sold')) NOT NULL DEFAULT 'new',
	manufacture_information text,
	category text
);

CREATE TABLE IF NOT EXISTS messages (
	message_id serial PRIMARY KEY, 
	content text NOT NULL,
	created_at TIMESTAMP DEFAULT current_timestamp,
	is_read boolean NOT NULL DEFAULT false
);

CREATE TABLE IF NOT EXISTS operations (
	operation_id serial PRIMARY KEY,
	user_id integer REFERENCES users(user_id) ON DELETE CASCADE,
	created_at TIMESTAMP NOT NULL DEFAULT current_timestamp,
	operation_type varchar(8) CHECK (operation_type in ('debiting', 'accrual', 'transfer')) NOT NULL,
	amount money NOT NULL,
	comment text NOT NULL
);

CREATE TABLE IF NOT EXISTS comments (
	comment_id serial PRIMARY KEY,
	created_at TIMESTAMP DEFAULT current_timestamp,
	content text NOT NULL,
	rating smallint CHECK (rating >= 1 AND rating <= 5) NOT NULL
);

CREATE TABLE IF NOT EXISTS users_to_comments (
	id serial PRIMARY KEY,
	comment_id integer REFERENCES comments(comment_id) ON DELETE CASCADE,
	from_user integer REFERENCES users(user_id) ON DELETE CASCADE,
	to_user integer REFERENCES users(user_id) ON DELETE CASCADE
);

ALTER TABLE users_to_comments ADD CONSTRAINT praise_yourself CHECK (from_user != to_user);

CREATE TABLE IF NOT EXISTS favourites (
	fav_id serial PRIMARY KEY,
	user_id integer REFERENCES users(user_id) ON DELETE CASCADE,
	product_id integer REFERENCES products(product_id) ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS product_to_photos (
	id serial PRIMARY KEY,
	product_id integer REFERENCES products(product_id) ON DELETE CASCADE,
	photo_id integer REFERENCES photos(photo_id) ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS users_to_messages (
	id serial PRIMARY KEY,
	message_id integer REFERENCES messages(message_id) ON DELETE CASCADE,
	from_user integer REFERENCES users(user_id) ON DELETE CASCADE,
	to_user integer REFERENCES users(user_id) ON DELETE CASCADE
);

ALTER TABLE users_to_messages ADD CONSTRAINT message_yourself CHECK (from_user != to_user);

CREATE TABLE IF NOT EXISTS messages_to_photos (
	id serial PRIMARY KEY,
	message_id integer REFERENCES messages(message_id) ON DELETE CASCADE,
	photo_id integer REFERENCES photos(photo_id) ON DELETE NO ACTION
);
END;