#ifndef LAB2_LINKEDLIST_H
#define LAB2_LINKEDLIST_H


#include <iostream>
#include "CustomException.h"


template<typename T>
struct Node{
    T data;
    Node *next;
};


template <typename T>
class LinkedList {
public:
    LinkedList();
    LinkedList(T arr[], int len_);
    LinkedList(const LinkedList<T> &assignable);
    ~LinkedList();
    LinkedList<T>& operator=(const LinkedList<T> &assignable);
    void map(T (*funcPtr)(T));
    void append(T value);
    void prepend(T value);
    void clear();
    void insert(int pos, T value);
    void remove(T value);
    void print();
    void set(int pos, T value);
    void concat(const LinkedList<T> &conc);
    void removeIf(bool (*funcPrt)(T));
    T operator[](int pos);
    T getFirst();
    T getLast();
    T pop(int pos = -1);
    int getLength();
private:
    Node<T> *head = nullptr;
    Node<T> *tail = nullptr;
    int len;
    void addNode(Node<T> **curr, T value);
};


template<typename T>
void LinkedList<T>::addNode(Node<T> **curr, T value) {
    *curr = new Node<T>;
    (*curr)->data = value;
    (*curr)->next = NULL;
    len += 1;
}


template<typename T>
LinkedList<T>::LinkedList()
    : len(0), head(NULL), tail(NULL) {
}


template<typename T>
LinkedList<T>::LinkedList(T arr[], int len_)
    : len(0)
{
    if (len_ > 0) {
        addNode(&head, arr[0]);
        Node<T> *tmp = head;
        for (int i = 1; i < len_; i++) {
            addNode(&(tmp->next), arr[i]);
            tmp = tmp->next;
        }
        tail = tmp;
    }
}


template<typename T>
LinkedList<T>::LinkedList(const LinkedList<T> &assignable)
    : len(0), head(NULL), tail(NULL)
{
    (*this = assignable);
}


template<typename T>
LinkedList<T>::~LinkedList() {
    clear();
}


template<typename T>
inline void LinkedList<T>::clear() {
    len = 0;
    while (head) {
        Node<T> *tmp = head;
        head = head->next;
        delete tmp;
    }
}


template<typename T>
LinkedList<T>& LinkedList<T>::operator=(const LinkedList<T> &assignable) {
    if (&assignable != this) {
        if(assignable.head) {
            this->clear();
            addNode(&head, assignable.head->data);
            Node<T> *tmp1, *tmp2;
            tmp1 = head;
            tmp2 = assignable.head->next;
            while (tmp2) {
                addNode(&(tmp1->next), tmp2->data);
                tmp1 = tmp1->next;
                tail = tmp1;
                tmp2 = tmp2->next;
            }
        } else {
            head = NULL;
            tail = NULL;
            len = 0;
        }
    }
    return (*this);
}


template<typename T>
inline void LinkedList<T>::append(T value) {
    if (!head) {
        addNode(&head, value);
        tail = head;
    } else {
        addNode(&tail->next, value);
        tail = tail->next;
    }
}


template<typename T>
inline void LinkedList<T>::prepend(T value) {
    if (!head) {
        addNode(&head, value);
    } else {
        Node<T> *tmp = new Node<T>;
        tmp->data = value;
        tmp->next = head;
        head = tmp;
        len += 1;
    }
}


template<typename T>
inline void LinkedList<T>::print() {
    if (head) {
        Node<T> *tmp = head;
        while (tmp) {
            std::cout<< tmp->data << ' ';
            tmp = tmp->next;
        }
    }
    std::cout << std::endl;
}


template <typename T>
inline int LinkedList<T>::getLength() {
    return len;
}


template<typename T>
inline void LinkedList<T>::map(T (*funcPtr)(T)) {
    if (head){
        Node<T> *tmp = head;
        while(tmp){
            tmp->data = (*funcPtr)(tmp->data);
            tmp = tmp->next;
        }
    }
}


template<typename T>
inline void LinkedList<T>::remove(T value) {
    Node<T> *tmp;
    if (head->data == value) {
        tmp = head;
        if (head->next) {
            head = head->next;
        } else {
            head = NULL;
        }
        delete tmp;
        len -= 1;
        return;
    } else if (tail->data == value) {
        tmp = head;
        while (tmp->next != tail) {
            tmp = tmp->next;
        }
        delete tmp->next;
        tail = tmp;
        tail->next = nullptr;
        len -= 1;
        return;
    } else {
        Node<T> *prev;
        prev = head;
        tmp = head->next;
        while (tmp) {
            if (tmp->data == value) {
                prev->next = tmp->next;
                delete tmp;
                len -= 1;
                return;
            }
            prev = tmp;
            tmp = tmp->next;
        }
    }
    throw DescriptiveException("Value error: this value isn`t in your list");
}


template<typename T>
inline void LinkedList<T>::insert(int pos, T value) {
    Node<T> *tmp;
    if (pos == 0) {
        prepend(value);
        return;
    } else {
        if (pos == len) {
            this->append(value);
        } else {
            int count = 1;
            Node<T> *prev;
            Node<T> *new_el;
            prev = head;
            tmp = head->next;
            while (count++ != pos) {
                if (tmp) {
                    prev = tmp;
                    tmp = tmp->next;
                } else {
                    throw DescriptiveException("Index error: your list contains less objects");
                }
            }
            addNode(&new_el, value);
            new_el->next = tmp;
            prev->next = new_el;
        }
    }
}


template<typename T>
inline T LinkedList<T>::pop(int pos /* =-1 */) {
    if (len > 0) {
        pos = (pos == -1) ? len - 1 : pos;
        Node<T> *tmp;
        T res;
        if (pos == 0) {
            tmp = head;
            head = head->next;
            len -= 1;
            res = tmp->data;
            delete tmp;
            return (res);
        } else {
            int count = 1;
            Node<T> *prev;
            prev = head;
            tmp = head->next;
            while (count++ != pos) {
                if (tmp) {
                    prev = tmp;
                    tmp = tmp->next;
                } else {
                    throw DescriptiveException("Index error: your list contains less objects");
                }
            }
            prev->next = tmp->next;
            res = tmp->data;
            if (pos == len - 1) {
                tail = prev;
            }
            len -= 1;
            delete tmp;
            return (res);
        }
    } else {
        throw DescriptiveException("Index error: your list contains less objects");
    }
}


template<typename T>
inline T LinkedList<T>::getFirst() {
    if (head) {
        return head->data;
    } else {
        throw DescriptiveException("Index error: your list contains less objects");
    }
}


template<typename T>
inline T LinkedList<T>::getLast() {
    if (tail) {
        return tail->data;
    } else {
        throw DescriptiveException("Index error: your list contains less objects");
    }
}


template<typename T>
inline T LinkedList<T>::operator[](int pos) {
    int count = 0;
    Node<T> *tmp;
    tmp = head;
    while (count++ != pos){
        if (tmp) {
            tmp = tmp->next;
        } else {
            throw DescriptiveException("Index error: your list contains less objects");
        }
    }
    return (tmp->data);
}



template<typename T>
inline void LinkedList<T>::concat(const LinkedList<T> &conc) {
    Node<T> *tmp;
    tmp = conc.head;
    while (tmp) {
        append(tmp->data);
        tmp = tmp->next;
    }
}


template<typename T>
inline void LinkedList<T>::set(int pos, T value) {
    int count = 0;
    Node<T> *tmp;
    tmp = head;
    while (count++ != pos){
        if (tmp) {
            tmp = tmp->next;
        } else {
            throw DescriptiveException("Index error: your list contains less objects");
        }
    }
    tmp->data = value;
}


template<typename T>
inline void LinkedList<T>::removeIf(bool (*funcPrt)(T)) {
    Node<T> *cur, *prev, *tmp;
    cur = head;
    while (cur) {
        if (funcPrt(cur->data)) {
            if(cur == head) {
                prev = head;
                head = head->next;
                cur = head;
                delete prev;
            } else if (cur == tail){
                tail = prev;
                tail->next = nullptr;
                delete cur;
            } else {
                tmp = cur;
                prev->next = cur->next;
                cur = cur->next;
                delete tmp;
            }
            len -= 1;
        } else {
            prev = cur;
            cur = cur->next;
        }
    }
}


#endif
