#ifndef LAB2_DYNAMICARRAY_H
#define LAB2_DYNAMICARRAY_H


#include <iostream>
#include "CustomException.h"


#define STANDART_CAPACITY 10


template <typename T>
class DynamicArray {
public:
    DynamicArray();
    DynamicArray(T* arr, int length);
    DynamicArray(const DynamicArray<T> &assignable);
    ~DynamicArray();
    DynamicArray<T>& operator=(const DynamicArray<T> &assignable);
    void map(T (*funcPtr)(T));
    void append(T value);
    void clear();
    void insert(int pos, T value);
    void remove(T value);
    void print();
    void set(int pos, T value);
    void concat(DynamicArray<T> &conc);
    void removeIf(bool (*funcPrt)(T));
    T operator[](int pos);
    T pop(int pos = -1);
    int getLength();
    int getCapacity();
    DynamicArray<T>& getSubArray(int begin=0, int end=-1);
private:
    T *data = NULL;
    int capacity;
    int len;
    void addMemory(int additionalCap = STANDART_CAPACITY);
    void freeMemory(int additionalCap = STANDART_CAPACITY);
};


template<typename T>
inline void DynamicArray<T>::addMemory(int additionalCap /* =STANDART_CAPACITY */) {
    capacity += additionalCap;
    data = static_cast<T *>(realloc(data, capacity * sizeof(T)));
}


template<typename T>
inline void DynamicArray<T>::freeMemory(int deletedCap /* =STANDART_CAPACITY */) {
    capacity -= deletedCap;
    data = static_cast<T *>(realloc(data, capacity * sizeof(T)));
}


template<typename T>
DynamicArray<T>::DynamicArray()
    : len(0), capacity(STANDART_CAPACITY)
{
    data = static_cast<T *>(malloc(sizeof(T) * capacity));
}


template<typename T>
DynamicArray<T>::DynamicArray(T arr[], int length)
    : len(length), capacity(STANDART_CAPACITY * (length / STANDART_CAPACITY + 1))
{
    data = static_cast<T  *>(malloc(sizeof(T) * capacity));
    for (int i = 0; i < length; i++) {
        data[i] = arr[i];
    }
}


template<typename T>
DynamicArray<T>::DynamicArray(const DynamicArray<T> &assignable)
        : len(0), capacity(0)
{
    (*this) = assignable;
}


template<typename T>
DynamicArray<T>& DynamicArray<T>::operator=(const DynamicArray<T> &assignable) {
    if (&assignable != this) {
        this->clear();
        addMemory(assignable.capacity);
        len = assignable.len;
        for (int i = 0; i < len; i++){
            data[i] = assignable.data[i];
        }
    }
    return (*this);
}


template<typename T>
DynamicArray<T>::~DynamicArray() {
    clear();
}


template<typename T>
inline void DynamicArray<T>::clear() {
    capacity = 0;
    len = 0;
    free(data);
    data = NULL;
}


template<typename T>
inline void DynamicArray<T>::print() {
    for (int i = 0; i < len; i++){
        std::cout << data[i] << ' ';
    }
    std::cout << std::endl;
}


template<typename T>
inline void DynamicArray<T>::append(T value) {
    if (len < capacity) {
        data[len] = value;
        len += 1;
    } else {
        addMemory();
        data[len] = value;
        len += 1;
    }
}


template<typename T>
inline void DynamicArray<T>::map(T (*funcPtr)(T)) {
    for (int i = 0; i < len; i++) {
        data[i] = (*funcPtr)(data[i]);
    }
}


template<typename T>
inline int DynamicArray<T>::getLength() {
    return len;
}


template<typename T>
inline int DynamicArray<T>::getCapacity() {
    return capacity;
}


template<typename T>
inline void DynamicArray<T>::remove(T value) {
    for (int i = 0; i < len; i++){
        if (data[i] == value) {
            for(int j = i+1; j < len; j++) {
                data[j-1] = data[j];
            }
            len -= 1;
            if (capacity - STANDART_CAPACITY == len) {
                freeMemory();
            }
            return;
        }
    }
    throw DescriptiveException("Value error: this value isn`t in your array");
}


template<typename T>
inline T DynamicArray<T>::pop(int pos /* =-1 */) {
    if (pos >= 0) {
        if (pos < len) {
            T value = data[pos];
            --len;
            for (int i = pos; i < len; i++) {
                data[i] = data[i+1];
            }
            if (capacity - STANDART_CAPACITY == len) {
                freeMemory();
            }
            return value;
        } else {
            throw DescriptiveException("Index error: your array contains less objects");
        }
    } else if (pos == -1){
        if (len != 0) {
            T value = data[len - 1];
            len -= 1;
            if (capacity - STANDART_CAPACITY == len) {
                freeMemory();
            }
            return value;
        } else {
            throw DescriptiveException("Index error: your array is empty =(");
        }
    } else {
        throw DescriptiveException("Index error: index should be positive or -1");
    }
}


template<typename T>
inline void DynamicArray<T>::insert(int pos, T value) {
    if (pos < len && pos >= 0) {
        if (len + 1 == capacity){
            addMemory();
        }
        T tmp = data[pos];
        for (int i = len; i > pos; i--) {
            data[i] = data[i-1];
        }
        data[pos] = value;
        len += 1;
    } else if (pos >= len){
        throw DescriptiveException("Index error: your array contains less objects");
    } else {
        throw DescriptiveException("Index error: index should be positive");
    }
}


template<typename T>
inline T DynamicArray<T>::operator[](int pos) {
    if (pos >= len) {
        throw DescriptiveException("Index error: your array contains less objects");
    } else if (pos < 0){
        throw DescriptiveException("Index error: index should be positive");
    } else {
        return data[pos];
    }
}


template<typename T>
inline void DynamicArray<T>::concat(DynamicArray<T> &conc) {
    addMemory(conc.getLength());
    for (int i = len; i < len + conc.getLength(); i++) {
        data[i] = conc[i-len];
    }
    len += conc.getLength();
}


template<typename T>
inline void DynamicArray<T>::set(int pos, T value) {
    if (pos >= len) {
        throw DescriptiveException("Index error: your array contains less objects");
    } else if (pos < 0){
        throw DescriptiveException("Index error: index should be positive");
    } else {
        data[pos] = value;
    }
}


template<typename T>
inline void DynamicArray<T>::removeIf(bool (*funcPrt)(T)) {
    T *frst, *scnd;
    frst = data;
    scnd = data;
    int l = len;
    if (l > 0) {
        while (scnd != (data + l)) {
            if (funcPrt(*scnd)) {
                ++scnd;
                len -= 1;
            } else {
                if (frst != scnd) {
                    (*frst) = (*scnd);
                }
                ++frst;
                ++scnd;
            }
        }
        int newCap = STANDART_CAPACITY * (len / STANDART_CAPACITY + 1);
        if (newCap < capacity) {
            freeMemory(capacity - newCap);
        }
    } else {
        throw DescriptiveException("Your array is empty");
    }
}


template<typename T>
inline DynamicArray<T>& DynamicArray<T>::getSubArray(int begin, int end) {
    if (end == -1) {
        end = len + 1;
    }
    if ((begin < 0) || (end > len + 1) || (begin > end)) {
        throw DescriptiveException("wrong indexes");
    } else {
        DynamicArray<T> *res = new DynamicArray<T>;
        for (int i = begin; i < end; i++) {
            res->append(data[i]);
        }
        return *res;
    }
}


#endif
