#include <iostream>
#include <complex>
#include "DiagonalMatrix.h"
#include <iostream>

using namespace std;


bool aux_palindrom(Node<char> **frst, Node<char> **fnsh) {
    if (*fnsh) {
        bool res = aux_palindrom(frst, &((*fnsh)->next));
        if (res && ((*fnsh)->data == (*frst)->data)) {
            *frst = (*frst)->next;
            return true;
        } else {
            return false;
        }
    }
    return true;
}


bool check_palindrom(Node<char> *data) {
    Node<char> **cur = &data;
    return aux_palindrom(&data, cur);
}


int main() {
    Node<char> *head = nullptr, *tmp;
    string s;
    char c;
    int len = 5;
    while ((c = getchar()) != '\n') {
        if (head) {
            tmp->next = new Node<char>;
            tmp = tmp->next;
            tmp->data = c;
            tmp->next = nullptr;
        } else {
            head = new Node<char>;
            head->next = nullptr;
            head->data = c;
            tmp = head;
        }
    }
    tmp = head;
    while (tmp) {
        cout << tmp->data << ' ';
        tmp = tmp->next;
    }
    cout << endl;
    check_palindrom(head) ? cout << "YES" : cout << "NO";
}

/*
char getType();
int getSize();
char getRandomC();
DiagonalMatrix<int, ArraySequence> *createRandomInt(int size);
DiagonalMatrix<int, ArraySequence> *createInt(int size);
DiagonalMatrix<complex<float>, ArraySequence> *createRandomComp(int size);
DiagonalMatrix<complex<float>, ArraySequence> *createComp(int size);
void addMatrixInt(DiagonalMatrix<int, ArraySequence> *data);
void addMatrixComplex(DiagonalMatrix<complex<float>, ArraySequence> *data);


int main() {
    bool run = true;
    DiagonalMatrix<int, ArraySequence> dataI;
    DiagonalMatrix<complex<float>, ArraySequence> dataC;
    int size;
    char c, t;
    t = getType();
    size = getSize();
    c = getRandomC();

    if (t == 'i') {
        if (c == 'y') {
            dataI = (*createRandomInt(size));
        } else {
            dataI = (*createInt(size));
        }
    } else {
        if (c == 'y') {
            dataC = (*createRandomComp(size));
        } else {
            dataC = (*createComp(size));
        }
    }
    cout << "Change command from list below: " << endl;
    cout << "'q' - exit," << endl;
    cout << "'m' - multiply by number" << endl;
    cout << "'p' - print" << endl;
    cout << "'a' - add another matrix" << endl;
    cout << "'n' - get norm of your matrix" << endl;
    while(run) {
        cout << "Enter your command:" << endl;
        cin >> c;
        switch(c) {
            case 'q':
                run = false;
                break;
            case 'p':
                if (t == 'i') {
                    dataI.print();
                } else {
                    dataC.print();
                }
                break;
            case 'm':
                cout << "Enter your number to multiply by:" << endl;
                if (t == 'i') {
                    int tmp;
                    cin >> tmp;
                    dataI.multiplyByNum(tmp);
                } else {
                    complex<float> tmp;
                    cin >> tmp;
                    dataC.multiplyByNum(tmp);
                }
                break;
            case 'n':
                if (t == 'i') {
                    cout << dataI.getNorm() << endl;
                } else {
                    cout << dataC.getNorm() << endl;
                }
                break;
            case 'a':
                if (t == 'i') {
                    addMatrixInt(&dataI);
                } else {
                    addMatrixComplex(&dataC);
                }
                break;
            default:
                cout << "Enter command from list" << endl;
                break;
        }
    }
}


char getType() {
    char t;
    cout << "Please specify type of elements in your matrix (i or c):" << endl;
    cin >> t;
    while(t != 'i' && t != 'c') {
        cout << "Please, enter i or c" << endl;
        cin >> t;
    }
    return t;
}


int getSize() {
    int size;
    cout << "Enter size of your matrix (positive number): " << endl;
    cin >> size;
    while (size <= 0) {
        cout << "Please, enter a correct value" << endl;
        cin >> size;
    }
    return size;
}

char getRandomC() {
    char c;
    cout << "Do you want to fill it with random numbers? (y or n)" << endl;
    cin >> c;
    while (c != 'y' && c != 'n') {
        cout << "Please< enter yes or no (y or n)" << endl;
        cin >> c;
    }
    return c;
}


DiagonalMatrix<int, ArraySequence> *createRandomInt(int size) {
    int a[size];
    for(int i = 0; i < size; i++) {
        a[i] = rand()%10 + 1;
    }
    DiagonalMatrix<int, ArraySequence> *res = new DiagonalMatrix<int, ArraySequence>(a, size);
    return res;
}


DiagonalMatrix<int, ArraySequence> *createInt(int size) {
    int a[size];
    for(int i = 0; i < size; i++) {
        cin >> a[i];
    }
    DiagonalMatrix<int, ArraySequence> *res = new DiagonalMatrix<int, ArraySequence>(a, size);
    return res;
}


DiagonalMatrix<complex<float>, ArraySequence> *createRandomComp(int size) {
    complex<float> a[size];
    complex<float> tmp;
    for(int i = 0; i < size; i++) {
       tmp = complex<float>(rand()%10+1, rand()%10+1);
       a[i] = tmp;
    }
    DiagonalMatrix<complex<float>, ArraySequence> *res = new DiagonalMatrix<complex<float>, ArraySequence>(a, size);
    return res;
}


DiagonalMatrix<complex<float>, ArraySequence> *createComp(int size) {
    complex<float> a[size];
    for(int i = 0; i < size; i++) {
        cin >> a[i];
    }
    DiagonalMatrix<complex<float>, ArraySequence> *res = new DiagonalMatrix<complex<float>, ArraySequence>(a, size);
    return res;
}


void addMatrixInt(DiagonalMatrix<int, ArraySequence> *data) {
    cout << "Enter another matrix of int type and size " << data->getSize() << ": " << endl;
    int a[data->getSize()];
    for (int i = 0; i < data->getSize(); i++) {
        cin >> a[i];
    }
    DiagonalMatrix<int, ArraySequence> tmp = DiagonalMatrix<int, ArraySequence>(a, data->getSize());
    data->add(tmp);
}


void addMatrixComplex(DiagonalMatrix<complex<float>, ArraySequence> *data) {
    cout << "Enter another matrix of complex type and size " << data->getSize() << ": " << endl;
    complex<float> a[data->getSize()];
    for (int i = 0; i < data->getSize(); i++) {
        cin >> a[i];
    }
    DiagonalMatrix<complex<float>, ArraySequence> tmp = DiagonalMatrix<complex<float>, ArraySequence>(a, data->getSize());
    data->add(tmp);
}*/