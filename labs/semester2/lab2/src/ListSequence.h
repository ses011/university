#ifndef LAB2_LISTSEQUENCE_H
#define LAB2_LISTSEQUENCE_H

#include <iostream>
#include "Sequence.h"

template<typename T>
class ListSequence : public Sequence<T> {
public:
    ListSequence();
    ListSequence(T *data, int length);
    ListSequence(const ListSequence<T> &assignable);
    ListSequence<T>& operator=(const ListSequence<T> &assignable);
    virtual void map(T (*funcPtr)(T)) override;
    virtual void insert(int pos, T value) override;
    virtual void remove(T value) override;
    virtual T pop(int pos = -1) override;
    virtual void print() override;
    virtual void set(int pos, T value) override;
    virtual T operator[](int pos) override;
    virtual int getLength() override;
    virtual void append(T value) override;
    virtual ~ListSequence();
private:
    LinkedList<T> items;
};


template<typename T>
ListSequence<T>::ListSequence()
        : items() {
}


template<typename T>
ListSequence<T>::ListSequence(T *data, int length)
        : items(data, length) {
}


template<typename T>
ListSequence<T>::ListSequence(const ListSequence<T> &assignable) {
    if (&assignable != this) {
        items = LinkedList<T>(assignable.items);
    }
}


template<typename T>
ListSequence<T> & ListSequence<T>::operator=(const ListSequence<T> &assignable) {
    if (&assignable != this) {
        items = assignable.items;
    }
}


template<typename T>
ListSequence<T>::~ListSequence() {
    items.clear();
}


template<typename T>
inline void ListSequence<T>::map(T (*funcPtr)(T)) {
    items.map(funcPtr);
}


template<typename T>
inline void ListSequence<T>::insert(int pos, T value) {
    items.insert(pos, value);
}


template<typename T>
inline T ListSequence<T>::operator[](int pos) {
    return items[pos];
}


template<typename T>
inline void ListSequence<T>::remove(T value) {
    items.remove(value);
}


template<typename T>
inline T ListSequence<T>::pop(int pos /* = -1 */) {
    return items.pop(pos);
}


template<typename T>
inline void ListSequence<T>::set(int pos, T value) {
    items.set(pos, value);
}


template<typename T>
inline void ListSequence<T>::append(T value) {
    items.append(value);
}


template<typename T>
inline void ListSequence<T>::print() {
    items.print();
}


template<typename T>
inline int ListSequence<T>::getLength() {
    return items.getLength();
}

#endif
