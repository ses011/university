#include </home/egor/CLionProjects/LAB2_MAIN/src/DiagonalMatrix.h>
#include <gtest/gtest.h>


TEST(DiagonalMatrixTest, CreateFromArray) {
    //given
    int a[5] = {1, 2, 3, 4, 5};
    DiagonalMatrix<int, ArraySequence> data(a, 5);
    //when
    //then
    EXPECT_EQ(5, data.getSize());
    for(int i = 0; i < data.getSize(); i++) {
        EXPECT_EQ(i+1, data[i]);
    }
}


TEST(DiagonalMatrixTest, GetFunction) {
    //given
    int a[5] = {1, 2, 3, 4, 5};
    DiagonalMatrix<int, ArraySequence> data(a, 5);
    //when
    //then
    EXPECT_EQ(5, data.getSize());
    EXPECT_EQ(3, data[2]);
    EXPECT_THROW(data[12], DescriptiveException);
}


TEST(DiagonalMatrixTest, SetFunction) {
    //given
    int a[5] = {1, 2, 3, 4, 5};
    DiagonalMatrix<int, ArraySequence> data(a, 5);
    //when
    data.set(2, 15);
    //then
    EXPECT_EQ(5, data.getSize());
    EXPECT_EQ(15, data[2]);
    EXPECT_THROW(data.set(123, 123), DescriptiveException);
}


TEST(DiagonalMatrixTest, GetSize) {
    //given
    int a[5] = {1, 2, 3, 4, 5};
    DiagonalMatrix<int, ArraySequence> data(a, 5);
    //when
    //then
    EXPECT_EQ(5, data.getSize());
}


TEST(DiagonalMatrixTest, GetNorm) {
    //given
    int a[5] = {1, 2, 3, 4, 5};
    DiagonalMatrix<int, ArraySequence> data(a, 5);
    //when
    //then
    EXPECT_EQ(7, data.getNorm());
}


TEST(DiagonalMatrixTest, MultyplyByNum) {
    //given
    int a[5] = {1, 2, 3, 4, 5};
    DiagonalMatrix<int, ArraySequence> data(a, 5);
    //when
    data.multiplyByNum(3);
    //then
    EXPECT_EQ(5, data.getSize());
    for(int i = 0; i < data.getSize(); i++) {
        EXPECT_EQ(3*(i+1), data[i]);
    }
}


TEST(DiagonalMatrixTest, AddFunction) {
    //given
    int a[5] = {1, 2, 3, 4, 5};
    DiagonalMatrix<int, ArraySequence> data1(a, 5);
    int b[5] = {5, 4, 3, 2, 1};
    DiagonalMatrix<int, ArraySequence> data2(b, 5);
    //when
    data1.add(data2);
    //then
    EXPECT_EQ(5, data1.getSize());
    for(int i = 0; i < data1.getSize(); i++) {
        EXPECT_EQ(6, data1[i]);
    }
    for(int i = 0; i < data2.getSize(); i++) {
        EXPECT_EQ(5-i, data2[i]);
    }
}


TEST(DiagonalMatrixTest, AddFunctionWithDifferentSizes) {
    //given
    int a[5] = {1, 2, 3, 4, 5};
    DiagonalMatrix<int, ArraySequence> data1(a, 5);
    int b[6] = {5, 4, 3, 2, 1, 0};
    DiagonalMatrix<int, ArraySequence> data2(b, 6);
    //when
    //then
    EXPECT_THROW(data1.add(data2), DescriptiveException);
    EXPECT_THROW(data2.add(data1), DescriptiveException);
}


TEST(DiagonalMatrixTest, AddFunctionWithDifferentTypes) {
    //given
    int a[5] = {1, 2, 3, 4, 5};
    DiagonalMatrix<int, ArraySequence> data1(a, 5);
    int b[5] = {5, 4, 3, 2, 1};
    DiagonalMatrix<int, ListSequence> data2(b, 5);
    //when
    data1.add(data2);
    //then
    EXPECT_EQ(5, data1.getSize());
    for(int i = 0; i < data1.getSize(); i++) {
        EXPECT_EQ(6, data1[i]);
    }
    for(int i = 0; i < data2.getSize(); i++) {
        EXPECT_EQ(5-i, data2[i]);
    }
}