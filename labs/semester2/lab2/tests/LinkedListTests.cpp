#include </home/egor/CLionProjects/LAB2_MAIN/src/LinkedList.h>
#include </home/egor/CLionProjects/LAB2_MAIN/src/CustomException.h>
#include <gtest/gtest.h>


class LinkedListTest : public ::testing::Test {
protected:
    LinkedList<int> data;
    void SetUp() override {
        int a[5] = {1, 2, 3, 4, 5};
        data = LinkedList<int>(a, 5);
    }
    void TearDown() override {
        data.clear();
    }
};


TEST_F(LinkedListTest, CreateEmpty) {
    //given
    LinkedList<int> a;
    //when
    //then
    EXPECT_EQ(0, a.getLength());
}


TEST_F(LinkedListTest, CreateFromArray) {
    //given
    //when
    //then
    EXPECT_EQ(5, data.getLength());
    for (int i = 0; i < data.getLength(); i++) {
        EXPECT_EQ(i+1, data[i]);
    }
    EXPECT_EQ(1, data.getFirst());
    EXPECT_EQ(5, data.getLast());
}


TEST_F(LinkedListTest, CreateFromEmptyArray) {
    //given
    int a[0] = {};
    LinkedList<int> b(a, 0);
    //when
    //then
    EXPECT_EQ(0, b.getLength());
}


TEST_F(LinkedListTest, CreateFromAnotherList) {
    //given
    LinkedList<int> a = data;
    //when
    //then
    EXPECT_EQ(a.getLength(), data.getLength());
    for (int i = 0; i < data.getLength(); i++) {
        EXPECT_EQ(i+1, data[i]);
    }
    EXPECT_EQ(a.getFirst(), data.getFirst());
    EXPECT_EQ(a.getLast(), data.getLast());
}


TEST_F(LinkedListTest, AssignmentOperator) {
    //given
    LinkedList<int> a;
    //when
    a = data;
    //then
    EXPECT_EQ(a.getLength(), data.getLength());
    for (int i = 0; i < data.getLength(); i++) {
        EXPECT_EQ(a[i], data[i]);
    }
}


TEST_F(LinkedListTest, GetFunction) {
    //given
    //when
    //then
    EXPECT_EQ(1, data[0]);
    EXPECT_THROW(data[12], DescriptiveException);
    EXPECT_THROW(data[-12], DescriptiveException);
}


TEST_F(LinkedListTest, GetFirstFunction) {
    //given
    //when
    //then
    EXPECT_EQ(1, data.getFirst());
}


TEST_F(LinkedListTest, GetLastFunction) {
    //given
    //when
    //then
    EXPECT_EQ(5, data.getLast());
}


TEST_F(LinkedListTest, SetFunction) {
    //given
    //when
    data.set(3, 13);
    //then
    EXPECT_EQ(5, data.getLength());
    EXPECT_EQ(13, data[3]);
    EXPECT_THROW(data.set(13, 0), DescriptiveException);
    EXPECT_THROW(data.set(-13, 0), DescriptiveException);
}


TEST_F(LinkedListTest, AppendFunction) {
    //given
    //when
    data.append(6);
    //then
    EXPECT_EQ(6, data.getLength());
    EXPECT_EQ(6, data[5]);
    EXPECT_EQ(6, data.getLast());
}


TEST_F(LinkedListTest, PrependFunction) {
    //given
    //when
    data.prepend(0);
    //then
    EXPECT_EQ(6, data.getLength());
    EXPECT_EQ(0, data[0]);
    EXPECT_EQ(0, data.getFirst());
}


TEST_F(LinkedListTest, InsertFunction) {
    //given
    //when
    data.insert(2, 13);
    //then
    EXPECT_EQ(6, data.getLength());
    EXPECT_EQ(13, data[2]);
    EXPECT_THROW(data.insert(-12, 13), DescriptiveException);
    EXPECT_THROW(data.insert(12, 13), DescriptiveException);
}


TEST_F(LinkedListTest, InsertAtTheBegFunction) {
    //give
    //when
    data.insert(0, -12);
    EXPECT_EQ(6, data.getLength());
    EXPECT_EQ(-12, data[0]);
    EXPECT_EQ(-12, data.getFirst());
}


TEST_F(LinkedListTest, InsertAtTheEndFunction) {
    //given
    //when
    data.insert(5, 6);
    //then
    EXPECT_EQ(6, data.getLength());
    EXPECT_EQ(6, data[5]);
    EXPECT_EQ(6, data.getLast());
}


TEST_F(LinkedListTest, RemoveFunction) {
    //given
    //when
    data.remove(3);
    //then
    EXPECT_EQ(4, data.getLength());
    for (int i = 0; i < data.getLength(); i++) {
        EXPECT_NE(3, data[i]);
    }
    EXPECT_THROW(data.remove(12), DescriptiveException);
    EXPECT_THROW(data.remove(-12), DescriptiveException);
}


TEST_F(LinkedListTest, RemoveFunctionWithEqualValues) {
    //given
    int a[3] = {1, 2, 1};
    LinkedList<int> da(a, 3);
    //when
    da.remove(1);
    //then
    EXPECT_EQ(2, da.getLength());
    EXPECT_EQ(2, da[0]);
    EXPECT_EQ(1, da[1]);
}


TEST_F(LinkedListTest, RemoveAllValueFromList) {
    //given
    int a[2] = {1, 2};
    LinkedList<int> ll(a, 2);
    //when
    ll.remove(2);
    ll.remove(1);
    //then
    EXPECT_EQ(0, ll.getLength());
    EXPECT_THROW(ll.getFirst(), DescriptiveException);
}


TEST_F(LinkedListTest, RemoveFirstValue) {
    //given
    //when
    data.remove(1);
    //then
    EXPECT_EQ(4, data.getLength());
    EXPECT_EQ(2, data.getFirst());
}


TEST_F(LinkedListTest, RemoveLastValue) {
    //given
    //when
    data.remove(5);
    //then
    EXPECT_EQ(4, data.getLength());
    EXPECT_EQ(4, data.getLast());
}


TEST_F(LinkedListTest, PopFunction) {
    //given
    //when
    int a = data.pop(2);
    //then
    EXPECT_EQ(a, 3);
    EXPECT_EQ(4, data.getLength());
    for (int i = 0; i < data.getLength(); i++) {
        EXPECT_NE(a, data[i]);
    }
    EXPECT_THROW(data.pop(12), DescriptiveException);
    EXPECT_THROW(data.pop(-12), DescriptiveException);
}


TEST_F(LinkedListTest, PopFromEmtyList) {
    //given
    LinkedList<int> a;
    //when
    //then
    EXPECT_THROW(a.pop(), DescriptiveException);
    EXPECT_THROW(a.pop(1), DescriptiveException);
}


TEST_F(LinkedListTest, PopFirstValue) {
    //give
    //when
    int a = data.pop(0);
    //then
    EXPECT_EQ(4, data.getLength());
    EXPECT_EQ(1, a);
    EXPECT_EQ(2, data.getFirst());
}


TEST_F(LinkedListTest, PopLastValue) {
    //given
    //when
    int a = data.pop();
    //then
    EXPECT_EQ(4, data.getLength());
    EXPECT_EQ(5, a);
    EXPECT_EQ(4, data.getLast());
}


TEST_F(LinkedListTest, Concationation) {
    //given
    int b[3] = {12, 13, 14};
    LinkedList<int> data2(b, 3);
    //when
    data.concat(data2);
    //then
    ASSERT_EQ(8, data.getLength());
    EXPECT_EQ(14, data.getLast());
    EXPECT_EQ(14, data[7]);
    EXPECT_EQ(13, data[6]);
    EXPECT_EQ(12, data[5]);
}


int dual(int a){
    return a*a;
}


TEST_F(LinkedListTest, MapFunction) {
    //given
    //when
    data.map(&dual);
    //then
    EXPECT_EQ(5, data.getLength());
    for (int i = 0; i < data.getLength(); i++) {
        EXPECT_EQ(dual(i+1), data[i]);
    }
}