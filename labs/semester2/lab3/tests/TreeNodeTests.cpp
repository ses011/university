#include </home/egor/CLionProjects/lab3/src/TreeNode.h>
#include </home/egor/CLionProjects/LAB2_MAIN/src/DynamicArray.h>
#include <gtest/gtest.h>


class TreeNodesTests : public ::testing::Test {
protected:
    Node<int> *tree = nullptr;
    Node<int> *createTree(int *a, int len);
    Node<int> *auxAdd(Node<int> *parent, int value);
    void SetUp() override {
        int a[5] = {1, 2, 3, 4, 5};
        tree = createTree(a, 5);
    }
    void TearDown() override {
        deleteNodes(tree);
    }
};


Node<int> * TreeNodesTests::auxAdd(Node<int> *parent, int value) {
    if (!parent) {
        return new Node<int>(value);
    }
    if (value < parent->data) {
        parent->left = auxAdd(parent->left, value);
    } else {
        parent->right = auxAdd(parent->right, value);
    }
    return balance(parent);
}


Node<int> * TreeNodesTests::createTree(int *a, int len) {
    Node<int> *result = nullptr;
    for (int i = 0; i < len; i++) {
        result = auxAdd(result, a[i]);
    }
    return balance(result);
}


TEST_F(TreeNodesTests, CheckHeight) {
    //given
    DynamicArray<Node<int> *> frstLvl;
    frstLvl.append(tree->left);
    frstLvl.append(tree->right->right);
    frstLvl.append(tree->right->left);

    DynamicArray<Node<int> *> scndLvl;
    scndLvl.append(tree->right);

    DynamicArray<Node<int> *> thirdLvl;
    thirdLvl.append(tree);
    //when
    //then
    for (int i = 0; i < 3; i++) {
        EXPECT_EQ(frstLvl[i]->height, 1);
    }
    EXPECT_EQ(scndLvl[0]->height, 2);
    EXPECT_EQ(thirdLvl[0]->height, 3);
}


TEST_F(TreeNodesTests, RotateLeft) {
    //given
    int a[3] = {1, 2, 3};
    Node<int> *node = createTree(a, 3);
    //when
    node = rotateLeft(node);
    //then
    EXPECT_EQ(node->data, 3);
    EXPECT_EQ(node->height, 3);
    EXPECT_EQ(node->left->data, 2);
    EXPECT_EQ(node->left->height, 2);
    EXPECT_EQ(node->left->left->data, 1);
    EXPECT_EQ(node->left->left->height, 1);
}


TEST_F(TreeNodesTests, RotateRight) {
    //given
    int a[3] = {1, 2, 3};
    Node<int> *node = createTree(a, 3);
    //when
    node = rotateRight(node);
    //then
    EXPECT_EQ(node->data, 1);
    EXPECT_EQ(node->height, 3);
    EXPECT_EQ(node->right->data, 2);
    EXPECT_EQ(node->right->height, 2);
    EXPECT_EQ(node->right->right->data, 3);
    EXPECT_EQ(node->right->right->height, 1);
}


TEST_F(TreeNodesTests, Balance3RightNodes) {
    //given
    Node<int> *node = new Node<int>(1);
    node->right = new Node<int>(2);
    node->right->height = 2;
    node->right->right = new Node<int>(3);
    //when
    Node<int> *node1 = balance(node);
    //then
    EXPECT_EQ(node1->data, 2);
    EXPECT_EQ(node1->height, 2);

    deleteNodes(node);
}


TEST_F(TreeNodesTests, Balance3LeftNodes) {
    //given
    Node<int> *node = new Node<int>(3);
    node->left = new Node<int>(2);
    node->left->height = 2;
    node->left->left = new Node<int>(1);
    //when
    Node<int> *node1 = balance(node);
    //then
    EXPECT_EQ(node1->data, 2);
    EXPECT_EQ(node1->height, 2);

    deleteNodes(node);
}


TEST_F(TreeNodesTests, DeleteLeave) {
    //give
    //when
    tree = remove(tree, 1);
    //then
    EXPECT_EQ(tree->data, 4);
    EXPECT_EQ(tree->height, 3);
    EXPECT_EQ(tree->left->data, 2);
    EXPECT_EQ(tree->left->right->data, 3);
    EXPECT_EQ(tree->right->data, 5);
    EXPECT_EQ(tree->left->left, nullptr);
    EXPECT_TRUE(balanceFactor(tree) == -1);
}


TEST_F(TreeNodesTests, DeleteRoot) {
    //given
    //when
    tree = remove(tree, 2);
    //then
    EXPECT_EQ(tree->data, 3);
    EXPECT_TRUE(balanceFactor(tree) == 1);
    EXPECT_EQ(tree->left->data, 1);
    EXPECT_EQ(tree->right->data, 4);
}


TEST_F(TreeNodesTests, AddLeave) {
    //give
    Node<int> *newTree = new Node<int>(1);
    newTree->height = 2;
    newTree->right = new Node<int>(2);
    //when
    newTree = auxAdd(newTree, 3);
    //then
    EXPECT_EQ(newTree->data, 2);
    EXPECT_EQ(balanceFactor(newTree), 0);
}



