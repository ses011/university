#include </home/egor/CLionProjects/lab3/src/AVLTree.h>
#include </home/egor/CLionProjects/LAB2_MAIN/src/DynamicArray.h>
#include <gtest/gtest.h>


class AVLTreeTests : public ::testing::Test {
protected:
    AVLTree<int> tree;
    void SetUp() override {
        int a[10] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        tree = AVLTree<int>(a, 10);
    }
    void TearDown() override {
    }
};


TEST_F(AVLTreeTests, CreateEmpty) {
    //given
    AVLTree<int> tree1;
    //when
    DynamicArray<int> res = tree1.saveToArray();
    //then
    EXPECT_EQ(res.getLength(), 0);
}


TEST_F(AVLTreeTests, CreateFromArray) {
    //given
    //when
    DynamicArray<int> res = tree.saveToArray();
    //then
    ASSERT_EQ(res.getLength(), 10);
    for(int i = 0; i < res.getLength(); i++) {
        EXPECT_EQ(res[i], i+1);
    }
}


TEST_F(AVLTreeTests, AssignmentOperator) {
    //given
    AVLTree<int> tree1;
    tree1 = tree;
    //when
    //then
    for(int i = 1; i < 11; i++) {
        EXPECT_TRUE(tree1.search(i));
    }
}


TEST_F(AVLTreeTests, CopyingConstructor) {
    //given
    AVLTree<int> tree1 = tree;
    //when
    //then
    for(int i = 1; i < 11; i++) {
        EXPECT_TRUE(tree1.search(i));
    }
}



TEST_F(AVLTreeTests, SaveToArray) {
    //given
    //when
    DynamicArray<int> res = tree.saveToArray();
    //then
    ASSERT_EQ(res.getLength(), 10);
    for(int i = 0; i < res.getLength(); i++) {
        EXPECT_EQ(res[i], i+1);
    }
}


TEST_F(AVLTreeTests, SearchValueTrue) {
    //given
    //when
    //then
    for (int i = 1; i < 11; i++) {
        EXPECT_TRUE(tree.search(i));
    }
}


TEST_F(AVLTreeTests, SearchValueFalse) {
    //given
    //when
    //then
    EXPECT_FALSE(tree.search(11));
    EXPECT_FALSE(tree.search(123));
}


TEST_F(AVLTreeTests, AddValue) {
    //given
    //when
    tree.add(11);
    //then
    EXPECT_TRUE(tree.search(11));
}


TEST_F(AVLTreeTests, DeleteValue) {
    //given
    //when
    tree.del(6);
    ASSERT_ANY_THROW(tree.del(123));
    //then
    EXPECT_FALSE(tree.search(6));
}


TEST_F(AVLTreeTests, DeleteAllValues) {
    //given
    int b[3] = {1, 2, 2};
    AVLTree<int> tree1(b, 3);
    //when
    tree1.del(1);
    tree1.del(2);
    tree1.del(2);
    //then
    EXPECT_FALSE(tree1.search(1));
    EXPECT_FALSE(tree1.search(2));
}


TEST_F(AVLTreeTests, IsSubTreeTest) {
    //given
    int b[6] = {5, 6, 7, 8, 9, 10};
    AVLTree<int> tree1(b, 6);
    int c[3] = {1, 2, 14};
    AVLTree<int> tree2(c, 3);
    //when
    //then
    EXPECT_TRUE(tree.isSubTree(tree1));
    EXPECT_FALSE(tree.isSubTree(tree2));
}


int sum(int a, int b) {
    return a+b;
}


TEST_F(AVLTreeTests, Reduce) {
    //given
    //when
    int res = tree.reduce(&sum, 1);
    //then
    EXPECT_EQ(res, 56);
}


int square(int a) {
    return a*a;
}


TEST_F(AVLTreeTests, Map) {
    //given
    //when
    tree.map(&square);
    //then
    for(int i = 1; i < 11; i++) {
        EXPECT_TRUE(tree.search(i*i));
    }
}


TEST_F(AVLTreeTests, GetSubTree) {
    //given
    //when
    AVLTree<int> tree1 = tree.getSubTree(2);
    AVLTree<int> tree2 = tree.getSubTree(8);
    //then
    for(int i = 1; i < 4; i++) {
        EXPECT_TRUE(tree1.search(i));
    }
    for(int i = 4; i < 11; i++) {
        EXPECT_FALSE(tree1.search(i));
    }
    for(int i = 5; i < 11; i++) {
        EXPECT_TRUE(tree2.search(i));
    }
    for(int i = 1; i < 5; i++) {
        EXPECT_FALSE(tree2.search(i));
    }
}




