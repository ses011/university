#ifndef SRC_CUSTOMEXCEPTION_H
#define SRC_CUSTOMEXCEPTION_H


#include <iostream>


class DescriptiveException : public std::exception {
public:
    DescriptiveException(std::string const &message) : msg_(message) {}
    virtual char const *what() const noexcept { return msg_.c_str(); }
private:
    std::string msg_;
};


#endif
