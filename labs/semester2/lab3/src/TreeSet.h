#ifndef SRC_TREESET_H
#define SRC_TREESET_H


#include <cstdlib>
#include <iostream>
#include "TreeNode.h"
#include </home/egor/CLionProjects/LAB2_MAIN/src/CustomException.h>
#include </home/egor/CLionProjects/LAB2_MAIN/src/DynamicArray.h>


template <typename T>
class TreeSet {
public:
    TreeSet();
    TreeSet(T *arr, int len);
    TreeSet(const TreeSet<T> &assignable);
    TreeSet<T> &operator=(const TreeSet<T> &assignable);
    ~TreeSet();
    void insert(T value);
    void del(T value);
    bool search(T value);
    void map(T (*funcPtr)(T));
    T reduce(T (*funcPtr)(T, T), T value);
    bool isEqualTo(const TreeSet<T> &verifiable) const;
    bool contains(const TreeSet<T> &verifiable) const;
    DynamicArray<T> saveToArray() const;
protected:
    Node<T> *head;
private:
    Node<T> *auxInsert(Node<T> *parent, T value);
    Node<T> *auxSearch(Node<T> *parent, T value);
    void auxMap(Node<T> **parent, T (*funcPtr)(T));
    T auxReduce(T (*funcPtr)(T, T), Node<T> *parent, T value);
    void auxSave(DynamicArray<T> *buffer, Node<T> *parent) const;
};


template <typename T>
TreeSet<T>::TreeSet()
: head(nullptr){}


template <typename T>
TreeSet<T>::TreeSet(T *arr, int len)
: head(nullptr) {
    for (int i = 0; i < len; i++) {
        head = auxInsert(head, arr[i]);
    }
}


template <typename T>
TreeSet<T>::TreeSet(const TreeSet<T> &assignable)
: head(nullptr) {
    (*this) = assignable;
}


template <typename T>
TreeSet<T>::~TreeSet<T>() {
    head = deleteNodes(head);
}


template <typename T>
TreeSet<T> & TreeSet<T>::operator=(const TreeSet<T> &assignable) {
    if (&assignable != this) {
        head = deleteNodes(head);
        Node<T> *newhead = copy(assignable.head);
        this->head = newhead;
    }
    return (*this);
}


template <typename T>
Node<T> * TreeSet<T>::auxInsert(Node<T> *parent, T value) {
    if (!parent) {
        return new Node<T>(value);
    }
    if (value < parent->data) {
        parent->left = auxInsert(parent->left, value);
    } else if (value > parent->data) {
        parent->right = auxInsert(parent->right, value);
    } else {
        throw DescriptiveException("This value is already in the set");
    }
    return balance(parent);
}


template <typename T>
void TreeSet<T>::insert(T value) {
    head = auxInsert(head, value);
}


template <typename T>
void TreeSet<T>::del(T value) {
    head = remove(head , value);
}


template <typename T>
Node<T> * TreeSet<T>::auxSearch(Node<T> *parent, T value) {
    if (! parent) {
        return nullptr;
    }
    if (value == parent->data) {
        return parent;
    }
    if (value > parent->data) {
        return auxSearch(parent->right, value);
    } else {
        return auxSearch(parent->left, value);
    }
}


template <typename T>
bool TreeSet<T>::search(T value) {
    return auxSearch(head, value) != nullptr;
}


template <typename T>
T TreeSet<T>::auxReduce(T (*funcPtr)(T, T), Node<T> *parent, T value) {
    if (parent) {
        T x1 = auxReduce(funcPtr, parent->left, value);
        T x2 = auxReduce(funcPtr, parent->right, x1);
        return funcPtr(x2, parent->data);
    }
    return value;
}


template <typename T>
T TreeSet<T>::reduce(T (*funcPtr)(T, T), T value) {
    T result = auxReduce(funcPtr, head, value);
    return result;
}


template <typename T>
void TreeSet<T>::auxMap(Node<T> **parent, T (*funcPtr)(T)) {
    if (*parent) {
        auxMap(&((*parent)->left), funcPtr);
        (*parent)->data = funcPtr((*parent)->data);
        auxMap(&((*parent)->right), funcPtr);
    }
}


template <typename T>
void TreeSet<T>::map(T (*funcPtr)(T)) {
    auxMap(&head, funcPtr);
}


template <typename T>
void TreeSet<T>::auxSave(DynamicArray<T> *buffer, Node<T> *parent) const {
    if (parent) {
        auxSave(buffer, parent->left);
        buffer->append(parent->data);
        auxSave(buffer, parent->right);
    }
}


template <typename T>
DynamicArray<T> TreeSet<T>::saveToArray() const {
    DynamicArray<T> result;
    auxSave(&result, head);
    return (result);
}


template <typename T>
bool TreeSet<T>::isEqualTo(const TreeSet<T> &verifiable) const {
    if (&verifiable == this) {
        return true;
    }
    DynamicArray<T> frst = saveToArray();
    DynamicArray<T> scnd = verifiable.saveToArray();
    if (frst.getLength() != scnd.getLength()) {
        return false;
    }
    for (int i = 0; i < frst.getLength(); i++) {
        if (frst[i] != scnd [i]) {
            return false;
        }
    }
    return true;
}


template <typename T>
bool TreeSet<T>::contains(const TreeSet<T> &verifiable) const {
    if (&verifiable == this) {
        return true;
    }
    DynamicArray<T> frst = saveToArray();
    DynamicArray<T> scnd = verifiable.saveToArray();
    if (frst.getLength() < scnd.getLength()) {
        return false;
    }
    bool res = true;
    int j = 0;
    for (int i = 0; i < scnd.getLength(); i++) {
        while(frst[j] != scnd[i]) {
            j++;
            if (j == frst.getLength()) {
                res = false;
                break;
            }
        }
    }
    return res;
}

#endif
