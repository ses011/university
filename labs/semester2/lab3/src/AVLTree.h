#ifndef SRC_AVLTREE_H
#define SRC_AVLTREE_H

#include <cstdlib>
#include <iostream>
#include "TreeNode.h"
#include </home/egor/CLionProjects/LAB2_MAIN/src/CustomException.h>
#include </home/egor/CLionProjects/LAB2_MAIN/src/DynamicArray.h>


template <typename T>
class AVLTree {
public:
    AVLTree();
    AVLTree(T *arr, int len);
    AVLTree(const AVLTree<T> &assignable);
    AVLTree<T> &operator=(const AVLTree<T> &assignable);
    ~AVLTree();
    T reduce(T (*funcPtr)(T, T), T value);
    AVLTree<T> getSubTree(T value);
    void map(T (*funcPtr)(T));
    bool search(T value);
    bool isSubTree(const AVLTree<T> &verifiable);
    void add(T value);
    void del(T value);
    void printSort();
    void print(std::string c);
    void clear();
    int getHeight();
    DynamicArray<T> saveToArray() const;
protected:
    Node<T> *head;
private:
    Node<T> * auxSearch(Node <T> *parent, T value);
    Node<T> * auxAdd(Node<T> *parent, T value);
    void auxPrintSort(Node<T> *parent);
    void auxMap(Node<T> **parent, T (*funcPtr)(T));
    void auxSave(DynamicArray<T> *buffer, Node<T> *parent) const;
    T auxReduce(T (*funcPtr)(T, T), Node<T> *parent, T value);
    bool auxSubTree(Node<T> *main, Node<T> *verifiable);

    void RLP(Node<T> *parent);
    void RPL(Node<T> *parent);
    void LRP(Node<T> *parent);
    void LPR(Node<T> *parent);
    void PRL(Node<T> *parent);
    void PLR(Node<T> *parent);
};


template <typename T>
AVLTree<T>::AVLTree()
: head(nullptr) {}


template <typename T>
AVLTree<T>::AVLTree(T *arr, int len)
: head(nullptr) {
    for (int i = 0; i < len; i++) {
        head = auxAdd(head, arr[i]);
    }
}


template <typename T>
AVLTree<T>::AVLTree(const AVLTree<T> &assignable)
: head(nullptr) {
    (*this) = assignable;
}


template <typename T>
AVLTree<T>::~AVLTree<T>() {
    head = deleteNodes(head);
}


template <typename T>
Node<T> * AVLTree<T>::auxAdd(Node<T> *parent, T value) {
    if (!parent) {
        return new Node<T>(value);
    }
    if (value < parent->data) {
        parent->left = auxAdd(parent->left, value);
    } else {
        parent->right = auxAdd(parent->right, value);
    }
    return balance(parent);
}


template <typename T>
void AVLTree<T>::add(T value) {
    head = balance(auxAdd(head, value));
}


template <typename T>
void AVLTree<T>::del(T value) {
    head = remove(head , value);
}


template <typename T>
Node<T> * AVLTree<T>::auxSearch(Node<T> *parent, T value) {
    if (! parent) {
        return nullptr;
    }
    if (value == parent->data) {
        return parent;
    }
    if (value > parent->data) {
        return auxSearch(parent->right, value);
    } else {
        return auxSearch(parent->left, value);
    }
}


template <typename T>
bool AVLTree<T>::search(T value) {
    return auxSearch(head, value) != nullptr;
}


template <typename T>
void AVLTree<T>::auxPrintSort(Node<T> *parent) {
    if (parent) {
        auxPrintSort(parent->left);
        std::cout << parent->data << ' ';
        auxPrintSort(parent->right);
    }
}


template <typename T>
void AVLTree<T>::printSort() {
    if (head) {
        auxPrintSort(head);
        std::cout << std::endl;
    } else {
        throw DescriptiveException("This tree is empty");
    }
}


template <typename T>
void AVLTree<T>::auxMap(Node<T> **parent, T (*funcPtr)(T)) {
    if (*parent) {
        auxMap(&((*parent)->left), funcPtr);
        (*parent)->data = funcPtr((*parent)->data);
        auxMap(&((*parent)->right), funcPtr);
    }
}


template <typename T>
void AVLTree<T>::map(T (*funcPtr)(T)) {
    auxMap(&head, funcPtr);
}


template <typename T>
AVLTree<T> AVLTree<T>::getSubTree(T value) {
    Node<T> *newHead = auxSearch(head, value);
    if (! newHead) {
        throw DescriptiveException("This value isn`t in tree =(");
    } else {
        AVLTree<T> result;
        result.head = copy(newHead);
        return (result);
    }
}


template <typename T>
AVLTree<T> & AVLTree<T>::operator=(const AVLTree<T> &assignable) {
    if (&assignable != this) {
        head = deleteNodes(head);
        Node<T> *newhead = copy(assignable.head);
        this->head = newhead;
    }
    return (*this);
}


template <typename T>
T AVLTree<T>::auxReduce(T (*funcPtr)(T, T), Node<T> *parent, T value) {
    if (parent) {
        T x1 = auxReduce(funcPtr, parent->left, value);
        T x2 = auxReduce(funcPtr, parent->right, x1);
        return funcPtr(x2, parent->data);
    }
    return value;
}


template <typename T>
T AVLTree<T>::reduce(T (*funcPtr)(T, T), T value) {
    T result = auxReduce(funcPtr, head, value);
    return result;
}


template <typename T>
void AVLTree<T>::RLP(Node<T> *parent) {
    if (parent) {
        if (parent->right) {
            std::cout << '[';
            RLP(parent->right);
            std::cout << ']';
        }
        if (parent->left) {
            std::cout << '{';
            RLP(parent->left);
            std::cout << '}';
        }
        std::cout << parent->data;
    }
}


template <typename T>
void AVLTree<T>::RPL(Node<T> *parent) {
    if (parent) {
        if (parent->right) {
            std::cout << '[';
            RPL(parent->right);
            std::cout << ']';
        }
        std::cout << parent->data;
        if (parent->left) {
            std::cout << '{';
            RPL(parent->left);
            std::cout << '}';
        }
    }
}


template <typename T>
void AVLTree<T>::LRP(Node<T> *parent) {
    if (parent) {
        if (parent->left) {
            std::cout << '{';
            LRP(parent->left);
            std::cout << '}';
        }
        if (parent->right) {
            std::cout << '[';
            LRP(parent->right);
            std::cout << ']';
        }
        std::cout << parent->data;
    }
}


template <typename T>
void AVLTree<T>::LPR(Node<T> *parent) {
    if (parent) {
        if (parent->left) {
            std::cout << '{';
            LPR(parent->left);
            std::cout << '}';
        }
        std::cout << parent->data;
        if (parent->right) {
            std::cout << '[';
            LPR(parent->right);
            std::cout << ']';
        }
    }
}

template <typename T>
void AVLTree<T>::PLR(Node<T> *parent) {
    if (parent) {
        std::cout << parent->data;
        if (parent->left) {
            std::cout << '{';
            PLR(parent->left);
            std::cout << '}';
        }
        if (parent->right) {
            std::cout << '[';
            PLR(parent->right);
            std::cout << ']';
        }
    }
}


template <typename T>
void AVLTree<T>::PRL(Node<T> *parent) {
    if (parent) {
        std::cout << parent->data;
        if (parent->right) {
            std::cout << '[';
            PRL(parent->right);
            std::cout << ']';
        }
        if (parent->left) {
            std::cout << '{';
            PRL(parent->left);
            std::cout << '}';
        }
    }
}


template <typename T>
void AVLTree<T>::print(std::string c) {
    if (c == "RLP") {
        RLP(head);
    } else if (c == "RPL") {
        RPL(head);
    } else if (c == "LRP") {
        LRP(head);
    } else if (c == "PRL") {
        PRL(head);
    } else if (c == "PLR") {
        PLR(head);
    } else if (c == "LPR") {
        LPR(head);
    } else {
        throw DescriptiveException("This formatting string is incorrect");
    }
    std::cout << std::endl;
}


template <typename T>
bool AVLTree<T>::auxSubTree(Node<T> *main, Node<T> *verifiable) {
    if ((! main) && (! verifiable)) {
        return true;
    } else if (main && verifiable) {
        return auxSubTree(main->left, verifiable->left) &&
               auxSubTree(main->right, verifiable->right) && (main->data == verifiable->data);
    } else {
        return false;
    }
}


template <typename T>
bool AVLTree<T>::isSubTree(const AVLTree<T> &verifiable) {
    Node<T> *verMain = auxSearch(head, verifiable.head->data);
    Node<T> *verHead = verifiable.head;
    if (! verMain) {
        return false;
    }
    return auxSubTree(verMain, verHead);
}


template <typename T>
void AVLTree<T>::auxSave(DynamicArray<T> *buffer, Node<T> *parent) const {
    if (parent) {
        auxSave(buffer, parent->left);
        buffer->append(parent->data);
        auxSave(buffer, parent->right);
    }
}


template <typename T>
DynamicArray<T> AVLTree<T>::saveToArray() const {
    DynamicArray<T> result;
    auxSave(&result, head);
    return (result);
}


template <typename T>
void AVLTree<T>::clear() {
    head = deleteNodes(head);
    head = nullptr;
}


template <typename T>
int AVLTree<T>::getHeight() {
    return head->height;
}


#endif
