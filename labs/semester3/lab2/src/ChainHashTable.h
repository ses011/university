#ifndef SRC_CHAINHASHTABLE_H
#define SRC_CHAINHASHTABLE_H


#include "/home/egor/CLionProjects/LAB2_MAIN/src/Sequence.h"
#include "/home/egor/CLionProjects/lab3/src/TreeSet.h"
#include "/home/egor/CLionProjects/LAB2_MAIN/src/CustomException.h"
#include "HashTableCell.h"
#include <cmath>
#include <iostream>


#define STANDART_CAPACITY 16
#define HASHING_CONSTANT 0.6180339887 // using for fibonacci hashing


template<typename TKey, typename TElement>
class ChainHashTable{
public:
    explicit ChainHashTable(int capacity_ = STANDART_CAPACITY);
    ChainHashTable(Sequence<TKey> *keys, Sequence<TElement> *elems);
    ChainHashTable(const ChainHashTable<TKey, TElement> &assignable);
    ChainHashTable<TKey, TElement>& operator=(const ChainHashTable<TKey, TElement> &assignable);
    int getSize();
    int getCapacity();
    bool containsKey(TKey key);
    TElement get(TKey key);
    void clear();
    void add(TKey key, TElement elem);
    void remove(TKey key);
    ~ChainHashTable();

    friend std::ostream& operator<< (std::ostream &out, ChainHashTable<TKey, TElement> &h) {
        out << '{';
        for (int i = 0; i < h.capacity; i++) {
            if (h.data[i]) {
                DynamicArray<HashTableCell<TKey, TElement>> res = h.data[i]->saveToArray();
                if (res.getLength() != 0) {
                    out << res << ", ";
                }
            }
        }
        out << '\b' << '\b' << "" << "";
        out << '}';
        return out;
    };

private:
    int hash(int key);
    int hash(std::string key);
    int capacity = 0;
    int size = 0;
    TreeSet<HashTableCell<TKey, TElement>> **data = nullptr;
};


template<typename TKey, typename TElement>
int ChainHashTable<TKey, TElement>::hash(int key) {
    int h = capacity * fmod(key * HASHING_CONSTANT, 1);
    return h;
}


template<typename TKey, typename TElement>
int ChainHashTable<TKey, TElement>::hash(std::string key) {
    int code = 0;
    for (int i = 0; i < key.size(); i++) {
        code += int(key[i]);
    }
    return hash(code);
}


template<typename TKey, typename TElement>
ChainHashTable<TKey, TElement>::ChainHashTable(int capacity_)
: size(0) {
    if (capacity_ <= 0) {
        throw DescriptiveException("Capacity should be positive");
    } else {
        capacity = capacity_;
    }
    data = new TreeSet<HashTableCell<TKey, TElement>> * [capacity];
    for (int i = 0; i < capacity; i++) {
        data[i] = nullptr;
    }
}


template<typename TKey, typename TElement>
ChainHashTable<TKey, TElement>::ChainHashTable(Sequence<TKey> *keys, Sequence<TElement> *elems) : size(0) {
    int keysLen = keys->getLength();
    int elemsLen = elems->getLength();
    if (keysLen != elemsLen) {
        throw DescriptiveException("Error: sequences should have equals size");
    } else {
        capacity = STANDART_CAPACITY;
        data = new TreeSet<HashTableCell<TKey, TElement>> * [capacity];
        capacity = keysLen;
        for (int i = 0; i < capacity; i++) {
            add((*keys)[i], (*elems)[i]);
        }
    }
}


template<typename TKey, typename TElement>
ChainHashTable<TKey, TElement>::ChainHashTable(const ChainHashTable<TKey, TElement> &assignable) :size(0), capacity(0) {
    (*this) = assignable;
}


template<typename TKey, typename TElement>
ChainHashTable<TKey, TElement>& ChainHashTable<TKey, TElement>::operator=(const ChainHashTable<TKey, TElement> &assignable) {
    if (&assignable != this) {
        this->clear();
        capacity = assignable.capacity;
        size = assignable.size;
        data = new TreeSet<HashTableCell<TKey, TElement>> * [assignable.capacity];
        for (int i = 0; i < capacity; i++) {
            data[i] = nullptr;
        }
        for (int i = 0; i < capacity; i++) {
            if (assignable.data[i]) {
                data[i] = new TreeSet<HashTableCell<TKey, TElement>>;
                (*data[i]) = (*assignable.data[i]);
            }
        }
    }
    return (*this);
}


template<typename TKey, typename TElement>
int ChainHashTable<TKey, TElement>::getSize() {
    return size;
}


template<typename TKey, typename TElement>
int ChainHashTable<TKey, TElement>::getCapacity() {
    return capacity;
}


template<typename TKey, typename TElement>
bool ChainHashTable<TKey, TElement>::containsKey(TKey key) {
    int pos = hash(key);
    if (data[pos]) {
        HashTableCell<TKey, TElement> el(key);
        return data[pos]->search(el);
    } else {
        return false;
    }
}


template<typename TKey, typename TElement>
TElement ChainHashTable<TKey, TElement>::get(TKey key) {
    int pos = hash(key);
    if (data[pos]) {
        HashTableCell<TKey, TElement> el(key);
        try {
            HashTableCell<TKey, TElement> res = data[pos]->get(el);
            return res.getElement();
        } catch (...) {
            throw DescriptiveException("Key error: this table isn`t contains this key");
        }
    } else {
        throw DescriptiveException("Key error: this table isn`t contains this key");
    }
}


template<typename TKey, typename TElement>
void ChainHashTable<TKey, TElement>::clear() {
    for (int i = 0; i < capacity; i++) {
        if (data[i]) {
            data[i]->clear();
        }
    }
    if (data) {
        delete data;
    }
    data = nullptr;
}


template<typename TKey, typename TElement>
void ChainHashTable<TKey, TElement>::add(TKey key, TElement elem) {
    int pos = hash(key);
    HashTableCell<TKey, TElement> el(key, elem);
    if (data[pos]) {
        try {
            data[pos]->insert(el);
        } catch (...) {
            throw DescriptiveException("Key error: this key is already in hash table");
        }
    } else {
        data[pos] = new TreeSet<HashTableCell<TKey, TElement>>;
        try {
            data[pos]->insert(el);
        } catch (...) {
            throw DescriptiveException("Key error: this key is already in hash table");
        }
    }
    size += 1;
}


template<typename TKey, typename TElement>
void ChainHashTable<TKey, TElement>::remove(TKey key) {
    int pos = hash(key);
    if (data[pos]) {
        HashTableCell<TKey, TElement> el(key);
        try {
            data[pos]->del(el);
            size -= 1;
        } catch (...) {
            throw DescriptiveException("Key error: this table isn`t contains this key");
        }
    } else {
        throw DescriptiveException("Key error: this table isn`t contains this key");
    }
}


template<typename TKey, typename TElement>
ChainHashTable<TKey, TElement>::~ChainHashTable() {
    clear();
}


#endif
