#ifndef SRC_IDICTIONARY_H
#define SRC_IDICTIONARY_H


#include "IChainHashTable.h"
#include "ChainHashTable.h"


template<typename TKey, typename TElement>
class IDictionary {
public:
    virtual int getSize() = 0;
    virtual int getCapacity() = 0;
    virtual TElement get(TKey key) = 0;
    virtual bool containsKey(TKey key) = 0;
    virtual void add(TKey key, TElement elem) = 0;
    virtual void remove(TKey key) = 0;
    virtual ChainHashTable<TKey, TElement>& getTable() = 0;
    ~IDictionary() = default;
};


template<typename TKey, typename TElement>
std::ostream& operator<<(std::ostream &out, IDictionary<TKey, TElement> &h) {
    ChainHashTable<TKey, TElement> resp = h.getTable();
    out << resp;
    return out;
};


#endif
