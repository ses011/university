#ifndef SRC_HASHTABLECELL_H
#define SRC_HASHTABLECELL_H

#include <iostream>


template <typename TKey, typename TElement>
class HashTableCell {
public:
    HashTableCell();
    HashTableCell(TKey key_);
    HashTableCell(TKey key_, TElement elem);
    TKey getKey() const;
    TElement getElement() const;
    HashTableCell<TKey, TElement>& operator=(const HashTableCell<TKey, TElement> &c1);
    bool operator== (const HashTableCell &c1) const;
    bool operator!= (const HashTableCell &c1) const;
    bool operator> (const HashTableCell &c1) const;
    bool operator>= (const HashTableCell &c1) const;
    bool operator< (const HashTableCell &c1) const;
    bool operator<= (const HashTableCell &c1) const;
    ~HashTableCell() = default;

    friend std::ostream& operator<< (std::ostream &out, HashTableCell<TKey, TElement> c1) {
        out << c1.key << ": " << c1.element;
        return out;
    };
private:
    TKey key;
    TElement element;
};


template<typename TKey, typename TElement>
HashTableCell<TKey, TElement>::HashTableCell() : key(), element() {
}


template<typename TKey, typename TElement>
HashTableCell<TKey, TElement>::HashTableCell(TKey key_) {
    key = key_;
}


template <typename TKey, typename TElement>
HashTableCell<TKey, TElement>::HashTableCell(TKey key_, TElement elem) {
    key = key_;
    element = elem;
}


template <typename TKey, typename TElement>
TKey HashTableCell<TKey, TElement>::getKey() const {
    return key;
}


template <typename TKey, typename TElement>
TElement HashTableCell<TKey, TElement>::getElement() const {
    return element;
}


template <typename TKey, typename TElement>
bool HashTableCell<TKey, TElement>::operator==(const HashTableCell<TKey, TElement> &c1) const {
    return (this->getKey() == c1.getKey());
}


template <typename TKey, typename TElement>
bool HashTableCell<TKey, TElement>::operator!=(const HashTableCell<TKey, TElement> &c1) const {
    return (this->getKey() != c1.getKey());
}


template <typename TKey, typename TElement>
bool HashTableCell<TKey, TElement>::operator>(const HashTableCell<TKey, TElement> &c1) const {
    return (this->getKey() > c1.getKey());
}


template <typename TKey, typename TElement>
bool HashTableCell<TKey, TElement>::operator>=(const HashTableCell<TKey, TElement> &c1) const {
    return (this->getKey() >= c1.getKey());
}


template <typename TKey, typename TElement>
bool HashTableCell<TKey, TElement>::operator<(const HashTableCell<TKey, TElement> &c1) const {
    return (this->getKey() < c1.getKey());
}


template <typename TKey, typename TElement>
bool HashTableCell<TKey, TElement>::operator<=(const HashTableCell<TKey, TElement> &c1) const {
    return (this->getKey() <= c1.getKey());
}


template <typename TKey, typename TElement>
HashTableCell<TKey, TElement>& HashTableCell<TKey, TElement>::operator=(const HashTableCell<TKey, TElement> &c1) {
    if (&c1 != this) {
        key = c1.getKey();
        element = c1.getElement();
    }
    return (*this);
}



#endif
