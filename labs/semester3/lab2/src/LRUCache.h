#ifndef SRC_LRUCACHE_H
#define SRC_LRUCACHE_H


#include <fstream>
#include "IChainHashTable.h"
#include "TimeQueue.h"
#include "/home/egor/CLionProjects/LAB2_MAIN/src/Sequence.h"


template<typename T>
class LRUCache {
public:
    LRUCache(int capacity = STANDART_CAPACITY /* = 16 */);
    ~LRUCache();
    bool containsKey(T key);
    void add(T key, T result);
    T get(T key);

    friend std::ostream& operator<< (std::ostream &out, LRUCache<T> &cache) {
        out << (*cache.table);
        return out;
    };

private:
    int currentTime;
    TimeQueue<T> timeQueue;
    IDictionary<T, T> *table;
};


template<typename T>
LRUCache<T>::LRUCache(int capacity): currentTime(0), timeQueue() {
    table = new IChainHashTable<T, T>(capacity);
}


template<typename T>
LRUCache<T>::~LRUCache<T>() {
    timeQueue.clear();
    delete table;
}


template<typename T>
bool LRUCache<T>::containsKey(T key) {
    return table->containsKey(key);
}


template<typename T>
void LRUCache<T>::add(T key, T result) {
    if (table->getSize() == table->getCapacity()) {
        T minKey = timeQueue.extractMinValue();
        table->remove(minKey);
    }
    try {
        table->add(key, result);
        timeQueue.add(key, currentTime);
    } catch (...) {
        if (result != table->get(key)) {
            table->remove(key);
            table->add(key, result);
        }
        timeQueue.set(key, currentTime);
    }
    currentTime += 1;
}


template<typename T>
T LRUCache<T>::get(T key) {
    try {
        timeQueue.set(key, currentTime);
        currentTime += 1;
        return table->get(key);
    } catch (...) {
        throw DescriptiveException("Key error: your cache isn`t contains this key");
    }
}


#endif