#include <ctime>
#include <string>
#include <cmath>
#include <unistd.h>
#include <iostream>
#include <map>
#include <random>
#include "LRUCache.h"
#include "Timer.h"
#include "/home/egor/CLionProjects/LAB2_MAIN/src/ArraySequence.h"


#define TESTS_NUM 4

int TIMER = 0;
//should set priority not in cache
//fix problems


std::string calculate(std::string key) {
    unsigned int microseconds = 1000;
    usleep(microseconds);
    return key;
}


std::string calculateWithCache(std::string key, LRUCache<std::string> &cache) {
    if (cache.containsKey(key)) {
        return cache.get(key);
    }
    std::string res = calculate(key);
    cache.add(key, res, TIMER);
    TIMER += 1;
    return res;
}

/*
void test1() {
    std::cout << "1) Cache vs simple operation test. Cache hit is 0%." << std::endl;

    clock_t start;
    clock_t end;
    double time;

    for (int k = 1; k <= TESTS_NUM; k++) {
        int param = (int) pow(10, k);
        std::cout << "\tTesting with " << param << " elements:" << std::endl;

        LRUCache<int> cache(1024);
        start = clock();
        for (int i = 0; i < param; i++) {
            calculateWithCache(i, cache);
        }
        end = clock();
        time = (double) (end - start) / CLOCKS_PER_SEC * 100.0;
        std::cout << "\t\tWith cache: " << time << " sec." << std::endl;

        start = clock();
        for (int i = 0; i < param; i++) {
            calculate(i);
        }
        end = clock();
        time = (double) (end - start) / CLOCKS_PER_SEC * 100.0;
        std::cout << "\t\tWithout cache: " << time << " sec." << std::endl;
    }
}


void test2() {
    std::cout << "2) Cache vs simple operation test. Cache hit is about 50%." << std::endl;

    srand(time(nullptr));
    clock_t start;
    clock_t end;
    double time;

    for (int k = 1; k <= TESTS_NUM; k++) {
        int param = (int) pow(10, k);
        std::cout << "\tTesting with " << param << " elements:" << std::endl;

        LRUCache<int> cache(1024);
        int tmp;
        start = clock();
        for (int i = 0; i < param; i++) {
            tmp = rand() % 1500 + 1;
            calculateWithCache(tmp, cache);
        }
        end = clock();
        time = (double) (end - start) / CLOCKS_PER_SEC * 100.0;
        std::cout << "\t\tWith cache: " << time << " sec." << std::endl;

        start = clock();
        for (int i = 0; i < param; i++) {
            calculate(i % 1750);
        }
        end = clock();
        time = (double) (end - start) / CLOCKS_PER_SEC * 100.0;
        std::cout << "\t\tWithout cache: " << time << " sec." << std::endl;
    }
}


void test3() {
    std::cout << "3) Cache vs simple operation test. Cache hit is 100%." << std::endl;

    clock_t start;
    clock_t end;
    double time;

    for (int k = 1; k <= TESTS_NUM; k++) {
        int param = (int) pow(10, k);
        std::cout << "\tTesting with " << param << " elements:" << std::endl;

        LRUCache<int> cache(1024);
        int tmp;
        start = clock();
        for (int i = 0; i < param; i++) {
            calculateWithCache(i % 1000, cache);
        }
        end = clock();
        time = (double) (end - start) / CLOCKS_PER_SEC * 100.0;
        std::cout << "\t\tWith cache: " << time << " sec." << std::endl;

        start = clock();
        for (int i = 0; i < param; i++) {
            calculate(i % 1000);
        }
        end = clock();
        time = (double) (end - start) / CLOCKS_PER_SEC * 100.0;
        std::cout << "\t\tWithout cache: " << time << " sec." << std::endl;
    }
} */


void getData(std::string fileName, Sequence<std::string> *result) {
    std::string s;
    std::ifstream file(fileName);
    while(getline(file, s)){
        result->append(s);
    }
    file.close();
}


int getNormalRandomNum(int min, int max) {
    std::random_device rd{};
    std::mt19937 gen{rd()};
    // values near the mean are the most likely
    // standard deviation affects the dispersion of generated values from the mean
    std::normal_distribution<> d{50,20};
    auto result = static_cast<int>(std::round(d(gen)));
    if (result < min) {
        result = -result;
    }
    if (result > max) {
        result -= max;
    }
    return result;
}


void testRealData() {
    std::cout << "4) Test with real data" << std::endl;

    Sequence<std::string> *data = new ArraySequence<std::string>;
    getData("/home/egor/CLionProjects/LAB2_SEM3/src/urls", data);

    Timer t;
    LRUCache<std::string> cache(40);
    for (int i = 0; i < 1000; i++) {
        std::string currentUrl = (*data)[getNormalRandomNum(0, 99)];
        calculateWithCache(currentUrl, cache);
    }
    std::cout << "\t\tWith cache: " << t.elapsed() << " sec." << std::endl;

    t.reset();
    for (int i = 0; i < 1000; i++) {
        std::string currentUrl = (*data)[getNormalRandomNum(0, 99)];
        calculate(currentUrl);
    }
    std::cout << "\t\tWithout cache: " << t.elapsed() << " sec." << std::endl;

    std::cout << cache << std::endl;
}


/*
void testAccessTime() {
    Sequence<std::string> *data = new ArraySequence<std::string>;
    getData("/home/egor/CLionProjects/LAB2_SEM3/src/urls", data);
    clock_t start;
    clock_t end;
    double time;
    LRUCache<std::string> cache(32);
    for (int i = 0; i < 1000; i++) {
        std::string currentUrl = (*data)[getNormalRandomNum(0, 99)];
        calculateWithCache(currentUrl, cache);
    }

    start = clock();
    for (int i = 0; i < 1000; i++) {
        cache.containsKey(i % 100);
    }
    end = clock();
    std::cout << (double) (end - start) << std::endl;
} */


int main() {
    //test1();
    //test2();
    //test3();
    //testRealData();
    //TimeQueue<int> a(10);
    //a.add(1, 2);
    LRUCache<std::string> a(3);
    a.add("123", "abc", 1);
    //a.add("567", "def", 2);
    //a.add("890", "mnk", 3);
    std::cout << a;
}
