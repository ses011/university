#ifndef SRC_ICHAINHASHTABLE_H
#define SRC_ICHAINHASHTABLE_H


#include "IDictionary.h"


template<typename TKey, typename TElement>
class IChainHashTable : public IDictionary<TKey, TElement> {
public:
    IChainHashTable();
    IChainHashTable(int capacity);
    IChainHashTable(Sequence<TKey> *keys, Sequence<TElement> *elems);
    IChainHashTable(const IChainHashTable<TKey, TElement> &assignable);
    IChainHashTable<TKey, TElement>& operator=(const IChainHashTable<TKey, TElement> &assignable);
    virtual int getSize() override;
    virtual int getCapacity() override;
    virtual TElement get(TKey key) override;
    virtual bool containsKey(TKey key) override;
    virtual void add(TKey key, TElement elem) override;
    virtual void remove(TKey key) override;
    virtual ~IChainHashTable();
    virtual ChainHashTable<TKey, TElement>& getTable() override;
private:
    ChainHashTable<TKey, TElement> data;
};


template<typename TKey, typename TElement>
IChainHashTable<TKey, TElement>::IChainHashTable(): data() {}


template<typename TKey, typename TElement>
IChainHashTable<TKey, TElement>::IChainHashTable(int capacity): data(capacity) {}


template<typename TKey, typename TElement>
IChainHashTable<TKey, TElement>::IChainHashTable(Sequence<TKey> *keys, Sequence<TElement> *elems)
    : data(keys, elems) {}


template<typename TKey, typename TElement>
IChainHashTable<TKey, TElement>::IChainHashTable(const IChainHashTable<TKey, TElement> &assignable) {
    if (&assignable != this) {
        data = ChainHashTable<TKey, TElement>(assignable.data);
    }
}


template<typename TKey, typename TElement>
IChainHashTable<TKey, TElement>& IChainHashTable<TKey, TElement>::
        operator=(const IChainHashTable<TKey, TElement> &assignable) {
    if (&assignable != this) {
        data = assignable.data;
    }
}


template<typename TKey, typename TElement>
IChainHashTable<TKey, TElement>::~IChainHashTable() {
    data.clear();
}


template<typename TKey, typename TElement>
int IChainHashTable<TKey, TElement>::getSize() {
    return data.getSize();
}


template<typename TKey, typename TElement>
int IChainHashTable<TKey, TElement>::getCapacity() {
    return data.getCapacity();
}


template<typename TKey, typename TElement>
TElement IChainHashTable<TKey, TElement>::get(TKey key) {
    return data.get(key);
}


template<typename TKey, typename TElement>
bool IChainHashTable<TKey, TElement>::containsKey(TKey key) {
    return data.containsKey(key);
}


template<typename TKey, typename TElement>
void IChainHashTable<TKey, TElement>::add(TKey key, TElement elem) {
    data.add(key, elem);
}


template<typename TKey, typename TElement>
void IChainHashTable<TKey, TElement>::remove(TKey key) {
    data.remove(key);
}

template<typename TKey, typename TElement>
ChainHashTable<TKey, TElement>& IChainHashTable<TKey, TElement>::getTable() {
    return data;
}


#endif
