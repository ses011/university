#ifndef SRC_TIMEQUEUE_H
#define SRC_TIMEQUEUE_H
#include "HashTableCell.h"
#include "/home/egor/CLionProjects/LAB2_MAIN/src/CustomException.h"


template<typename T>
struct TimeQueueNode {
    TimeQueueNode(T data_, int priority_): data(data_), priority(priority_) {
        next = nullptr;
    };
    TimeQueueNode *next;
    T data;
    int priority;
};


template<typename T>
class TimeQueue {
public:
    TimeQueue();
    void set(T elem, int priority);
    void add(T elem, int priority);
    void clear();
    T extractMinValue();
    int getSize();
    ~TimeQueue();
private:
    TimeQueueNode<T> *head;
    TimeQueueNode<T> *tail;
};


template<typename T>
TimeQueue<T>::TimeQueue() {
    head = nullptr;
    tail = nullptr;
}


template<typename T>
void TimeQueue<T>::clear() {
    if (head) {
        TimeQueueNode<T> *tmp;
        while (head) {
            tmp = head;
            head = head->next;
            delete tmp;
        }
    }
}


template<typename T>
TimeQueue<T>::~TimeQueue<T>() {
    clear();
}


template<typename T>
void TimeQueue<T>::add(T elem, int priority) {
    if (tail) {
        tail->next = new TimeQueueNode<T>(elem, priority);
        tail = tail->next;
        tail->next = nullptr;
    } else {
        head = new TimeQueueNode<T>(elem, priority);
        tail = head;
    }
}


template<typename T>
void TimeQueue<T>::set(T elem, int priority) {
    if (head) {
        TimeQueueNode<T> *tmp = head;
        while (tmp->data != elem) {
            if (tmp->next) {
                tmp = tmp->next;
            } else {
                throw DescriptiveException("Your TimeQueue isn`t contains this key");
            }
        }
        tmp->priority = priority;
    } else {
        throw DescriptiveException("Your TimeQueue is free");
    }
}


template<typename T>
T TimeQueue<T>::extractMinValue() {
    if (head) {
        T minElem = head->data;
        TimeQueueNode<T> *tmp = head;
        head = head->next;
        delete tmp;
        return minElem;
    } else {
        throw DescriptiveException("Your TimeQueue is free");
    }
}


template<typename T>
int TimeQueue<T>::getSize() {
    int size = 0;
    if (head) {
        TimeQueueNode<T> *tmp = head;
        while (tmp) {
            size += 1;
            tmp = tmp->next;
        }
    }
    return size;
}


#endif
