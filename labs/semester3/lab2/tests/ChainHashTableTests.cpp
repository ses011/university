#include </home/egor/CLionProjects/LAB2_SEM3/src/ChainHashTable.h>
#include </home/egor/CLionProjects/LAB2_MAIN/src/ArraySequence.h>
#include <gtest/gtest.h>


class ChainHashTableTest : public ::testing::Test {
protected:
    ChainHashTable<int, int> data;
    void SetUp() override {
        data.add(1, 6);
        data.add(2, 7);
        data.add(3, 8);
        data.add(4, 9);
        data.add(5, 10);
    }
    void TearDown() override {
    };
};


TEST_F(ChainHashTableTest, CreateEmpty) {
    //given
    ChainHashTable<int, int> a;
    //when
    //then
    EXPECT_EQ(0, a.getSize());
    EXPECT_EQ(16, a.getCapacity());
}


TEST_F(ChainHashTableTest, CreateEmptyCapacity) {
    //given
    ChainHashTable<int, int> a(33);
    //when
    //then
    EXPECT_EQ(0, a.getSize());
    EXPECT_EQ(33, a.getCapacity());
}


TEST_F(ChainHashTableTest, CreateFromSequence) {
    //given;
    int a[5] = {1, 2, 3, 4, 5};
    Sequence<int> *keys = new ArraySequence<int>(a, 5);
    int b[5] = {5, 4, 3, 2, 1};
    Sequence<int> *elems = new ArraySequence<int>(b, 5);
    ChainHashTable<int, int> table(keys, elems);
    //when
    //then
    ASSERT_EQ(5, table.getSize());
    for (int i = 0; i < 5; i++) {
        ASSERT_TRUE(table.containsKey(i+1));
        EXPECT_EQ(5-i, table.get(i+1));
    }
}


TEST_F(ChainHashTableTest, CreateFromEmptySequence) {
    //given;
    int a[0] = {};
    Sequence<int> *keys = new ArraySequence<int>(a, 0);
    int b[0] = {};
    Sequence<int> *elems = new ArraySequence<int>(b, 0);
    ChainHashTable<int, int> table(keys, elems);
    //when
    //then
    ASSERT_EQ(0, table.getCapacity());
    ASSERT_EQ(0, table.getSize());
}


TEST_F(ChainHashTableTest, CreateFromAnotherTable) {
    //given
    ChainHashTable<int, int> a = data;
    //when
    //then
    ASSERT_EQ(5, a.getSize());
    ASSERT_EQ(16, a.getCapacity());
    for (int i = 1; i < 6; i++) {
        ASSERT_TRUE(a.containsKey(i));
        EXPECT_EQ(5+i, a.get(i));
    }
}


TEST_F(ChainHashTableTest, AssignmentOperator) {
    //given
    ChainHashTable<int, int> table;
    //when
    table = data;
    //then
    ASSERT_EQ(5, table.getSize());
    ASSERT_EQ(16, table.getCapacity());
    for (int i = 1; i < 6; i++) {
        ASSERT_TRUE(table.containsKey(i));
        EXPECT_EQ(5+i, table.get(i));
    }
}


TEST_F(ChainHashTableTest, ContainsKeyFunction) {
    //given
    //when
    //then
    for (int i = 1; i < 6; i++) {
        EXPECT_TRUE(data.containsKey(i));
    }
    EXPECT_FALSE(data.containsKey(123));
}


TEST_F(ChainHashTableTest, GetFunction) {
    //given
    //when
    //then
    for (int i = 1; i < 6; i++) {
        EXPECT_EQ(5+i, data.get(i));
    }
    EXPECT_ANY_THROW(data.get(123));
    EXPECT_ANY_THROW(data.get(-123));
}


TEST_F(ChainHashTableTest, AddFunctionEmptyTable) {
    //given
    ChainHashTable<int, int> table;
    //when
    table.add(1, 1);
    //then
    EXPECT_EQ(1, table.getSize());
    EXPECT_EQ(1, table.get(1));
}


TEST_F(ChainHashTableTest, AddFunction) {
    //given
    //when
    data.add(123, 123);
    //then
    EXPECT_EQ(6, data.getSize());
    EXPECT_EQ(123, data.get(123));
}


TEST_F(ChainHashTableTest, AddFunctionWrongCases) {
    //given
    //when
    //then
    ASSERT_ANY_THROW(data.add(1, 6));
    ASSERT_ANY_THROW(data.add(3, 2));
}


TEST_F(ChainHashTableTest, RemoveFunction) {
    //given
    //when
    data.remove(2);
    //then
    ASSERT_EQ(4, data.getSize());
    ASSERT_ANY_THROW(data.get(2));
    ASSERT_ANY_THROW(data.remove(123));
}


TEST_F(ChainHashTableTest, RemoveFromEmpty) {
    //given
    ChainHashTable<int, int> a;
    //when
    //then
    ASSERT_ANY_THROW(a.remove(123));
}


TEST_F(ChainHashTableTest, RemoveAllPairs) {
    //given
    //when
    data.remove(1);
    data.remove(2);
    data.remove(3);
    data.remove(4);
    data.remove(5);
    //then
    ASSERT_EQ(0, data.getSize());
    ASSERT_ANY_THROW(data.get(3));
}


TEST_F(ChainHashTableTest, AddAfterRemove) {
    //given
    //when
    data.remove(3);
    data.add(3, 13);
    //then
    EXPECT_EQ(5, data.getSize());
    EXPECT_EQ(13, data.get(3));
}


TEST_F(ChainHashTableTest, Collisions) {
    //give
    for(int i = 6; i < 30; i++) {
        data.add(i, 30-i);
    }
    //when
    //then
    ASSERT_EQ(29, data.getSize());
    for(int i = 1; i < 30; i++) {
        EXPECT_TRUE(data.containsKey(i));
    }
}

