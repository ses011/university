#include </home/egor/CLionProjects/LAB2_SEM3/src/TimeQueue.h>
#include <gtest/gtest.h>


class TimeQueueTest : public ::testing::Test {
protected:
    TimeQueue<int> data;
    void SetUp() override {
        data.add(1, 6);
        data.add(2, 7);
        data.add(3, 8);
        data.add(4, 9);
        data.add(5, 10);
    }
    void TearDown() override {
    };
};


TEST_F(TimeQueueTest, CreateEmpty) {
    //given
    TimeQueue<int> q;
    //when
    //then
    ASSERT_EQ(0, q.getSize());
}


TEST_F(TimeQueueTest, AddFunction) {
    //given
    TimeQueue<int> q;
    //when
    q.add(3, 123);
    //then
    ASSERT_EQ(1, q.getSize());
    EXPECT_EQ(3, q.extractMinValue());
}


TEST_F(TimeQueueTest, AddSomeVals) {
    //given
    //when
    //then
    ASSERT_EQ(5, data.getSize());
}


TEST_F(TimeQueueTest, ExtractMinValue) {
    //given
    //when
    int val = data.extractMinValue();
    //then
    ASSERT_EQ(4, data.getSize());
    EXPECT_EQ(1, val);
}


TEST_F(TimeQueueTest, ExtractMinFromEmpty) {
    //given
    TimeQueue<int> q;
    //given
    //then
    EXPECT_ANY_THROW(q.extractMinValue());
}


TEST_F(TimeQueueTest, SetFunction) {
    //given
    //when
    data.set(4, 0);
    //then
    ASSERT_EQ(5, data.getSize());
    EXPECT_EQ(1, data.extractMinValue());
}


TEST_F(TimeQueueTest, SetFunctionEmpty) {
    //given
    TimeQueue<int> q;
    //given
    //then
    EXPECT_ANY_THROW(q.set(123, 123));
}


TEST_F(TimeQueueTest, SetFunctionWrongValue) {
    //given
    //when
    //then
    ASSERT_ANY_THROW(data.set(444, 444));
}
