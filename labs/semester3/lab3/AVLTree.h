#ifndef LAB3_SEM3_AVLTREE_H
#define LAB3_SEM3_AVLTREE_H


#include <cstdlib>
#include <vector>
#include </home/egor/CLionProjects/LAB2_MAIN/src/CustomException.h>
#include "TreeNode.h"



class AVLTree {
public:
    AVLTree();
    ~AVLTree();
    TreeNode<AVLTree> * get(std::string &keys);
    std::vector<int> search(std::vector<std::string> &keys, int pos = 0);
    void add(std::vector<std::string> &keys, int pos, int filePosition);
    void clear();
protected:
    TreeNode<AVLTree> *head;
private:
    TreeNode<AVLTree> *searchWithinTree(TreeNode<AVLTree> *parent, std::string &key);
    TreeNode<AVLTree> *auxSearch(TreeNode<AVLTree> *parent, std::vector<std::string> &value, int pos);
    TreeNode<AVLTree> *auxAdd(TreeNode<AVLTree> *parent, std::vector<std::string> &value, int pos, int filePosition);
};


#endif
