#include "AVLTree.h"
#include "TreeNode.h"
#include <vector>

AVLTree::AVLTree()
        : head(nullptr) {}


AVLTree::~AVLTree() {
    head = deleteNodes(head);
}


TreeNode<AVLTree> *AVLTree::auxAdd(TreeNode<AVLTree> *parent, std::vector<std::string> &value, int pos, int filePosition) {
    if (! parent) {
        return new TreeNode<AVLTree>(value, pos, filePosition);
    }
    if (value[pos] < parent->key) {
        parent->left = auxAdd(parent->left, value, pos, filePosition);
    } else {
        parent->right = auxAdd(parent->right, value, pos, filePosition);
    }
    return balance(parent);
}


void AVLTree::add(std::vector<std::string> &keys, int pos, int filePosition) {
    if (pos == keys.size() - 1) {
        try {
            TreeNode<AVLTree> *tmp = this->get(keys[pos]);
            tmp->positions.push_back(filePosition);
        } catch (...) {
            head = balance(auxAdd(head, keys, pos, filePosition));
        }
    } else {
        try {
            TreeNode<AVLTree> *tmp = this->get(keys[pos]);
            if (tmp->data) {
                tmp->data->add(keys, pos + 1, filePosition);
            } else {
                head = balance(auxAdd(head, keys, pos, filePosition));
            }
        } catch (...) {
            head = balance(auxAdd(head, keys, pos, filePosition));
        }
    }
}


TreeNode<AVLTree> *AVLTree::auxSearch(TreeNode<AVLTree> *parent, std::vector<std::string> &value, int pos) {
    if (! parent) {
        throw DescriptiveException("key error");
    }
    if (value[pos] == parent->key) {
        if (pos == value.size() - 1) {
            return parent;
        } else {
            try {
                auto tmp = parent->data;
                return tmp->auxSearch(tmp->head, value, pos + 1);
            } catch (...) {
                throw DescriptiveException("key error");
            }
        }
    }
    if (value[pos] > parent->key) {
        return auxSearch(parent->right, value, pos);
    } else {
        return auxSearch(parent->left, value, pos);
    }
}


std::vector<int> AVLTree::search(std::vector<std::string> &keys, int pos) {
    return auxSearch(head, keys, pos)->positions;
}


void AVLTree::clear() {
    head = deleteNodes(head);
    head = nullptr;
}


TreeNode<AVLTree> *AVLTree::searchWithinTree(TreeNode<AVLTree> *parent, std::string &key) {
    if (! parent) {
        throw DescriptiveException("key error");
    }
    if (key == parent->key) {
        return parent;
    }
    if (key > parent->key) {
        return searchWithinTree(parent->right, key);
    } else {
        return searchWithinTree(parent->left, key);
    }
}


TreeNode<AVLTree> * AVLTree::get(std::string &keys) {
    TreeNode<AVLTree> *res = searchWithinTree(head, keys);
    if (res) {
        return res;
    } else {
        throw DescriptiveException("This value isn`t in set");
    }
}

