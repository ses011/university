#ifndef LAB3_SEM3_TREENODE_H
#define LAB3_SEM3_TREENODE_H

#include </home/egor/CLionProjects/LAB2_MAIN/src/CustomException.h>
//#include "AVLTree.h"


//class AVLTree;

template <typename T>
struct TreeNode {
    std::string key;
    std::vector<int> positions;
    T *data;
    unsigned int height;
    TreeNode *left;
    TreeNode *right;
    TreeNode(std::string &_key) {
        key = _key;
        data = nullptr;
        left = right = nullptr;
        height = 1;
    }
    TreeNode(std::vector<std::string> &keys, int position, int filePosition) {
        key = keys[position];
        left = right = nullptr;
        height = 1;
        if (position == keys.size() - 1) {
            positions.push_back(filePosition);
        } else {
            data = new T;
            data->add(keys, position + 1, filePosition);
        }
    }
};


template<typename T>
unsigned int height(TreeNode<T> *parent) {
    return parent ? parent->height : 0;
}


template<typename T>
int balanceFactor(TreeNode<T> *parent) {
    return height(parent->right) - height(parent->left);
}


template<typename T>
void correctHeight(TreeNode<T> *parent) {
    unsigned int h1 = height(parent->left);
    unsigned int h2 = height(parent->right);
    parent->height = (h1 > h2 ? h1 : h2) + 1;
}


template<typename T>
TreeNode<T> *rotateRight(TreeNode<T> *parent) {
    TreeNode<T> *tmp = parent->left;
    parent->left = tmp->right;
    tmp->right = parent;
    correctHeight(parent);
    correctHeight(tmp);
    return tmp;
}


template<typename T>
TreeNode<T> *rotateLeft(TreeNode<T> *parent) {
    TreeNode<T> *tmp = parent->right;
    parent->right = tmp->left;
    tmp->left = parent;
    correctHeight(parent);
    correctHeight(tmp);
    return tmp;
}


template<typename T>
TreeNode<T> *balance(TreeNode<T> *parent) {
    correctHeight(parent);
    if (balanceFactor(parent) == 2) {
        if (balanceFactor(parent->right) < 0) {
            parent->right = rotateRight(parent->right);
        }
        return rotateLeft(parent);
    }
    if (balanceFactor(parent) == -2) {
        if (balanceFactor(parent) > 0) {
            parent->left = rotateLeft(parent->left);
        }
        return rotateRight(parent);
    }
    return parent;
}


template<typename T>
TreeNode<T> *findMin(TreeNode<T> *parent) {
    return parent->left ? findMin(parent->left) : parent;
}


template<typename T>
TreeNode<T> *removeMin(TreeNode<T> *parent) {
    if (! parent->left) {
        return parent->right;
    }
    parent->left = removeMin(parent->left);
    return balance(parent);
}


template<typename T>
TreeNode<T> *remove(TreeNode<T> *parent, std::string value) {
    if (! parent) {
        throw DescriptiveException("Your list does not contains this value");
    }
    if (value < parent->key) {
        parent->left = remove(parent->left, value);
    } else if (parent->key < value) {
        parent->right = remove(parent->right, value);
    } else {
        TreeNode<T> *left = parent->left;
        TreeNode<T> *right = parent->right;
        delete parent;
        if (! right) {
            return left;
        }
        TreeNode<T> *minR = findMin(right);
        minR->right = removeMin(right);
        minR->left = left;
        return balance(minR);
    }
    return balance(parent);
}


template<typename T>
TreeNode<T> *deleteNodes(TreeNode<T> *parent) {
    if (parent != nullptr) {
        deleteNodes(parent->left);
        deleteNodes(parent->right);
        delete parent->data;
        delete parent;
    }
    return nullptr;
}


#endif
