#ifndef LAB3_CAR_H
#define LAB3_CAR_H


#include <string>
//firm, year, mileage, type, color, engine, power, gearbox, drive, owners, price, number

class Car {
public:
    Car(std::string _firm = "", int _year = -1, int _mileage = -1, std::string _type = "", std::string _color = "",
        std::string _engine = "", int _power = -1, std::string _gearbox = "", std::string _drive = "",
        int _owners = -1, int _price = -1, int _number = -1)
        : firm(_firm),
          year(_year),
          mileage(_mileage),
          type(_type),
          color(_color),
          engine(_engine),
          power(_power),
          gearbox(_gearbox),
          drive(_drive),
          owners(_owners),
          price(_price),
          number(_number) {};

    Car(std::string buffer[12]) {
        {
            firm = buffer[0];
            year = std::stoi(buffer[1]);
            mileage = std::stoi(buffer[2]);
            type = buffer[3];
            color = buffer[4];
            engine = buffer[5];
            power = std::stoi(buffer[6]);
            gearbox = buffer[7];
            drive = buffer[8];
            owners = std::stoi(buffer[9]);
            price = std::stoi(buffer[10]);
            number = std::stoi(buffer[11]);
        }
    };
    ~Car() = default;
    bool operator== (Car &c1) {return c1.getNumber() == number;};

    void setFirm(std::string _firm) {firm = _firm;}
    void setYear(int _year) {year = _year;}
    void setType(std::string _type) {type = _type;}
    void setMileage(int _mileage) {mileage = _mileage;}
    void setColor(std::string _color) {color = _color;}
    void setEngine(std::string _engine) {engine = _engine;}
    void setPower(int _power) {power = _power;}
    void setGearbox(std::string _gearbox) {gearbox = _gearbox;}
    void setDrive(std::string _drive) {drive = _drive;}
    void setOwners(int _owners) {owners = _owners;}
    void setPrice(int _price) {price = _price;}
    void setNumber(int _number) {number = _number;}

    std::string getFirm() {return firm;}
    int getYear() {return year;}
    std::string getType() {return type;}
    int getMileage() {return mileage;}
    std::string getColor() {return color;}
    std::string getEngine() {return engine;}
    int getPower() {return power;}
    std::string getGearbox() {return gearbox;}
    std::string getDrive() {return drive;}
    int getOwners() {return owners;}
    int getPrice() {return price;}
    int getNumber() {return number;}

    Car& operator=(Car c) {
      if (&c != this) {
          firm = c.getFirm();
          year = c.getYear();
          type = c.getType();
          mileage = c.getMileage();
          color = c.getColor();
          engine = c.getEngine();
          power = c.getPower();
          gearbox = c.getGearbox();
          drive = c.getDrive();
          owners = c.getOwners();
          price = c.getPrice();
          number = c.getNumber();
      }
      return (*this);
    };
    friend std::ostream& operator<<(std::ostream &out, Car &c) {
        out << "Car(" << c.getNumber() << ", " << c.getFirm() << ", " << c.getYear() << ", " << c.getColor() << ")";
        return out;
    }
private:
    std::string firm;
    int year;
    std::string type;
    int mileage;
    std::string color;
    std::string engine;
    int power;
    std::string gearbox;
    std::string drive;
    int owners;
    int price;
    int number;
};


#endif