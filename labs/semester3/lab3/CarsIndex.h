#ifndef LAB3_CARSINDEX_H
#define LAB3_CARSINDEX_H


#include <fstream>
#include <vector>
#include "Car.h"
#include "/home/egor/CLionProjects/LAB2_SEM3/src/IChainHashTable.h"


class CarsIndex {
public:
    CarsIndex(const std::string &_fileName);
    ~CarsIndex();
    void perform(std::vector<std::string> &keys);
    std::vector<Car> getAll(std::string &key);
    std::vector<Car> getAll(std::vector<std::string> &key);
private:
    IDictionary<std::string, std::vector<int> *> *data;
    std::vector<std::string> uniqueKeys;
    std::string fileName;
    int getIdentifier(std::string key);
    long long getIdentifier(std::vector<std::string> &keys);
};

#endif
