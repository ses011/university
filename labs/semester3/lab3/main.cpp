#include <string>
#include <iostream>
#include <random>
#include <cstdlib>
#include <math.h>
#include <fstream>

#include "Index.h"
#include "Timer.h"


#define TEST_NUM 5


void executeMakeDatafile(int num) {
    std::string comand = "python3 /home/egor/CLionProjects/LAB3_SEM3/make_datafile.py " + std::to_string(num);
    std::system(comand.c_str());
}


void executeDisplay() {
    std::string comand = "python3 /home/egor/CLionProjects/LAB3_SEM3/display_tests.py";
    std::system(comand.c_str());
}


void printLine() {
    std::cout << "------------------------------------------------------------" << std::endl;
}


void description() {
    std::cout << "Please, choose option: " << std::endl;
    std::cout << "1) Generate new file with data." << std::endl;
    std::cout << "2) Manual test." << std::endl;
    std::cout << "3) Run tests." << std::endl;
    std::cout << "Type 'e' for exit." << std::endl;
    printLine();
}


void helloText() {
    std::cout << "Hi! Here you can test the task about indexing data." << std::endl;
    std::cout << "There tho types of indexing: using hash table and AVL tree." << std::endl;
    std::cout << "This classes can process only one certain class: Car" << std::endl;
    printLine();
}


void generateNewData() {
    std::cout << "Enter number of lines: ";
    int num;
    bool retry = true;
    while (retry) {
        std::cin >> num;
        if (num > 0) {
            retry = false;
        } else {
            std::cout << "Enter correct value." << std::endl;
        }
    }
    executeMakeDatafile(num);
    std::cout << "Datafile successfully updated\n";
}
void manualTest() {
    auto fileName = "/home/egor/CLionProjects/LAB3_SEM3/cars";
    Index *data;
    bool retry = true;
    char c;

    std::cout << "Choose data structure (1 - Tree, 2 - HashTable): ";
    while (retry) {
        std::cin >> c;
        if (c == '1') {
            data = new ITreeCarsIndex(fileName);
            retry = false;
        } else if (c == '2') {
            data = new IHashCarsIndex(fileName);
            retry = false;
        } else {
            std::cout << "Enter correct value.\n";
        }
    }

    retry = true;
    c = '-';
    int num;
    std::cout << "Select number of keys (from 1 to 12): ";
    while (retry) {
        std::cin >> num;
        if (num <= 0) {
            std::cout << "Enter correct value.\n";
        } else {
            retry = false;
        }
    }

    retry = true;
    c = '-';
    int tmp;
    std::vector<std::string> keys;
    std::cout << "Choose keys for indexing (" << std::to_string(num) << " pieces):\n";
    std::cout << "1 - firm, 2 - year, 3 - mileage, 4 - type, 5 - color, 6 - engine,"
                 " 7 - power, 8 - gearbox, 9 - drive, 10 - owners, 11 - price, 12 - number \n";
    std::vector<std::string> fields {"firm", "year", "mileage", "type", "color", "engine",
                                     "power", "gearbox", "drive", "owners", "price", "number"};
    for (int i = 0; i < num; i++) {
        std::cin >> tmp;
        keys.push_back(fields[tmp - 1]);
    }

    data->perform(keys);

    while (retry) {

        std::cout << "Enter certain keys in correct order(";
        for (auto x: keys) {
            std::cout << x << ", ";
        }
        std::cout << '\b' << '\b' << "): ";

        std::vector<std::string> certainVal;
        std::string tmp1;
        for (int i = 0; i < num; i++) {
            std::cin >> tmp1;
            if ((i == 0) && (tmp1 == "stop")) {
                retry = false;
                break;
            }
            certainVal.push_back(tmp1);
        }
        if (! retry) {
            break;
        }

        std::vector<Car> res = data->getAll(certainVal);
        std::cout << "Index can find values (" << res.size() << "): ";
        if (res.size() != 0) {
            for (auto x: res) {
                std::cout << x << ", ";
            }
            std::cout << '\b' << '\b' << "\n";
        } else {
            std::cout << "nothing =(\n";
        }
    }
    delete data;
};


void testCreatingTime() {
    std::vector<std::string> keys;
    std::vector<double> treeOne = {};
    std::vector<double> hashOne = {};
    std::vector<double> treeTwo = {};
    std::vector<double> hashTwo = {};
    std::vector<double> treeThree = {};
    std::vector<double> hashThree = {};
    Index *data;
    Timer timer;
    for (int i = 1; i <= TEST_NUM; i++) {
        executeMakeDatafile(pow(10, 1 + i));
        keys = {};
        keys.push_back("firm");
        data = new IHashCarsIndex("/home/egor/CLionProjects/LAB3_SEM3/cars");
        timer.reset();
        data->perform(keys);
        hashOne.push_back(timer.elapsed());
        delete data;

        //executeMakeDatafile(pow(100, i));
        keys = {};
        keys.push_back("color");
        data = new ITreeCarsIndex("/home/egor/CLionProjects/LAB3_SEM3/cars");
        timer.reset();
        data->perform(keys);
        treeOne.push_back(timer.elapsed());
        //delete data;

        //executeMakeDatafile(pow(100, i));
        keys = {};
        keys.push_back("firm");
        keys.push_back("year");
        data = new IHashCarsIndex("/home/egor/CLionProjects/LAB3_SEM3/cars");
        timer.reset();
        data->perform(keys);
        hashTwo.push_back(timer.elapsed());
        delete data;

        //executeMakeDatafile(pow(100, i));
        keys = {};
        keys.push_back("firm");
        keys.push_back("year");
        data = new ITreeCarsIndex("/home/egor/CLionProjects/LAB3_SEM3/cars");
        timer.reset();
        data->perform(keys);
        treeTwo.push_back(timer.elapsed());
        //delete data;

        keys = {};
        keys.push_back("color");
        keys.push_back("power");
        keys.push_back("drive");
        data = new IHashCarsIndex("/home/egor/CLionProjects/LAB3_SEM3/cars");
        timer.reset();
        data->perform(keys);
        hashThree.push_back(timer.elapsed());
        delete data;

        keys = {};
        keys.push_back("color");
        keys.push_back("power");
        keys.push_back("drive");
        data = new ITreeCarsIndex("/home/egor/CLionProjects/LAB3_SEM3/cars");
        timer.reset();
        data->perform(keys);
        treeThree.push_back(timer.elapsed());
    }

    std::ofstream file("/home/egor/CLionProjects/LAB3_SEM3/tests/creatingTests");
    for (int i = 1; i <= TEST_NUM; i++) {
        file << pow(10, 1 + i) << "\n";
        file << hashOne[i-1] << "\n";
        file << treeOne[i-1] << "\n";
        file << hashTwo[i-1] << "\n";
        file << treeTwo[i-1] << "\n";
        file << hashThree[i-1] << "\n";
        file << treeThree[i-1] << "\n";
    }
    file.close();
}


double findWithoutFile(Car &c) {
    Timer timer;
    std::string buffer[12];
    std::ifstream in("/home/egor/CLionProjects/LAB3_SEM3/cars");
    while (in >> buffer[0] >> buffer[1] >> buffer[2] >> buffer[3] >> buffer[4] >> buffer[5] >> buffer[6] >> buffer[7]
              >> buffer[8] >> buffer[9] >> buffer[10] >> buffer[11]) {
        Car currentCar(buffer);
        if (c == currentCar) {
            return timer.elapsed();
        }
    }
    return timer.elapsed();
}


void testFindingTime() {
    srand(time(0));
    Car currentCar;
    std::vector<Car> result;
    std::vector<std::string> keys;
    std::vector<std::string> keysToFind;
    std::vector<double> withoutIndex1 = {};
    std::vector<double> withoutIndex2 = {};
    std::vector<double> treeOne = {};;
    std::vector<double> treeTwo = {};
    Index *dataTree;
    Timer timer;
    for (int i = 1; i <= TEST_NUM; i++) {
        result = {};
        keys = {};
        keysToFind = {};

        executeMakeDatafile(pow(10, 1 + i));
        dataTree = new ITreeCarsIndex("/home/egor/CLionProjects/LAB3_SEM3/cars");
        keys.emplace_back("firm");
        dataTree->perform(keys);
        keysToFind.push_back("LADA");
        std::vector<Car> result = dataTree->getAll(keysToFind);
        Car currentCar = result[rand() % result.size()];
        withoutIndex1.push_back(findWithoutFile(currentCar));
        timer.reset();
        result = dataTree->getAll(keysToFind);
        for (auto x: result) {
            if (x == currentCar) {
                treeOne.push_back(timer.elapsed());
                break;
            }
        }
        //delete dataTree;

        result = {};
        keys = {};
        keysToFind = {};
        dataTree = new ITreeCarsIndex("/home/egor/CLionProjects/LAB3_SEM3/cars");
        keys.push_back("firm");
        keys.push_back("color");
        dataTree->perform(keys);
        keysToFind.push_back("LADA");
        keysToFind.push_back("red");
        result = dataTree->getAll(keysToFind);
        std::cout << result.size() << std::endl;
        currentCar = result[rand() % (result.size() - 1)];
        withoutIndex2.push_back(findWithoutFile(currentCar));

        timer.reset();
        result = dataTree->getAll(keysToFind);
        for (auto x: result) {
            if (x == currentCar) {
                treeTwo.push_back(timer.elapsed());
                break;
            }
        }
    }

    std::ofstream file("/home/egor/CLionProjects/LAB3_SEM3/tests/finding100Tests");
    for (int i = 1; i <= TEST_NUM; i++) {
        file << pow(10, 1 + i) << "\n";
        file << withoutIndex1[i-1] << "\n";
        file << treeOne[i-1] << "\n";
        file << withoutIndex2[i-1] << "\n";
        file << treeTwo[i-1] << "\n";
    }
    file.close();
}


void runTests() {
    testCreatingTime();
    testFindingTime();
    executeDisplay();
};


int main() {
    helloText();
    description();
    char c;
    bool run = true;
    while (run) {
        std::cout << "Please, enter a command." << std::endl;
        std::cin >> c;
        switch(c) {
            case 'e':
                run = false;
                break;
            case '1':
                generateNewData();
                break;
            case '2':
                manualTest();
                break;
            case '3':
                runTests();
                break;
            default:
                description();
                break;
        }
        printLine();
    }
}
