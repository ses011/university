#ifndef LAB3_SEM3_TREECARSINDEX_H
#define LAB3_SEM3_TREECARSINDEX_H


#include "AVLTree.h"
#include "Car.h"
#include <fstream>


class TreeCarsIndex {
public:
    TreeCarsIndex(std::string _fileName);
    ~TreeCarsIndex() = default;
    void perform(std::vector<std::string> &keys);
    std::vector<Car> getAll(std::vector<std::string> &key);
private:
    std::string fileName;
    AVLTree data;
    int getIdentifier(std::string key);
    long long getIdentifier(std::vector<std::string> &keys);
};



#endif
