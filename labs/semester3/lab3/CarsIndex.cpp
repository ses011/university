#include "CarsIndex.h"


CarsIndex::CarsIndex(const std::string &_fileName) {
    fileName = _fileName;
    std::ifstream in(fileName);
    if (in.bad()) {
        throw DescriptiveException("Wrong file name");
    }
    int numLines = 0;
    std::string unused;
    while (std::getline(in, unused))
        ++numLines;
    data = new IChainHashTable<std::string, std::vector<int> *>(numLines);
}


CarsIndex::~CarsIndex() {
    for (auto x: uniqueKeys) {
        auto tmp = data->get(x);
        delete tmp;
    }
}


long long CarsIndex::getIdentifier(std::vector<std::string> &keys) {
    long long id = 0;
    int deg = 1;
    for (int i = 0; i < keys.size(); i++) {
        id = getIdentifier(keys[i]) * deg + id;
        deg *= 10;
    }
    return id;
}


int CarsIndex::getIdentifier(std::string key) {
    int id = -1;
    std::vector<std::string> fields {"firm", "year", "mileage", "type", "color", "engine",
                                     "power", "gearbox", "drive", "owners", "price", "number"};
    for (int i = 0; i < fields.size(); i++) {
        if (key == fields[i]) {
            id = i + 1;
            break;
        }
    }
    if (id != -1) {
        return id;
    } else {
        throw DescriptiveException("Invalid key");
    }
}


//firm, year, mileage, type, color, engine, power, gearbox, drive, owners, price, number
void CarsIndex::perform(std::vector<std::string> &keys) {
    std::ifstream in(fileName);
    int id = getIdentifier(keys);
    std::string buffer[12];
    std::string key;
    int pos = 0;
    int tmp;
    while (in >> buffer[0] >> buffer[1] >> buffer[2] >> buffer[3] >> buffer[4] >> buffer[5] >> buffer[6] >> buffer[7]
              >> buffer[8] >> buffer[9] >> buffer[10] >> buffer[11]) {
        key = "";
        tmp = id;
        while (tmp > 0) {
            key += buffer[(tmp % 10) - 1];
            tmp /= 10;
        }
        try {
            std::vector<int> *tmp = data->get(key);
            tmp->push_back(pos);
        } catch(...) {
            std::vector<int> *tmp = new std::vector<int>;
            tmp->push_back(pos);
            data->add(key, tmp);
        }
        pos = in.tellg();
    }
}


std::vector<Car> CarsIndex::getAll(std::vector<std::string> &key) {
    std::string result;
    for (auto x: key) {
        result += x;
    }
    return getAll(result);
}


std::vector<Car> CarsIndex::getAll(std::string &key) {
    std::string buffer[12];
    std::vector<Car> result = {};
    std::ifstream in(fileName);
    try {
        for (auto x: (*data->get(key))) {
            in.seekg(x);
            in >> buffer[0] >> buffer[1] >> buffer[2] >> buffer[3] >> buffer[4] >> buffer[5] >> buffer[6] >> buffer[7]
               >> buffer[8] >> buffer[9] >> buffer[10] >> buffer[11];
            Car currentCar(buffer);
            result.push_back(currentCar);
        }
    } catch (...) {
        return result;
    }
    return result;
}
