#include "TreeCarsIndex.h"


TreeCarsIndex::TreeCarsIndex(std::string _fileName) {
    fileName = _fileName;
    std::ifstream in(fileName);
    if (in.bad()) {
        throw DescriptiveException("Wrong file name");
    }
}


long long TreeCarsIndex::getIdentifier(std::vector<std::string> &keys) {
    long long id = 0;
    int deg = 1;
    for (int i = 0; i < keys.size(); i++) {
        id = getIdentifier(keys[i]) * deg + id;
        deg *= 10;
    }
    return id;
}


int TreeCarsIndex::getIdentifier(std::string key) {
    int id = -1;
    std::vector<std::string> fields {"firm", "year", "mileage", "type", "color", "engine",
                                     "power", "gearbox", "drive", "owners", "price", "number"};
    for (int i = 0; i < fields.size(); i++) {
        if (key == fields[i]) {
            id = i + 1;
            break;
        }
    }
    if (id != -1) {
        return id;
    } else {
        throw DescriptiveException("Invalid key");
    }
}


//firm, year, mileage, type, color, engine, power, gearbox, drive, owners, price, number
void TreeCarsIndex::perform(std::vector<std::string> &keys) {
    std::ifstream in(fileName);
    long long id = getIdentifier(keys);
    std::string buffer[12];
    std::vector<std::string> key;
    int pos = 0;
    long long tmp = -1;
    while (in >> buffer[0] >> buffer[1] >> buffer[2] >> buffer[3] >> buffer[4] >> buffer[5] >> buffer[6] >> buffer[7]
              >> buffer[8] >> buffer[9] >> buffer[10] >> buffer[11]) {
        key = {};
        tmp = id;
        while (tmp > 0) {
            key.push_back(buffer[(tmp % 10) - 1]);
            tmp /= 10;
        }
        data.add(key, 0, pos);
        pos = in.tellg();
    }
}


std::vector<Car> TreeCarsIndex::getAll(std::vector<std::string> &key) {
    std::string buffer[12];
    std::vector<Car> result = {};
    std::ifstream in(fileName);
    try {
        for (auto x: data.search(key, 0)) {
            in.seekg(x);
            in >> buffer[0] >> buffer[1] >> buffer[2] >> buffer[3] >> buffer[4] >> buffer[5] >> buffer[6] >> buffer[7]
               >> buffer[8] >> buffer[9] >> buffer[10] >> buffer[11];
            Car currentCar(buffer);
            result.push_back(currentCar);
        }
    } catch (...) {
        return result;
    }
    return result;
}