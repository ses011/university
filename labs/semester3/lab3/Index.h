#ifndef LAB3_SEM3_INDEX_H
#define LAB3_SEM3_INDEX_H

#include "TreeCarsIndex.h"
#include "CarsIndex.h"


class Index {
public:
    virtual ~Index() = default;
    virtual void perform(std::vector<std::string> &keys) = 0;
    virtual std::vector<Car> getAll(std::vector<std::string> &keys) = 0;
};


class ITreeCarsIndex : public Index {
public:
    ITreeCarsIndex(std::string fileName) : ind(fileName) {};
    ~ITreeCarsIndex() = default;
    void perform(std::vector<std::string> &keys) override {ind.perform(keys);};
    std::vector<Car> getAll(std::vector<std::string> &keys) override {return ind.getAll(keys);}
private:
    TreeCarsIndex ind;
};


class IHashCarsIndex : public Index {
public:
    IHashCarsIndex(std::string fileName) : ind(fileName) {};
    ~IHashCarsIndex() = default;
    void perform(std::vector<std::string> &keys) override {ind.perform(keys);};
    std::vector<Car> getAll(std::vector<std::string> &keys) override {return ind.getAll(keys);}
private:
    CarsIndex ind;
};

#endif
