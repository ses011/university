import sys
import matplotlib.pyplot as plt


def display1():
    xs = [2, 3, 4, 5, 6]
    ys = [[], [], [], [], [], []]
    i = -1
    with open("/home/egor/CLionProjects/LAB3_SEM3/tests/creatingTests", "r") as file:
        for line in file:
            i += 1
            if i % 7 == 0:
                continue
            ys[(i % 7) - 1].append(float(line[:len(line)-2]))

    tmp = 1
    for i in range(0, 5, 2):
        plt.plot(xs, ys[i], 'o-r', color='r', label=f'hash {tmp} key')
        plt.plot(xs, ys[i+1], 'o-r', color='b', label=f'tree {tmp} key')
        plt.legend()
        plt.axis([2, 6, 0, 1.5])
        plt.xlabel('Num of elems.')
        plt.ylabel('Time in sec.')
    #plt.show()
        plt.savefig(f'/home/egor/CLionProjects/LAB3_SEM3/tests/indexingTime{i}.png')
        plt.close()
        tmp += 1

    plt.plot(xs, ys[0], 'o-r', color='r', label=f'hash 1 key')
    plt.plot(xs, ys[2], 'o-r', color='b', label=f'hash 2 key')
    plt.plot(xs, ys[4], 'o-r', color='g', label=f'hash 3 key')
    plt.legend()
    plt.axis([2, 6, 0, 1.5])
    plt.xlabel('Num of elems.')
    plt.ylabel('Time in sec.')
    #plt.show()
    plt.savefig(f'/home/egor/CLionProjects/LAB3_SEM3/tests/indexingTimeHash.png')
    plt.close()

    plt.plot(xs, ys[1], 'o-r', color='r', label=f'tree 1 key')
    plt.plot(xs, ys[3], 'o-r', color='b', label=f'tree 2 key')
    plt.plot(xs, ys[5], 'o-r', color='g', label=f'tree 3 key')
    plt.legend()
    plt.axis([2, 6, 0, 1.5])
    plt.xlabel('Num of elems.')
    plt.ylabel('Time in sec.')
    #plt.show()
    plt.savefig(f'/home/egor/CLionProjects/LAB3_SEM3/tests/indexingTimeTree.png')
    plt.close()


def display2():
    xs = [2, 3, 4, 5, 6]
    ys = [[], [], [], []]
    i = -1
    with open("/home/egor/CLionProjects/LAB3_SEM3/tests/finding100Tests", "r") as file:
        for line in file:
            i += 1
            if i % 5 == 0:
                continue
            if 'e-05' in line:
                ys[(i % 5) - 1].append(float(line[:3]) / 10)
            else:
                ys[(i % 5) - 1].append(float(line[:len(line)-2]) * 1000)
    for x in ys:
        print(x)
    plt.plot(xs, ys[0], 'o-r', color='r', label=f'file')
    plt.plot(xs, ys[1], 'o-r', color='b', label=f'Index 2 key')
    plt.plot(xs, ys[3], 'o-r', color='g', label=f'Index 3 key')
    plt.legend()
    plt.axis([2, 6, 0, 700])
    plt.xlabel('Num of elems.')
    plt.ylabel('Time in millisec.')
    #plt.show()
    plt.savefig(f'/home/egor/CLionProjects/LAB3_SEM3/tests/findingTime.png')
    plt.close()

def main():
    #display1()
    display2()


main()