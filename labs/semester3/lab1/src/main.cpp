#include <iostream>
#include "Sorters.h"
#include <ctime>
#include </home/egor/CLionProjects/LAB2_MAIN/src/ArraySequence.h>
#include </home/egor/CLionProjects/LAB2_MAIN/src/ListSequence.h>


bool compareInt(int a, int b) {
    return (a < b);
}


void printBlanketLine() {
    std::cout << "------------------------------------------------------" << std::endl;
}


void printTryAgain() {
    std::cout << "> Please enter the right length" << std::endl;
}


void printInfo() {
    printBlanketLine();
    std::cout << "This program implements the following sorting methods:" << std::endl;
    std::cout << "1) Bubble sort" << std::endl;
    std::cout << "2) Insertion sort with binary search" << std::endl;
    std::cout << "3) Shell sort with constant offset" << std::endl;
    std::cout << "4) Quick sort" << std::endl;
}


void printCommands() {
    std::cout << "Choose command:" << std::endl;
    std::cout << "1) Manual verification" << std::endl;
    std::cout << "2) Automatic verification" << std::endl;
    std::cout << "e) Exit" << std::endl;
}


int inputLen() {
    std::cout << "> Enter length of your sequence" << std::endl;
    bool f = true;
    int len = 0;
    while (f) {
        std::cin >> len;
        if (len <= 0) {
            printTryAgain();
        } else {
            f = false;
        }
    }
    return len;
}


ISorter<int> * inputSorter() {
    std::cout << "> Choose type of sort" << std::endl;
    bool f = true;
    char c;
    while (f) {
        std::cout << "  1 - Bubble sort\n  2 - Insertion sort\n  3 - Shell sort\n  4 - Quick sort" << std::endl;
        std::cin >> c;
        switch (c) {
            case '1':
                return new BubbleSorter<int>;
            case '2':
                return new InsertionSorter<int>;
            case '3':
                return new ShellSorter<int>;
            case '4':
                return new QuickSorter<int>;
            default:
                printTryAgain();
        }
    }
}


void printTimingsMan(ISorter<int> *sorter, Sequence<int> *mainSeq, Sequence<int> *secondSeq = nullptr) {
    Sequence<int> *res1, *res2;
    std::cout << "> The sorting process is underway..." << std::endl;
    double start = clock();
    if (secondSeq == nullptr) {
        res1 = sorter->sort(mainSeq, &compareInt);
        double duration = clock() - start;
        std::cout << "> For sequence with length=" << mainSeq->getLength() << " ";
        std::cout << "the sorting lasted " << duration / CLOCKS_PER_SEC << " sec" << std::endl;
        std::cout << "> Sorted sequence:" << std::endl;
        res1->print();
    } else {
        res1 = sorter->sort(mainSeq, &compareInt);
        double end1 = clock();
        std::cout << "> For array sequence with length=" << mainSeq->getLength() << " ";
        std::cout << "the sorting lasted " << (end1 - start) / CLOCKS_PER_SEC << " sec" << std::endl;
        std::cout << "> Sorted sequence:" << std::endl;
        res1->print();
        res2 = sorter->sort(secondSeq, &compareInt);
        std::cout << "> For list sequence with length=" << secondSeq->getLength() << " ";
        std::cout << "the sorting lasted " << (clock() - end1) / CLOCKS_PER_SEC << " sec" << std::endl;
        std::cout << "> Sorted sequence:" << std::endl;
        res2->print();
    }
}


void fillSequenceRandom(int len, Sequence<int> *seq1, Sequence<int> *seq2) {
    srand(time(NULL));
    int tmp;
    for (int i = 0; i < len; i++){
        tmp = rand() % 200;
        seq1->append(tmp);
        if (seq2 != nullptr) {
            seq2->append(tmp);
        }
    }
}


void fillSequence(int len, Sequence<int> *seq1, Sequence<int> *seq2 = nullptr) {
    std::cout << "Do you want to fill sequence with random numbers? (y/n) ";
    bool f = true;
    char c;
    int tmp;
    while (f) {
        std::cin >> c;
        if ((c == 'y') || (c == 'n')) {
            break;
        } else {
            printTryAgain();
        }
    }
    if (c == 'n') {
        std::cout << "> Enter elements of your sequence:" << std::endl;
        for (int i = 0; i < len; i++) {
            std::cin >> tmp;
            seq1->append(tmp);
            if (seq2 != nullptr) {
                seq2->append(tmp);
            }
        }
    } else {
        fillSequenceRandom(len, seq1, seq2);
    }
}


void manualVerification() {
    char s = ']';
    int len;
    Sequence<int> *sequence1, *sequence2, *res1 = nullptr, *res2 = nullptr;
    ISorter<int> *sorter;
    bool f;

    len = inputLen();

    std::cout << "> Create sequence based on list or array?" << std::endl;
    f = true;
    while (f) {
        std::cout << "  l - list\n  a - array\n  b - both" << std::endl;
        std::cin >> s;
        switch (s) {
            case 'a':
                sequence1 = new ArraySequence<int>;
                f = false;
                break;
            case 'l':
                sequence1 = new ListSequence<int>;
                f = false;
                break;
            case 'b':
                sequence1 = new ArraySequence<int>;
                sequence2 = new ListSequence<int>;
                f = false;
                break;
            default:
                printTryAgain();
        }
    }

    if (s == 'b') {
        fillSequence(len, sequence1, sequence2);
    } else {
        fillSequence(len, sequence1);
    }

    sorter = inputSorter();

    std::cout << "> Your sequence is:" << std::endl;
    sequence1->print();
    if (s == 'b')
        printTimingsMan(sorter, sequence1, sequence2);
    else
        printTimingsMan(sorter, sequence1);
}


void printTimingsAuto(ISorter<int> *sorter, Sequence<int> *seq1, Sequence<int> *seq2) {
    Sequence<int> *res1, *res2;
    std::cout << "> The sorting process is underway..." << std::endl;
    double start = clock();
    bool f = false;
    if (seq2 == nullptr) {
        res1 = sorter->sort(seq1, &compareInt);
        double duration = clock() - start;
        std::cout << "> For sequence with length=" << seq1->getLength() << " ";
        std::cout << "the sorting lasted " << duration / CLOCKS_PER_SEC << " sec" << std::endl;
        std::cout << "> Sorted sequence:" << std::endl;
        for (int i = 1; i < res1->getLength(); i++) {
            if ((*res1)[i-1] > (*res1)[i]){
                f = true;
            }
        }
        if (f) {
            std::cout << "This sort method doesn`t work for array" << std::endl;
        } else {
            std::cout << "This sort method works fine for array!" << std::endl;
        }

    } else {
        res1 = sorter->sort(seq1, &compareInt);
        double end1 = clock();
        std::cout << "> For array sequence with length=" << seq1->getLength() << " ";
        std::cout << "the sorting lasted " << (end1 - start) / CLOCKS_PER_SEC << " sec" << std::endl;
        for (int i = 1; i < res1->getLength(); i++) {
            if ((*res1)[i-1] > (*res1)[i]){
                f = true;
            }
        }
        if (f) {
            std::cout << "This sort method doesn`t work for array" << std::endl;
        } else {
            std::cout << "This sort method works fine for array!" << std::endl;
        }
        end1 = clock();
        res2 = sorter->sort(seq2, &compareInt);
        std::cout << "> For list sequence with length=" << seq2->getLength() << " ";
        std::cout << "the sorting lasted " << (clock() - end1) / CLOCKS_PER_SEC << " sec" << std::endl;
        for (int i = 1; i < res1->getLength(); i++) {
            if ((*res2)[i-1] > (*res2)[i]){
                f = true;
            }
        }
        if (f) {
            std::cout << "This sort method doesn`t work for list" << std::endl;
        } else {
            std::cout << "This sort method works fine for list!" << std::endl;
        }
    }
}


void automaticVerification() {
    int len;
    len = inputLen();
    Sequence<int> *sequence1, *sequence2;
    ISorter<int> *sorter;
    sequence1 = new ArraySequence<int>;
    sequence2 = new ListSequence<int>;
    fillSequenceRandom(len, sequence1, sequence2);
    sorter = inputSorter();
    //make many sorters

    printTimingsAuto(sorter, sequence1, sequence2);
}


int main() {
    char c = 'r';
    printInfo();
    while (c != 'e') {
        printBlanketLine();
        printCommands();
        printBlanketLine();
        std::cin >> c;
        switch (c) {
            case '1':
                manualVerification();
                break;
            case '2':
                automaticVerification();
                break;
            case 'e':
                break;
            default:
                std::cout << "Please choose command from list" << std::endl;
        }
    }
}

