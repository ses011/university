#include "ISorter.h"


template<typename T>
class ShellSorter : public ISorter<T> {
public:
    Sequence<T> *sort(Sequence<T> *seq, bool override;
private:
    Sequence<T> *data;
};


template<typename T>
Sequence<T> * ShellSorter<T>::sort(Sequence<T> *seq, bool (*cmp)(T, T)) {
    data = seq->clone();
    int len = data->getLength();
    int step, i, j;
    for (step = len/2; step > 0; step /= 2) {
        for (i = step; i < len; i++) {
            for (j = i - step; j >= 0 && cmp((*data)[j + step], (*data)[j]); j -= step) {
                data->swap(j, j+step);
            }
        }
    }
    return data;
}
