#include <gtest/gtest.h>
#include "ComapreInt.h"
#include </home/egor/CLionProjects/LAB3_MAIN/src/QuickSorter.h>
#include </home/egor/CLionProjects/LAB2_MAIN/src/ListSequence.h>
#include </home/egor/CLionProjects/LAB2_MAIN/src/ArraySequence.h>


TEST(QuickSorter, ArraySequenceLen0) {
    //given
    Sequence<int> *seq = new ArraySequence<int>;
    ISorter<int> *sorter = new QuickSorter<int>;
    //when
    Sequence<int> *res = sorter->sort(seq, &compareInt);
    //then
    ASSERT_EQ(0, res->getLength());
}


TEST(QuickSorter, ArraySequenceLen6) {
    //given
    int a[6] = {6, 2, 1, 4, 5, 0};
    Sequence<int> *seq = new ArraySequence<int>(a, 6);
    ISorter<int> *sorter = new QuickSorter<int>;
    //when
    Sequence<int> *res = sorter->sort(seq, &compareInt);
    //then
    ASSERT_EQ(6, res->getLength());
    for (int i = 1; i < 6; i++) {
        EXPECT_TRUE((*res)[i-1] < (*res)[i]);
    }
}


TEST(QuickSorter, ArraySequenceImmutableSource) {
    //given
    int a[6] = {6, 2, 1, 4, 5, 0};
    Sequence<int> *seq = new ArraySequence<int>(a, 6);
    ISorter<int> *sorter = new QuickSorter<int>;
    //when
    Sequence<int> *res = sorter->sort(seq, &compareInt);
    //then
    ASSERT_EQ(6, seq->getLength());
    for (int i = 0; i < 6; i++) {
        EXPECT_TRUE((*seq)[i] == a[i]);
    }
}


TEST(QuickSorter, ArraySequenceNegative) {
    //given
    int a[6] = {-6, 2, -1, 4, 5, 0};
    Sequence<int> *seq = new ArraySequence<int>(a, 6);
    ISorter<int> *sorter = new QuickSorter<int>;
    //when
    Sequence<int> *res = sorter->sort(seq, &compareInt);
    //then
    ASSERT_EQ(6, res->getLength());
    for (int i = 1; i < 6; i++) {
        EXPECT_TRUE((*res)[i-1] < (*res)[i]);
    }
}


TEST(QuickSorter, ArraySequenceEqualElems) {
    //given
    int a[4] = {2, 1, 3, 1};
    Sequence<int> *seq = new ArraySequence<int>(a, 4);
    ISorter<int> *sorter = new QuickSorter<int>;
    //when
    Sequence<int> *res = sorter->sort(seq, &compareInt);
    //then
    ASSERT_EQ(4, res->getLength());
    EXPECT_EQ(1, (*res)[0]);
    EXPECT_EQ(1, (*res)[1]);
    EXPECT_EQ(2, (*res)[2]);
    EXPECT_EQ(3, (*res)[3]);
}


TEST(QuickSorter, ArraySequenceLargeLen) {
    //given
    int a[5] = {4, 1, 6, 9, 2};
    Sequence<int> *seq = new ArraySequence<int>(a, 5);
    for (int i = 0; i < 10000; i++) {
        seq->append(a[4 - (i % 5)]);
    }
    ISorter<int> *sorter = new QuickSorter<int>;
    //when
    Sequence<int> *res = sorter->sort(seq, &compareInt);
    ASSERT_EQ(10005, res->getLength());
    for (int i = 1; i < 10005; i++) {
        EXPECT_TRUE((*res)[i-1] <= (*res)[i]);
    }
}


TEST(QuickSorter, ListSequenceLen0) {
    //given
    Sequence<int> *seq = new ListSequence<int>;
    ISorter<int> *sorter = new QuickSorter<int>;
    //when
    Sequence<int> *res = sorter->sort(seq, &compareInt);
    //then
    ASSERT_EQ(0, res->getLength());
}


TEST(QuickSorter, ListSequenceLen6) {
    //given
    int a[6] = {6, 2, 1, 4, 5, 0};
    Sequence<int> *seq = new ListSequence<int>(a, 6);
    ISorter<int> *sorter = new QuickSorter<int>;
    //when
    Sequence<int> *res = sorter->sort(seq, &compareInt);
    //then
    ASSERT_EQ(6, res->getLength());
    for (int i = 1; i < 6; i++) {
        EXPECT_TRUE((*res)[i-1] < (*res)[i]);
    }
}


TEST(QuickSorter, ListSequenceImmutableSource) {
    //given
    int a[6] = {6, 2, 1, 4, 5, 0};
    Sequence<int> *seq = new ListSequence<int>(a, 6);
    ISorter<int> *sorter = new QuickSorter<int>;
    //when
    Sequence<int> *res = sorter->sort(seq, &compareInt);
    //then
    ASSERT_EQ(6, seq->getLength());
    for (int i = 0; i < 6; i++) {
        EXPECT_TRUE((*seq)[i] == a[i]);
    }
}


TEST(QuickSorter, ListSequenceNegative) {
    //given
    int a[6] = {-6, 2, -1, 4, 5, 0};
    Sequence<int> *seq = new ListSequence<int>(a, 6);
    ISorter<int> *sorter = new QuickSorter<int>;
    //when
    Sequence<int> *res = sorter->sort(seq, &compareInt);
    //then
    ASSERT_EQ(6, res->getLength());
    for (int i = 1; i < 6; i++) {
        EXPECT_TRUE((*res)[i-1] < (*res)[i]);
    }
}


TEST(QuickSorter, ListSequenceEqualElems) {
    //given
    int a[4] = {2, 1, 3, 1};
    Sequence<int> *seq = new ListSequence<int>(a, 4);
    ISorter<int> *sorter = new QuickSorter<int>;
    //when
    Sequence<int> *res = sorter->sort(seq, &compareInt);
    //then
    ASSERT_EQ(4, res->getLength());
    EXPECT_EQ(1, (*res)[0]);
    EXPECT_EQ(1, (*res)[1]);
    EXPECT_EQ(2, (*res)[2]);
    EXPECT_EQ(3, (*res)[3]);
}


TEST(QuickSorter, ListSequenceLargeLen) {
    //given
    int a[5] = {4, 1, 6, 9, 2};
    Sequence<int> *seq = new ListSequence<int>(a, 5);
    for (int i = 0; i < 10000; i++) {
        seq->append(a[4 - (i % 5)]);
    }
    ISorter<int> *sorter = new QuickSorter<int>;
    //when
    Sequence<int> *res = sorter->sort(seq, &compareInt);
    ASSERT_EQ(10005, res->getLength());
    for (int i = 1; i < 10005; i++) {
        EXPECT_TRUE((*res)[i-1] <= (*res)[i]);
    }
}

