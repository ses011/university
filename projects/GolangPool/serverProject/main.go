package main

import (
	"awesomeProject/server"
	"fmt"
	"os"
	"strconv"
)

func main() {
	host := os.Getenv("HOST")
	port := os.Getenv("PORT")
	maxWorkers, err := strconv.Atoi(os.Getenv("MAXWORKERS"))

	if err != nil {
		fmt.Println("Cant load env vars, shutting down")
		return
	}

	s := server.InitServer(host, port, maxWorkers)

	s.Start()
}
