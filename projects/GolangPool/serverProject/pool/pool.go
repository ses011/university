package pool

import (
	"awesomeProject/task"
	"fmt"
	"github.com/google/uuid"
	"time"
)

type Pool struct {
	IsActive     bool
	inChan       chan task.Task
	outChan      chan task.Task
	results      map[string]chan int
	workersChan  []chan bool
	receiverChan chan bool
}

func (p *Pool) Start(workerNum int) {
	if p.IsActive {
		p.Stop()
	}

	p.IsActive = true
	p.inChan = make(chan task.Task, MAXTASKS)
	p.outChan = make(chan task.Task, MAXTASKS)

	p.results = make(map[string]chan int)

	p.initReceiver()

	p.workersChan = []chan bool{}
	for i := 0; i < workerNum; i++ {
		ch := make(chan bool)
		p.workersChan = append(p.workersChan, ch)
		go worker(i+1, p.inChan, p.outChan, ch)
	}

	fmt.Printf("Pool with %d workers initialized\n", workerNum)
}

func (p *Pool) initReceiver() {
	recChan := make(chan bool)

	p.receiverChan = recChan

	go receiver(p.results, p.outChan, p.receiverChan)
}

func (p *Pool) Stop() {
	p.IsActive = false
	p.results = make(map[string]chan int)

	for _, ch := range p.workersChan {
		ch <- true
	}

	p.receiverChan <- true

	fmt.Printf("Pool closed\n")
}

func (p *Pool) Execute(t task.Task) <-chan int {
	resChan := make(chan int, 2)

	t.Id = uuid.NewString()
	p.results[t.Id] = resChan
	p.inChan <- t

	return resChan
}

func receiver(results map[string]chan int, resChan <-chan task.Task, quit <-chan bool) {
	for {
		select {

		case t, ok := <-resChan:

			if !ok {
				fmt.Printf("??? out channel is closed\n")
				return
			}

			results[t.Id] <- t.Res
			close(results[t.Id])
			delete(results, t.Id)

		case <-quit:
			return
		}
	}
}

func handleTask(t task.Task) int {
	var res int
	switch t.Func {
	case "*":
		res = t.X * t.Y
	case "-":
		res = t.X - t.Y
	case "+":
		res = t.X + t.Y
	case "/":
		res = t.X / t.Y
	case "%":
		res = t.X % t.Y
	}

	time.Sleep(time.Duration(t.Delay) * time.Millisecond)

	return res
}

func worker(workerId int, inChan <-chan task.Task, outChan chan<- task.Task, quit <-chan bool) {
	for {
		select {

		case t, ok := <-inChan:
			if !ok {
				fmt.Printf("inChan for worker %d is closed\n", workerId)
				return
			}

			res := handleTask(t)
			t.Res = res
			outChan <- t

		case <-quit:
			fmt.Printf("Worker %d finished his work\n", workerId)
			return
		}
	}
}
