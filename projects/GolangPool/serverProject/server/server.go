package server

import (
	"awesomeProject/pool"
	"fmt"
	"net"
	"sync"
	"time"
)

type Server struct {
	host string
	port string
	pool pool.Pool
}

func InitServer(host, port string, maxWorkers int) Server {
	var s Server
	s.host = host
	s.port = port
	s.pool = pool.Pool{}
	s.pool.Start(maxWorkers)

	return s
}

func (s *Server) Start() {
	mx := sync.Mutex{}
	listener, _ := net.Listen("tcp", fmt.Sprintf("%s:%s", s.host, s.port))

	fmt.Println("Server is ready for accept connections")

	for {

		conn, err := listener.Accept()

		if err != nil {
			fmt.Println(err)
			continue
		}

		fmt.Println("Handling new client...")

		go func(conn net.Conn) {
			defer conn.Close()

			start := time.Now()

			buf := make([]byte, 50)
			readLen, err := conn.Read(buf)

			if err != nil {
				fmt.Println(err)
				resp := FormResponse("cant_read_buffer", 0, time.Since(start))
				conn.Write(resp)
				return
			}

			var res <-chan int

			if task, err := ParseRequest(string(buf[:readLen])); err == nil {
				mx.Lock()
				res = s.pool.Execute(task)
				mx.Unlock()
			} else {
				resp := FormResponse(err.Error(), 0, time.Since(start))
				conn.Write(resp)
				return
			}

			resVal := <-res

			resp := FormResponse("OK", resVal, time.Since(start))

			conn.Write(resp)
		}(conn)
	}
}
