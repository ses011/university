package server

import (
	"awesomeProject/task"
	"errors"
	"fmt"
	"strconv"
	"strings"
	"time"
)

func ParseRequest(req string) (task.Task, error) {
	vals := strings.Split(req, " ")

	x, err := strconv.Atoi(vals[0])
	if err != nil {
		return task.Task{}, errors.New(statuses[1])
	}

	op := vals[1]
	if !validOp(op) {
		return task.Task{}, errors.New(statuses[1])
	}

	y, err := strconv.Atoi(vals[2])
	if err != nil {
		return task.Task{}, errors.New(statuses[1])
	}

	delay, err := strconv.Atoi(vals[3])
	if err != nil {
		return task.Task{}, errors.New(statuses[1])
	}

	if y == 0 && op == "/" {
		return task.Task{}, errors.New(statuses[2])
	}

	if delay < 0 {
		return task.Task{}, errors.New(statuses[3])
	}

	return task.Task{X: x, Y: y, Func: op, Delay: delay}, nil
}

func validOp(op string) bool {
	switch op {
	case "+", "-", "*", "/", "%":
		return true
	default:
		return false
	}
}

func FormResponse(status string, res int, elapsed time.Duration) []byte {
	stringResp := fmt.Sprintf("%s %d %s\n", status, res, elapsed)
	return []byte(stringResp)
}
