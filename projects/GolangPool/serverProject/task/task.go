package task

type Task struct {
	Id    string
	Delay int
	X     int
	Y     int
	Func  string
	Res   int
}
