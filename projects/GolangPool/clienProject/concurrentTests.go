package main

import (
	"bufio"
	"fmt"
	"math/rand"
	"net"
	"strconv"
	"strings"
	"sync"
)

func runConcTests(host, port string, reader *bufio.Reader) {
	fmt.Print("Enter an amount of concurrent clients: ")
	testCases, _ := reader.ReadString('\n')
	testCases = strings.TrimSuffix(testCases, "\n")
	tests, err := strconv.Atoi(testCases)

	if err != nil || tests < 1 {
		fmt.Println("Amount of concurrent clients should be positive int")
		return
	}

	wg := sync.WaitGroup{}

	for i := 0; i < tests; i++ {
		wg.Add(1)
		go testServer(&wg, i, fmt.Sprintf("%s:%s", host, port))
	}

	wg.Wait()
}

func testServer(wg *sync.WaitGroup, ind int, server string) {
	defer wg.Done()

	conn, err := net.Dial("tcp", server)

	if err != nil {
		fmt.Println("Server is unavailable")
		return
	}

	ops := []string{"/", "*", "+", "-"}
	x := rand.Intn(1000)
	y := rand.Intn(1000)
	delay := rand.Intn(3000)
	op := ops[rand.Int()%len(ops)]

	ff := fmt.Sprintf("%d %s %d %d", x, op, y, delay)

	fmt.Fprintf(conn, ff)
	status, _ := bufio.NewReader(conn).ReadString('\n')
	fmt.Printf("Client%d: for %s; receive %s", ind, ff, status)
}
