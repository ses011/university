package main

import (
	"bufio"
	"fmt"
	"net"
	"os"
	"strings"
)

func main() {
	reader := bufio.NewReader(os.Stdin)
	fmt.Print("Enter the host: ")
	host, _ := reader.ReadString('\n')
	host = strings.TrimSuffix(host, "\n")
	fmt.Print("Enter the port: ")
	port, _ := reader.ReadString('\n')
	port = strings.TrimSuffix(port, "\n")

	text := "s"

	for text != "q" {
		fmt.Print("Enter new command (\"h\" for help): ")
		text, _ = reader.ReadString('\n')

		switch text[:1] {
		case "q":
			fmt.Println("Shutting down")
			return
		case "h":
			fmt.Println("Press \"q\" for quit")
			fmt.Println("Press \"h\" for help")
			fmt.Println("Press \"t\" for execute concurrent tests. There will be a lot of text :)")
			fmt.Println("Press \"r\" for make request")
			fmt.Println("Request format: \"x op y delay\", where x, y, delay (in milliseconds) is int, op is one from \"*\", \"/\", \"+\", \"-\", \"%\"")
			fmt.Println("Response format: \"status result elapsed_time\"")
		case "r":
			executeRequest(host, port, reader)
		case "t":
			runConcTests(host, port, reader)
		default:
			fmt.Println("Unknown command, enter \"h\" for help")
		}

	}
}

func executeRequest(host, port string, reader *bufio.Reader) {
	fmt.Print("Enter request (format: \"x op y delay\"): ")
	req, _ := reader.ReadString('\n')
	req = strings.TrimSuffix(req, "\n")

	fmt.Println(req)

	conn, err := net.Dial("tcp", fmt.Sprintf("%s:%s", host, port))

	if err != nil {
		fmt.Println("Server is unavailable")
		return
	}

	fmt.Fprintf(conn, req)
	status, _ := bufio.NewReader(conn).ReadString('\n')
	fmt.Printf("Response: %s", status)
}
