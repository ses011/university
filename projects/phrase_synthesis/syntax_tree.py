import json
import pymorphy2
import timeit


class TreeNode:
    def __init__(self, word, conn_parent, grams):
        self.word = word                        # word in initial form
        self.conn_type_to_parent = conn_parent  # type of connection to parent
        self.parent_index = -1                  # index of parent node in adj list
        self.index = -1                         # index of current word in adj list
        self.grams = grams                      # grammemes of word

    def __str__(self):
        return f'TreeNode({self.word}, {self.parent_index})'

    def set_conn_type(self, _type):
        self.conn_type_to_parent = _type


class SyntaxTree:

    #@timeit.timeit
    def __init__(self, filename):
        self.phrase = SyntaxTree.parse_json(filename)  # all info from json file
        self.nodes = {}  # dict of TreeNodes
        self.adj_list = []  # adj list to store tree
        self.root = -1  # index of root node
        self.result = ''  # sting result phrase
        self.make_tree()  # fill nodes and adj list

    @staticmethod
    def parse_json(filename):
        with open(filename, 'r', encoding='utf-8') as file:
            json_data = json.loads(file.read())
        return json_data

    def make_adjacency_list(self):
        n = len(self.phrase['tokens'])
        res = [[] for _ in range(n)]
        used = [0 for _ in range(n)]
        uniq_conns = dict()

        for s in self.phrase['synts']:
            if (s[0] not in uniq_conns.keys()) or (s[0] in uniq_conns.keys() and uniq_conns[s[0]] != s[1]):
                uniq_conns[s[0]] = s[1]
                res[s[0]].append(s[1])
                self.nodes[s[1]].parent_index = s[0]
                self.nodes[s[1]].index = s[1]
                self.nodes[s[1]].conn_type_to_parent = s[2]
                used[s[1]] = 1

        morph = pymorphy2.MorphAnalyzer()
        root = -1
        for i in range(n):
            if used[i] == 0:
                p = morph.parse(self.phrase['tokens'][i])[0]
                if ('PNCT' not in p.tag) and ('UNKN' not in p.tag):
                    if root != -1:
                        # print(p)
                        continue
                    root = i

        self.root = root
        self.adj_list = res

    @staticmethod
    def extract_grams(tags):
        # from OpencorporaTag(args) to args
        frst = tags.index('\'')
        tags = tags[frst + 1:]
        scnd = tags.index('\'')
        tags = tags[0: scnd]
        return tags

    def make_tree(self):
        # 0 - master index
        # 1 - slave index
        # 2 - type name
        for i, word in enumerate(self.phrase['morphs']):
            if i not in self.nodes and len(word) > 0:
                word1 = word[0]['lexem']
                grams1 = SyntaxTree.extract_grams(word[0]['tags'])
                self.nodes.update([(i, TreeNode(word1, '', grams1))])
        self.make_adjacency_list()

    #@timeit.timeit
    def dfs_main(self):
        synts = SyntaxTree.parse_json('all_synts_new.json')

        def dfs(v):
            if len(self.adj_list[v]) == 0:  # if node has no child return vertex
                return [v]

            if len(self.adj_list[v]) == 1:  # node has only one child
                # self.nodes[self.adj_list[v][0]] index of word
                # self.nodes[...].conn_to_parent
                # ind = cur.index(self.adj_list[v][0])
                cur = dfs(self.adj_list[v][0])
                conn_to_parent = self.nodes[self.adj_list[v][0]].conn_type_to_parent
                if conn_to_parent in synts['атрибутивные']:
                    t = 'атрибутивные'
                elif conn_to_parent in synts['актатные']:
                    t = 'актатные'
                elif conn_to_parent in synts['сочинительные']:
                    t = 'сочинительные'
                elif conn_to_parent in synts['служебные']:
                    t = 'служебные'

                if synts[t][conn_to_parent] == -1:
                    # cur.insert(ind, v)
                    cur.append(v)
                else:
                    # cur.insert(ind+1, v)
                    cur.insert(0, v)
                return cur

            # should divide phrases on head/tail and 4 types
            dep_phrases_act = {'head': [], 'tail': []}
            dep_phrases_atr = {'head': [], 'tail': []}
            dep_phrases_soc = {'head': [], 'tail': []}
            dep_phrases_slu = {'head': [], 'tail': []}

            for j, v1 in enumerate(self.adj_list[v]):
                conn_to_parent = self.nodes[self.adj_list[v][j]].conn_type_to_parent
                if conn_to_parent in synts['актатные']:
                    if synts['актатные'][conn_to_parent] == 1:
                        dep_phrases_act['tail'].append(dfs(v1))
                    else:
                        dep_phrases_act['head'].append(dfs(v1))
                if conn_to_parent in synts['атрибутивные']:
                    if synts['атрибутивные'][conn_to_parent] == 1:
                        dep_phrases_atr['tail'].append(dfs(v1))
                    else:
                        dep_phrases_atr['head'].append(dfs(v1))
                if conn_to_parent in synts['сочинительные']:
                    if synts['сочинительные'][conn_to_parent] == 1:
                        dep_phrases_soc['tail'].append(dfs(v1))
                    else:
                        dep_phrases_soc['head'].append(dfs(v1))
                if conn_to_parent in synts['служебные']:
                    # some rules for 'не' 'только' 'и'
                    if conn_to_parent == 'огранич':
                        word = self.nodes[v1].word
                        if word.casefold() == 'не' or word.casefold() == 'только' or word.casefold() == 'и':
                            dep_phrases_slu['head'].append(dfs(v1))
                    else:
                        if synts['служебные'][conn_to_parent] == 1:
                            dep_phrases_slu['tail'].append(dfs(v1))
                        else:
                            dep_phrases_slu['head'].append(dfs(v1))

            dep_phrases_act['head'] = sorted(dep_phrases_act['head'], key=lambda x: len(x))
            dep_phrases_act['tail'] = sorted(dep_phrases_act['tail'], key=lambda x: len(x))

            dep_phrases_atr['head'] = sorted(dep_phrases_atr['head'], key=lambda x: len(x))
            dep_phrases_atr['tail'] = sorted(dep_phrases_atr['tail'], key=lambda x: len(x))

            dep_phrases_soc['head'] = sorted(dep_phrases_soc['head'], key=lambda x: len(x))
            dep_phrases_soc['head'] = sorted(dep_phrases_soc['head'], key=lambda x: len(x))

            dep_phrases_slu['head'] = sorted(dep_phrases_slu['head'], key=lambda x: len(x))
            dep_phrases_slu['tail'] = sorted(dep_phrases_slu['tail'], key=lambda x: len(x))

            r = []

            if 'NOUN' not in self.nodes[v].grams or 'ADJF' in self.nodes[v].grams or 'ADJS' in self.nodes[v].grams:
                for j in dep_phrases_atr['head']:
                    r += j
                for j in dep_phrases_act['head']:
                    r += j
            else:
                for j in dep_phrases_act['head']:
                    r += j
                for j in dep_phrases_atr['head']:
                    r += j
            for j in dep_phrases_soc['head']:
                r += j
            for j in dep_phrases_slu['head']:
                r += j

            r += [v]

            for j in dep_phrases_slu['tail']:
                r += j
            if 'NOUN' in self.nodes[v].grams or 'ADJF' in self.nodes[v].grams or 'ADJS' in self.nodes[v].grams:
                for j in dep_phrases_act['tail']:
                    r += j
                for j in dep_phrases_atr['tail']:
                    r += j
            else:
                for j in dep_phrases_atr['tail']:
                    r += j
                for j in dep_phrases_act['tail']:
                    r += j
            for j in dep_phrases_soc['tail']:
                r += j

            return r

        res = dfs(self.root)
        for i in res:
            self.result += self.phrase['tokens'][i] + ' '
        return self.result

    def __str__(self):
        res = []
        for k in self.nodes.keys():
            res.append(str(self.nodes[k]))

        return str(res)
