import syntax_tree


def main():
    for i in range(1, 21):
        tree = syntax_tree.SyntaxTree(f"examples/in{i}.json")
        print(i, end=" ")
        print(tree.dfs_main())
        print("---------------------------------------------------------------")


main()
