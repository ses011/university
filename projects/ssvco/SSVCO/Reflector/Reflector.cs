using System;
using Microsoft.Extensions.Configuration;
using SSVCO.Domain;
using Object = SSVCO.Domain.Object;

namespace SSVCO.Reflector
{
    public class Reflector : IReflector
    {
        public Reflector(IConfigurationRoot cfg) {}

        public Object DescribePrototype(Type type)
        {
            return new Object();
        }

        public Object DescribeInstance<T>(T instance)
        {
            return new Object();
        }
        
        public PrototypeDescription BuildPrototype(Object obj)
        {
            return new PrototypeDescription();
        }

        public dynamic BuildInstance(Type type, Object obj)
        {
            return new object();
        }

        public Object CompareGetDiff<T>(T version, T instance)
        {
            return new Object();
        }
    }
}