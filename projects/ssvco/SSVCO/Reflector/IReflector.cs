using System;
using SSVCO.Domain;
using Object = SSVCO.Domain.Object;

namespace SSVCO.Reflector
{
    // IReflector строит описание классов и объектов во внутреннем представлении системы
    public interface IReflector
    {
        public Object               DescribePrototype(Type type);
        public Object               DescribeInstance<T>(T  instance);
        public PrototypeDescription BuildPrototype(Object  obj);
        public dynamic              BuildInstance(Type     type,    Object obj);
        public Object               CompareGetDiff<T>(T    version, T      instance);
    }
}