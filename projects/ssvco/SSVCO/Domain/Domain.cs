using System;
using System.Collections.Generic;

namespace SSVCO.Domain
{
    public class Vertex
    {
        public int    Id        { get; set; }
        public string Name      { get; set; }
        public string Namespace { get; set; }
        public string Type      { get; set; }
        public string Value     { get; set; }

        public Vertex() { }
    }

    public class Edge
    {
        public int    Id         { get; set; }
        public string Label      { get; set; }
        public int    FromVertex { get; set; }
        public int    ToVertex   { get; set; }
        
        public Edge() { }
    }

    public class Object
    {
        public int            Id       { get; set; }
        public int         Type     { get; set; }
        public List<Vertex> Vertexes { get; set; }
        public List<Edge>   Edges    { get; set; }
        
        public Object() { }
    }
    
    public class Author
    {
        public int    Id           { get; set; }
        public string Name         { get; set; }
        public string Password     { get; set; }
        public int    AccessRights { get; set; }
        
        public Author() { }
    }

    public class Branch
    {
        public int    Id           { get; set; }
        public string Name         { get; set; }
        public int    AuthorId     { get; set; }

        public Branch() { }

        public static Branch MasterBranch()
        {
            var master = new Branch
            {
                Id = 1,
                Name = "master",
                AuthorId = 1,
            };
            return master;
        } 
    }

    public class Commit
    {
        public int      Id           { get; set; }
        public int      PrevCommitId { get; set; }
        public string   Comment      { get; set; }
        public DateTime CreatedAt    { get; set; }
        public int      AuthorId     { get; set; }
        public int      BranchId     { get; set; }

        public Commit() { }
        public static Commit EmptyCommit(int branchId, int authorId)
        {
            var commit = new Commit
            {
                Id = 0,
                PrevCommitId = 0,
                BranchId = branchId,
                Comment = "first_commit",
                AuthorId = authorId
            };
            return commit;
        } 
    }

    public class Diff
    {
        public Object                          Frst    { get; set; }
        public Object                          Scnd    { get; set; }
        public Tuple<List<Vertex>, List<Edge>> Deleted { get; set; }
        public Tuple<List<Vertex>, List<Edge>> Updated { get; set; }
        public Tuple<List<Vertex>, List<Edge>> New { get; set; }
        public Diff() { }
    }

    public class PrototypeDescription
    {
        public PrototypeDescription() { }
    }
}