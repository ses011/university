using System;
using System.Collections.Generic;
using SSVCO.Domain;
using Object = SSVCO.Domain.Object;

namespace SSVCO.Storage
{
    // Сохраняет сложные объекты и их версии в долгосрочном хранилище
    public interface IStorage: IDisposable
    {
        public void   Dispose();
        
        // Работа с пользователями в бд
        public int    CreateAuthor(Author    author);
        public Author GetAuthorById(int      id);
        public Author GetAuthorByName(string name);
        public bool   IsAuthorExists(Author  author);

        // Работа с ветками в бд
        public Branch GetCurrentAuthorBranch(Author author);
        public int    CreateBranch(Branch           branch);
        public Branch GetBranchById(int             id);
        public Branch GetBranchByName(string        name);
        
        // Работа с коммитами в бд
        public int          CreateCommit(Commit          commit, List<Object> objects);
        public Commit       GetLastCommitOnBranch(Branch branch);
        public Commit       GetCommit(Commit             commit);
        public List<Object> GetCommitedObjects(Commit    commit, Object obj = null);
        
        // Работа с объектами в бд
        public Object GetObjectById(int objectId);
    }
}