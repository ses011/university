using System; 
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using SSVCO.Domain;
using SSVCO.Exceptions;
using Object = SSVCO.Domain.Object;

namespace SSVCO.Storage
{
    public class SQLiteStorage : IStorage, IDisposable
    {
        private readonly string           _databaseDir;
        private readonly string           _databaseFileName;
        private readonly string           _initScriptName;
        private readonly string           _purgeScriptName;
        private          SQLiteConnection _conn;

        public SQLiteStorage(IConfigurationRoot cfg)
        {
            this._databaseDir = cfg.GetValue<string>("database:dir");
            this._databaseFileName = cfg.GetValue<string>("database:file");
            this._initScriptName = cfg.GetValue<string>("database:init_script");
            this._purgeScriptName = cfg.GetValue<string>("database:purge_script");
            
            this.InitDatabase();
            this.InsertDefaultInfo(cfg);
        }

        private void InsertDefaultInfo(IConfigurationRoot cfg)
        {
            var queries = new List<string>
            {
                @"INSERT OR IGNORE INTO SerializationType(id, val) VALUES (@id, @val)",
                @"INSERT OR IGNORE INTO ObjectType(id, name) VALUES (@id, @val)",
                @"INSERT OR IGNORE INTO Author(id, name, password, access_rights) VALUES (@id, @name, @pass, @rights)",
                @"INSERT OR IGNORE INTO AccessRight(id, name) VALUES (@id, @name)",
                @"INSERT OR IGNORE INTO Branch(branch_name, author_id) VALUES (@bname, @aid)"
            };
            var sect = cfg.GetSection("ssvco");
            
            var cmd = new SQLiteCommand(this._conn);
            cmd.CommandText = queries[0];
            foreach (var e in sect.GetSection("serialization_types").GetChildren())
            {
                cmd.Parameters.AddWithValue("@id", e.Value);
                cmd.Parameters.AddWithValue("@val", e.Key);
            }
            cmd.Prepare();
            cmd.ExecuteNonQuery();
            
            cmd = new SQLiteCommand(this._conn);
            cmd.CommandText = queries[1];
            foreach (var e in sect.GetSection("stored_types").GetChildren())
            {
                cmd.Parameters.AddWithValue("@id", e.Value);
                cmd.Parameters.AddWithValue("@val", e.Key);
            }
            cmd.Prepare();
            cmd.ExecuteNonQuery();
            
            cmd = new SQLiteCommand(this._conn);
            cmd.CommandText = queries[2];
            cmd.Parameters.AddWithValue("@id", 1);
            cmd.Parameters.AddWithValue("@name", cfg.GetValue<string>("default:user"));
            cmd.Parameters.AddWithValue("@pass", cfg.GetValue<string>("default:password"));
            cmd.Parameters.AddWithValue("@access_rights", 3);
            cmd.Prepare();
            cmd.ExecuteNonQuery();
            
            cmd = new SQLiteCommand(this._conn);
            cmd.CommandText = queries[3];
            foreach (var e in sect.GetSection("access_rigts").GetChildren())
            {
                cmd.Parameters.AddWithValue("@id", e.Value);
                cmd.Parameters.AddWithValue("@name", e.Key);
            }
            cmd.Prepare();
            cmd.ExecuteNonQuery();
            
            cmd = new SQLiteCommand(this._conn);
            cmd.CommandText = queries[4];
            cmd.Parameters.AddWithValue("@bname", cfg.GetValue<string>("default:branch"));
            cmd.Parameters.AddWithValue("@aid", 1);
            cmd.Prepare();
            cmd.ExecuteNonQuery();
            cmd.Prepare();
            cmd.ExecuteNonQuery();
        }

        private void InitDatabase()
        {
            if (!Directory.Exists(this._databaseDir))
            {
                throw new SSVCOInitException($"Cant find database directory in path: {this._databaseDir}");
            }

            var connString = $"URI=file:{this._databaseDir}/{_databaseFileName}";
            this._conn = new SQLiteConnection(connString);
            this._conn.Open();

            using var cmd = new SQLiteCommand(this._conn);
            cmd.CommandText = File.ReadAllText($"{this._databaseDir}/{_initScriptName}");;
            cmd.ExecuteNonQuery();
        }

        ~SQLiteStorage() { this.Dispose(); }

        public void Dispose() { this._conn.Close(); }
        
        public void Purge()
        {
            if (!Directory.Exists(this._databaseDir))
            {
                throw new SSVCOInitException($"Cant find database directory in path: {this._databaseDir}");
            }

            var connString = $"URI=file:{this._databaseDir}/{_databaseFileName}";
            this._conn = new SQLiteConnection(connString);
            this._conn.Open();

            using var cmd = new SQLiteCommand(this._conn);
            cmd.CommandText = File.ReadAllText($"{this._databaseDir}/{_purgeScriptName}");
            cmd.ExecuteNonQuery();
        }

        private void ExecuteQuery(string query)
        {
            using var cmd = new SQLiteCommand(this._conn);
            cmd.CommandText = query;
            cmd.ExecuteNonQuery();
        }

        private SQLiteDataReader ExecuteQueryWithResult(string query)
        {
            using var cmd = new SQLiteCommand(this._conn);
            cmd.CommandText = query;
            return cmd.ExecuteReader();
        }

        public int CreateAuthor(Author author)
        {
            string query = $@"INSERT INTO Author(name, password, access_rights) VALUES
                ('{author.Name}', '{author.Password}', {author.AccessRights}) RETURNING id";
            var reader = this.ExecuteQueryWithResult(query);
            if (!reader.Read())
            {
                throw new InsertException($"Cant create author: author with name {author.Name} already exists");
            }
            return reader.GetInt32(0);
        }

        public Author GetAuthorById(int id)
        {
            string query = $@"SELECT a.id, a.name, a.passoword, a.access_rights FROM Author a WHERE id = {id}";
            var reader = this.ExecuteQueryWithResult(query);
            
            if (!reader.Read())
            {
                throw new NotFoundException($"Author with id={id} doesnt exists");
            }

            var result = new Author
            {
                Id = reader.GetInt32(0),
                Name = reader.GetString(1),
                Password = reader.GetString(2),
                AccessRights = reader.GetInt32(3)
            };
            return result;
        }
        
        public Author GetAuthorByName(string name)
        {
            string query = $@"SELECT a.id, a.name, a.passoword, a.access_rights FROM Author a WHERE name = '{name}'";
            var reader = this.ExecuteQueryWithResult(query);
            
            if (!reader.Read())
            {
                throw new NotFoundException($"Author with name={name} doesnt exists");
            }

            var result = new Author
            {
                Id = reader.GetInt32(0),
                Name = reader.GetString(1),
                Password = reader.GetString(2),
                AccessRights = reader.GetInt32(3)
            };
            return result;
        }

        public bool IsAuthorExists(Author author)
        {
            string query = $@"SELECT id FROM Author
                WHERE name = '{author.Name}' AND password = '{author.Password}'";
            var reader = this.ExecuteQueryWithResult(query);

            if (!reader.Read())
            {
                return false;
            }

            return true;
        }

        public Branch GetCurrentAuthorBranch(Author author)
        {
            string query = $@"
                WITH LastCommit AS (
                    SELECT c.branch_id as branch_id 
                    FROM `Commit` c WHERE c.author_id = {author.Id} 
                    ORDER BY c.created_at DESC LIMIT 1;
                )

                SELECT b.id, b.branch_name, b.author_id 
                FROM LastCommit lc JOIN Branch b ON lc.branch_id = b.id 
                WHERE lc.branch_id IS NOT NULL LIMIT 1";
            var reader = this.ExecuteQueryWithResult(query);

            if (!reader.Read())
            {
                return Branch.MasterBranch();
            }
            
            var result = new Branch
            {
                Id = reader.GetInt32(0),
                Name = reader.GetString(1),
                AuthorId = reader.GetInt32(2)
            };
            return result;
        }

        public int CreateBranch(Branch branch)
        {
            string query = $@"INSERT INTO Branch(branch_name, author_id) VALUES
                ('{branch.Name}', {branch.AuthorId}) RETURNING id";
            var reader = this.ExecuteQueryWithResult(query);
            
            if (!reader.Read())
            {
                throw new InsertException($"Cant create branch: branch with name={branch.Name} already exists");
            }
            
            return reader.GetInt32(0);
        }

        public Branch GetBranchById(int id)
        {
            string query = $@"SELECT id, branch_name, author_id FROM Branch WHERE id = {id}";
            var reader = this.ExecuteQueryWithResult(query);

            if (!reader.Read())
            {
                throw new NotFoundException($"Branch with id={id} doesnt exists");
            }

            return new Branch
            {
                Id = reader.GetInt32(0),
                Name = reader.GetString(1),
                AuthorId = reader.GetInt32(2)
            };
        }
        
        public Branch GetBranchByName(string name)
        {
            string query = $@"SELECT id, branch_name, author_id FROM Branch WHERE name = '{name}'";
            var reader = this.ExecuteQueryWithResult(query);

            if (!reader.Read())
            {
                throw new NotFoundException($"Branch with name={name} doesnt exists");
            }

            return new Branch
            {
                Id = reader.GetInt32(0),
                Name = reader.GetString(1),
                AuthorId = reader.GetInt32(2)
            };
        }
        
        private int InsertCommit(Commit commit)
        {
            string query = $@"INSERT INTO Commit(prev_commit_id, branch_id, comment, author_id) VALUES
                ({commit.Id}, {commit.BranchId}, '{commit.Comment}', {commit.AuthorId}) RETURNING id";
            var reader = this.ExecuteQueryWithResult(query);
            
            if (!reader.Read())
            {
                throw new InsertException("Cant create commit");
            }
            
            return reader.GetInt32(0);
        }
        
        private List<Vertex> InsertVertexes(List<Vertex> vertexes)
        {
            var cmd = new SQLiteCommand(this._conn);
            cmd.CommandText = @"INSERT INTO Vertex(name, namespace, vertex_type, vertex_value) VALUES
                    (@name, @namaspace, @type, @value) RETURNING id";

            foreach (var v in vertexes)
            {
                cmd.Parameters.AddWithValue("@name", v.Name);
                cmd.Parameters.AddWithValue("@namespace", v.Namespace);
                cmd.Parameters.AddWithValue("@type", v.Type);
                cmd.Parameters.AddWithValue("@value", v.Value);
            }
            
            cmd.Prepare();
            var reader = cmd.ExecuteReader();
            var res = new List<int>();
            while (reader.Read())
            {
                res.Add(reader.GetInt32(0));
            }

            if (res.Count != vertexes.Count)
            {
                throw new InsertException("Cant insert vertexes in db");
            }
            
            return this.MapVertexesIds(vertexes, res);
        }
        
        private List<Vertex> MapVertexesIds(List<Vertex> vertexes, List<int> vertexIds)
        {
            for (int i = 0; i < vertexes.Count; i++)
            {
                vertexes[i].Id = vertexIds[i];
            }

            return vertexes;
        }

        private List<Edge> InsertEdges(List<Vertex> vertexes, List<Edge> edges)
        {
            var cmd = new SQLiteCommand(this._conn);
            cmd.CommandText = @"INSERT INTO Edge(label, from_vertex, to_vertex) VALUES 
                    (@label, @from, @to) RETURNING id";

            foreach (var e in edges)
            {
                cmd.Parameters.AddWithValue("@label", e.Label);
                cmd.Parameters.AddWithValue("@from", vertexes[e.FromVertex].Id);
                cmd.Parameters.AddWithValue("@to", vertexes[e.ToVertex].Id);
            }
            
            cmd.Prepare();
            var reader = cmd.ExecuteReader();
            var res = new List<int>();
            while (reader.Read())
            {
                res.Add(reader.GetInt32(0));
            }

            if (res.Count != edges.Count)
            {
                throw new InsertException("Cant insert edges in db");
            }
            
            return this.MapEdgesIds(edges, res);
        }
        
        private List<Edge> MapEdgesIds(List<Edge> edges, List<int> edgesIds)
        {
            for (int i = 0; i < edges.Count; i++)
            {
                edges[i].Id = edgesIds[i];
            }

            return edges;
        }

        private List<Object> InsertObjects(List<Object> objects)
        {
            var cmd = new SQLiteCommand(this._conn);
            cmd.CommandText = @"INSERT INTO Object(object_type) VALUES (@type) RETURNING id";

            foreach (var o in objects)
            {
                cmd.Parameters.AddWithValue("@type", o.Type);
            }

            cmd.Prepare();
            var reader = cmd.ExecuteReader();
            var res = new List<int>();
            while (reader.Read())
            {
                res.Add(reader.GetInt32(0));
            }

            if (res.Count != objects.Count)
            {
                throw new InsertException("Cant insert objects in db");
            }
            
            return this.MapObjectIds(objects, res);
        }
        
        private List<Object> MapObjectIds(List<Object> objects, List<int> objectIds)
        {
            for (int i = 0; i < objects.Count; i++)
            {
                objects[i].Id = objectIds[i];
            }

            return objects;
        }

        private void ConnectEdgesVertexesObjects(List<Object> objects, 
            List<Vertex> vertexes, List<Edge> edges)
        {
            List<Tuple<int, int, int>> args = new List<Tuple<int, int, int>>();
            var vCount = 0;
            var eCount = 0;
            for (var i = 0; i < objects.Count; i++)
            {
                var curVCount = objects[i].Vertexes.Count ;
                var curECount = objects[i].Edges.Count;
                args.AddRange(new List<Tuple<int,int,int>>(curECount+curVCount));

                Parallel.For(vCount, vCount+curVCount,
                    (j) =>
                    {
                        args[vCount+eCount+j] = new Tuple<int, int, int>(objects[i].Id, vertexes[j].Id, 1);
                    });
                Parallel.For(eCount, eCount+curECount,
                    (j) =>
                    {
                        args[vCount+eCount+curVCount+j] = new Tuple<int, int, int>(objects[i].Id, edges[j].Id, 2);
                    });

                vCount += curVCount;
                eCount += curECount;
            }
            
            var cmd = new SQLiteCommand(this._conn);
            cmd.CommandText = @"INSERT INTO ObjectToVertexEdge(object_id, item_id, item_type) 
                    VALUES (@object_id, @item_id, @type)";
            foreach (var a in args)
            {
                cmd.Parameters.AddWithValue("@object_id", a.Item1);
                cmd.Parameters.AddWithValue("@item_id", a.Item2);
                cmd.Parameters.AddWithValue("@type", a.Item3);
            }
            cmd.Prepare();
            cmd.ExecuteNonQuery();
        }

        private List<Vertex> ExtractVertexesFromObjects(List<Object> objects)
        {
            var res = new List<Vertex>();
            foreach (var o in objects)
            {
                res.AddRange(o.Vertexes);
            }

            return res;
        }
        
        private List<Edge> ExtractEdgesFromObjects(List<Object> objects)
        {
            var res = new List<Edge>();
            foreach (var o in objects)
            {
                res.AddRange(o.Edges);
            }

            return res;
        }

        private void ConnectCommitObjects(Commit commit, List<Object> objects)
        {
            var cmd = new SQLiteCommand(this._conn);
            cmd.CommandText = @"INSERT INTO CommitToObject(commit_id, object_id) VALUES (@commit_id, @object_id)";

            foreach (var o in objects)
            {
                cmd.Parameters.AddWithValue("@commit_id", commit.Id);
                cmd.Parameters.AddWithValue("@object_id", o.Id);
            }
            cmd.Prepare();
            cmd.ExecuteNonQuery();
        }

        public int CreateCommit(Commit commit, List<Object> objects)
        {
            var allVertexes = this.ExtractVertexesFromObjects(objects);
            var allEdges = this.ExtractEdgesFromObjects(objects);
            
            using (var transaction = this._conn.BeginTransaction(IsolationLevel.ReadCommitted))
            {
                try
                {
                    commit.Id = this.InsertCommit(commit);
                    var updatedVertexes = this.InsertVertexes(allVertexes);
                    var updatedEdges = this.InsertEdges(allVertexes, allEdges);
                    var updatedObjects = this.InsertObjects(objects);
                    this.ConnectEdgesVertexesObjects(updatedObjects, updatedVertexes, updatedEdges);
                    this.ConnectCommitObjects(commit, updatedObjects);
                } catch (Exception e)
                {
                    transaction.Rollback();
                    throw e;
                }
                
                transaction.Commit();
            }

            return commit.Id;
        }
        
        public Commit GetLastCommitOnBranch(Branch branch)
        {
            var query = $@"SELECT id, prev_commit_id, branch_id, comment, created_at, author_id 
                    FROM `Commit` WHERE branch_id = {branch.Id} ORDER BY created_at DESC LIMIT 1";
            var reader = this.ExecuteQueryWithResult(query);

            if (!reader.Read())
            {
                throw new NotFoundException($"No commits on branch: {branch.Id}-{branch.Name}");
            }
            
            return new Commit
            {
                Id = reader.GetInt32(0),
                PrevCommitId = reader.GetInt32(1),
                BranchId = reader.GetInt32(2),
                Comment = reader.GetString(3),
                CreatedAt = reader.GetDateTime(4),
                AuthorId = reader.GetInt32(5)
            };
        }
        
        public Commit GetCommit(Commit commit)
        {
            var query = $@"SELECT prev_commit_id, branch_id, comment, created_at, author_id
                    FROM `Commit` WHERE id = {commit.Id}";
            var reader = this.ExecuteQueryWithResult(query);

            if (!reader.Read())
            {
                throw new NotFoundException($"Unknown commit id: {commit.Id}");
            }

            commit.PrevCommitId = reader.GetInt32(0);
            commit.BranchId = reader.GetInt32(1);
            commit.Comment = reader.GetString(2);
            commit.CreatedAt = reader.GetDateTime(3);
            commit.AuthorId = reader.GetInt32(4);

            return commit;
        }

        private Dictionary<int, List<Vertex>> GetVertexes(List<int> vertexesIds)
        {
            var args = "";
            foreach (var vid in vertexesIds)
            {
                args += $"{vid},";
            }
            args += "-1";
            var query = $@"WITH get_nec_v as (
                    SELECT v.id, v.name, v.namespace, v.vertex_type, v.vertex_value
                    FROM Vertex v WHERE id IN ({args})
                )
                SELECT v.id, v.name, v.namespace, v.vertex_type, v.vertex_value, ove.object_id
                FROM get_nec_v v JOIN ObjectToVertexEdge ove ON ove.item_type = 0 AND ove.item_id = v.id";
            var reader = this.ExecuteQueryWithResult(query);

            var res = new Dictionary<int, List<Vertex>>();
            while (reader.Read())
            {
                var objectId = reader.GetInt32(5);
                if (!res.ContainsKey(objectId))
                {
                    res[objectId] = new List<Vertex>();
                }
                
                res[objectId].Add(new Vertex
                {
                    Id = reader.GetInt32(1),
                    Name = reader.GetString(2),
                    Namespace = reader.GetString(3),
                    Type = reader.GetString(4),
                    Value = reader.GetString(5),
                });
            }

            if (res.Count != vertexesIds.Count)
            {
                throw new NotFoundException("Cant extract vertexes from db");
            }

            return res;
        }
        
        private Dictionary<int, List<Edge>> GetEdges(List<int> edgesIds)
        {
            var args = "";
            foreach (var eid in edgesIds)
            {
                args += $"{eid},";
            }
            args += "-1";
            var query = $@"WITH get_nec_e as (
                    SELECT e.id, e.label, e.from_vertex, e.to_vertex
                    FROM Edge e WHERE id IN ({args})
                )
                SELECT e.id, e.label, e.from_vertex, e.to_vertex, ove.object_id
                FROM get_nec_e e JOIN ObjectToVertexEdge ove ON ove.item_type = 1 AND ove.item_id = e.id";
            var reader = this.ExecuteQueryWithResult(query);

            var res = new Dictionary<int, List<Edge>>();
            while (reader.Read())
            {
                var objectId = reader.GetInt32(5);
                if (!res.ContainsKey(objectId))
                {
                    res[objectId] = new List<Edge>();
                }
                
                res[objectId].Add(new Edge
                {
                    Id = reader.GetInt32(1),
                    Label = reader.GetString(2),
                    FromVertex = reader.GetInt32(3),
                    ToVertex = reader.GetInt32(4),
                });
            }
            
            if (res.Count != edgesIds.Count)
            {
                throw new NotFoundException("Cant extract edges from db");
            }

            return res;
        }

        public List<Object> CombineObjectVertexesEdges(
            Dictionary<int, Object> objects,
            Dictionary<int, List<Vertex>> vertexes,
            Dictionary<int, List<Edge>> edges)
        {
            foreach (var objectId in objects.Keys)
            {
                if (!vertexes.ContainsKey(objectId))
                {
                    throw new GetException($"No vertexes for object with id={objectId}");
                }
                if (!edges.ContainsKey(objectId))
                {
                    throw new GetException($"No edges for object with id={objectId}");
                }

                objects[objectId].Vertexes = vertexes[objectId];
                objects[objectId].Edges = edges[objectId];
            }

            return objects.Values.ToList();
        }

        public List<Object> GetCommitedObjects(Commit commit, Object obj = null)
        {
            var query = $@"with nec_object as (
                    SELECT o.id as id, o.object_type as object_type
                    FROM CommitToObject cto JOIN Object o ON cto.object_id = o.id 
                      %placeholder% AND cto.commit_id = {commit.Id}
                )
                SELECT no.object_id, ove.object_id, ove.item_id, ove.item_type, ot.id
                FROM nec_object no JOIN ObjectToVertexEdge ove ON no.id = ove.object_id
                    JOIN ObjectType ot ON no.object_type = ot.id 
                ORDER BY ove.object_id";
            query = query.Replace("%placeholder%", obj == null ? "" : "AND o.id = {obj.Id}");
            var reader = this.ExecuteQueryWithResult(query);

            Dictionary<int, Object> objects = new Dictionary<int, Object>();
            List<int> vertexesIds = new List<int>();
            List<int> edgesIds = new List<int>();

            while (reader.Read())
            {
                var objectId = reader.GetInt32(0);
                if (!objects.ContainsKey(objectId))
                {
                    objects[objectId] = new Object
                    {
                        Id = objectId,
                        Type = reader.GetInt32(5)
                    };
                }
                if (reader.GetInt32(4) == 1)
                {
                    vertexesIds.Add(reader.GetInt32(3));
                }
                else
                {
                    edgesIds.Add(reader.GetInt32(3));
                }
            }

            var vertexes = this.GetVertexes(vertexesIds);
            var edges = this.GetEdges(edgesIds);
            
            return this.CombineObjectVertexesEdges(objects, vertexes, edges);
        }

        public Object GetObjectById(int objectId)
        {
            return new Object();
        }
    }
}