CREATE TABLE IF NOT EXISTS SerializationType (
    id INTEGER PRIMARY KEY,
    val TEXT NOT NULL
);
    
CREATE TABLE IF NOT EXISTS Vertex (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    name TEXT NOT NULL,
    namespace TEXT NOT NULL,
    vertex_type TEXT NOT NULL,
    vertex_value TEXT DEFAULT NULL,
    serialization_type INTEGER NOT NULL DEFAULT 0
);

CREATE TABLE IF NOT EXISTS Edge (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    label TEXT DEFAULT NULL,
    from_vertex INTEGER REFERENCES Vertex(id) ON DELETE CASCADE,
    to_vertex INTEGER REFERENCES Vertex(id) ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS ObjectType (
    id INTEGER PRIMARY KEY,
    name TEXT NOT NULL
);

CREATE TABLE IF NOT EXISTS Object(
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    object_type INTEGER REFERENCES ObjectType(id) ON DELETE SET NULL
);

-- 0 - Vertex, 1 - Edge
CREATE TABLE IF NOT EXISTS ObjectToVertexEdge (
    object_id INTEGER REFERENCES Object(id) ON DELETE CASCADE,
    item_id INTEGER NOT NULL,
    item_type INTEGER CHECK(item_type = 1 OR item_type = 2),
    PRIMARY KEY (object_id, item_id, item_type)
);

CREATE TABLE AccessRight (
    id INTEGER PRIMARY KEY,
    name TEXT NOT NULL
);

CREATE TABLE Author (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    name TEXT NOT NULL,
    password TEXT NOT NULL,
    access_rights INTEGER DEFAULT 1 REFERENCES AccessRight(id) ON DELETE SET DEFAULT,
    UNIQUE(name)
);
CREATE INDEX IF NOT EXISTS check_author_idx ON Author(name, password);

CREATE TABLE IF NOT EXISTS Branch (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    branch_name TEXT NOT NULL,
    author_id INTEGER REFERENCES Author(id) ON DELETE CASCADE,
    UNIQUE(branch_name)
);

CREATE TABLE IF NOT EXISTS `Commit` (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    prev_commit_id INTEGER DEFAULT 0,
    branch_id INTEGER REFERENCES Branch(id),
    comment TEXT NOT NULL,
    created_at TEXT DEFAULT CURRENT_TIMESTAMP,
    author_id INTEGER REFERENCES Author(id)
);
CREATE INDEX IF NOT EXISTS commit_branch_idx ON `Commit`(branch_id);

CREATE TABLE IF NOT EXISTS CommitToObject (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    commit_id INTEGER REFERENCES `Commit`(id) ON DELETE CASCADE,
    object_id INTEGER REFERENCES Object(id) ON DELETE CASCADE
);
