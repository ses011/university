-- Полное очищение базы, используется для нагрузочных тестов
DELETE FROM Vertex;
DELETE FROM Edge;
DELETE FROM Object;
DELETE FROM ObjectToVertexEdge;
DROP TABLE Author;
DROP TABLE Branch;
DELETE FROM `Commit`;
DELETE FROM CommitToObject;