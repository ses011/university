using System;

namespace SSVCO.Versioner
{
    // Интерфейс системы семантического версионирования сложных объектов
    public interface IVersioner
    {
        // Идентификация пользователя, системы
        public void          Login(string username, string password);
        public Domain.Author GetCurrentAuthor();
        public int           CreateAuthor(string name, string password, int accessRights);
        public Domain.Branch GetCurrentBranch();
        public int           CreateBranch(string name, bool immediateCheckout = false);
        public void          ChangeBranch(int    branchId);
        public void          ChangeBranch(string branchName);
        public Domain.Commit GetLastCommit();
        public Domain.Commit Commit(string comment);

        // Непосредственно версионирование объектов
        public int         LoadSchema(Type    t);
        public int         Insert<T>(T        instance);
        public int         Update<T>(T        instance, int id);
        public dynamic     Get(Type           t,        int commitId, int instanceId);
        public void        Delete(int         instanceId);
        public Domain.Diff GetVersionDiff(int instanceId, int commitId1, int commitId2);
    }
}