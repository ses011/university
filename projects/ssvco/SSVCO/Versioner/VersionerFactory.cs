using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using Microsoft.Extensions.Configuration;
using SSVCO.Reflector;
using SSVCO.Storage;

namespace SSVCO.Versioner
{
    public static class VersionerFactory
    {
        private static Dictionary<string, Type> _allowedStorages = new()
        {
            {"sqlite", typeof(SQLiteStorage)},
        };
        private static Dictionary<string, Type> _allowedReflectors = new()
        {
            {"reflection", typeof(Reflector.Reflector)},
            // "race"
        };

        public static List<string> GetAllowedStorages()
        {
            return new List<string>(_allowedStorages.Keys);
        }
        
        public static List<string> GetAllowedReflectors()
        {
            return new List<string>(_allowedReflectors.Keys);
        }

        private static void CheckArgs(string storageType, string reflectorType)
        {
            if (!_allowedStorages.ContainsKey(storageType))
            {
                throw new Exception($"Unknown storage type: {storageType}");
            }
            if (!_allowedReflectors.ContainsKey(reflectorType))
            {
                throw new Exception($"Unknown reflector type: {reflectorType}");
            }
        }

        public static IVersioner NewVersioner(
            string storageType   = "sqlite",
            string reflectorType = "reflection",
            bool autoCommit = false)
        {
            var cfg =
                new ConfigurationBuilder()
                    .SetBasePath(Directory.GetCurrentDirectory())
                    .AddJsonFile("appsettings.json", true)
                    .Build();
            
            CheckArgs(storageType, reflectorType);
            
            var storage = (IStorage)_allowedStorages[storageType]
                .GetConstructor(new[] { typeof(IConfigurationRoot) })?
                .Invoke(new object[] { cfg });
            var reflector = (IReflector)_allowedReflectors[reflectorType]
                .GetConstructor(new[] { typeof(IConfigurationRoot) })?
                .Invoke(new object[] { cfg });
            var res = (IVersioner)typeof(Versioner)
                .GetConstructor(
                    BindingFlags.NonPublic | BindingFlags.CreateInstance | BindingFlags.Instance, 
                    null, 
                    new[] { typeof(IVersioner), typeof(IReflector), typeof(bool) }, 
                    null
                )
                ?.Invoke(new object[] { storage, reflector, autoCommit });

            return res;
        }
    }
}