using System;
using System.Collections.Generic;
using SSVCO.Storage;
using SSVCO.Domain;
using SSVCO.Exceptions;
using SSVCO.Reflector;
using Object = SSVCO.Domain.Object;

namespace SSVCO.Versioner
{
    public class Versioner : IVersioner
    {
        // Используемые компоненты
        private readonly IStorage   _storage;
        private readonly IReflector _reflector;

        // Информация непосредственно используемая для версионирования
        private          Author _currentAuthor;
        private          Branch _currentBranch;
        private          Commit _currentCommit;
        private readonly bool   _autoCommit;

        protected Versioner(IStorage storage, IReflector reflector, bool autoCommit)
        {
            this._storage = storage;
            this._reflector = reflector;
            this._autoCommit = autoCommit;
        }

        ~Versioner()
        {
            this._storage.Dispose();
        }

        private void _checkAuth(int neededAccess = -1)
        {
            if (this._currentAuthor == null || this._currentBranch == null)
            {
                throw new SSVCOUsageException("Please, use method 'Login' before save any changes");
            }
            if (this._currentAuthor.AccessRights < neededAccess)
            {
                throw new SSVCOUsageException("Not enough access right to perform action");
            }
        }

        private dynamic _updateStoredObjects(Object o)
        {
            if (this._autoCommit)
            {
                this._currentCommit.Comment = "auto_commited";
                var id = this._storage.CreateCommit(this._currentCommit, new List<Object>{o});
                this._currentCommit = this._storage.GetCommit(new Commit{Id = id});
            }
            else
            {
                throw new Exception("not implemented");
            }
            return this._currentCommit.Id;
        }
        
        public void Login(string username, string password)
        {
            var a = new Author
            {
                Name = username,
                Password = password
            };
            
            if (!this._storage.IsAuthorExists(a))
            {
                throw new NotFoundException($"Author with name={username} dont exists");
            }

            this._currentAuthor = this._storage.GetAuthorByName(username);
            this._currentBranch = this._storage.GetCurrentAuthorBranch(this._currentAuthor);
            try
            {
                this._currentCommit = this._storage.GetLastCommitOnBranch(this._currentBranch);
            }
            catch (NotFoundException e)
            {
                this._currentCommit = Domain.Commit.EmptyCommit(this._currentBranch.Id, this._currentAuthor.Id);
            }
        }

        public int CreateAuthor(string name, string password, int accessRights)
        {
            this._checkAuth(neededAccess:2);
            var a = new Author
            {
                Name = name,
                Password = password,
                AccessRights = accessRights
            };
            return this._storage.CreateAuthor(a);
        }

        public int CreateBranch(string name, bool immediateCheckout = false)
        {
            this._checkAuth(neededAccess:2);
            var b = new Branch
            {
                AuthorId = this._currentAuthor.Id,
                Name = name,
            };
            var res = this._storage.CreateBranch(b);
            this._currentBranch = new Branch
            {
                Id = res,
                AuthorId = this._currentAuthor.Id,
                Name = name,
            };
            return res;
        }

        public void ChangeBranch(int branchId)
        {
            this._checkAuth();
            this._currentBranch = this._storage.GetBranchById(branchId);
        }
        
        public void ChangeBranch(string branchName)
        {
            this._checkAuth();
            this._currentBranch = this._storage.GetBranchByName(branchName);
        }

        public Author GetCurrentAuthor()
        {
            return this._currentAuthor;
        }

        public Branch GetCurrentBranch()
        {
            return this._currentBranch;
        }

        public Commit GetLastCommit()
        {
            return this._currentCommit;
        }

        public Commit Commit(string message)
        {
            if (!this._autoCommit)
            {
                throw new Exception("not implemented");
            }
            return this._currentCommit;
        }

        public int LoadSchema(Type t)
        {
            this._checkAuth();
            var obj = this._reflector.DescribePrototype(t);
            return this._updateStoredObjects(obj);
        }

        public int Insert<T>(T instance)
        {
            this._checkAuth();
            var obj = this._reflector.DescribeInstance(instance);
            return this._updateStoredObjects(obj);
        }

        private T GetObjectVersionById<T>(int id)
        {
            Object storedInstance;
            if (this._autoCommit) {
                storedInstance = this._storage.GetObjectById(id);
            }
            else
            {
                throw new Exception("not implemented");
            }
            return this._reflector.BuildInstance(typeof(T), storedInstance);
        }

        public int Update<T>(T instance, int id)
        {
            this._checkAuth();
            var version = this.GetObjectVersionById<T>(id);
            var changes = this._reflector.CompareGetDiff(version, instance);
            return this._updateStoredObjects(changes);
        }

        public dynamic Get(Type t, int commitId, int instanceId)
        {
            this._checkAuth();
            
            var commit = new Commit {Id = commitId};
            var obj = new Object {Id = instanceId};
            
            var objs = this._storage.GetCommitedObjects(commit, obj);
            if (objs.Count != 1)
            {
                throw new GetException($"Objects collision: want 1 object, get {objs.Count}");
            }

            if (objs[0].Type == 1)
            {
                return this._reflector.BuildPrototype(objs[0]);
            }
            return this._reflector.BuildInstance(t, objs[0]);
        }
        
        public void Delete(int instanceId)
        {
            this._checkAuth();
            throw new Exception("not implemented");
        }
        
        public Diff GetVersionDiff(int instanceId, int commitId1, int commitId2)
        {
            this._checkAuth();
            throw new Exception("not implemented");
        }
    }
}