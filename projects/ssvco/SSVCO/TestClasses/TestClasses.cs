using System;
using System.Collections.Generic;

namespace SSVCO.TestClasses
{
    public class User
    {
        private int    _id;
        private string _login;
        private string _password;
        private City   _city;

        public User() { }

        public User(string login, string password, string cityName)
        {
            this._login = login;
            this._password = password;
            this._city = new(cityName, "Russia");
        }

        public User(int id, string login, string password, string cityName)
        {
            this._id = id;
            this._login = login;
            this._password = password;
            this._city = new(cityName, "Russia");
        }
        
        public void SetPassword(string newPassword)
        {
            this._password = newPassword;
        }

        public int Id { get => _id; set => _id = value; }

        public override string ToString()
        {
            return $"User: id={this._id}, login={this._login}, password={this._password}, city={this._city}";
        }
    }

    public class City
    {
        private int    _id;
        private string _country;
        private string _name;
        
        public City() {}

        public City(string city, string country)
        {
            this._name = city;
            this._country = country;
        }
        
        public City(int id, string city, string country)
        {
            this._id = id;
            this._name = city;
            this._country = country;
        }

        public string GetCounty => this._country;
        public string GetName   => this._name;

        public override string ToString()
        {
            return $"name: {this._name}, country: {this._country}";
        }
    }

    public class ProjectInfo
    {
        private int               _id;
        private GeneralInfo       _generalInfo;
        private IList<Chapter>    _chapters;
        private IList<Literature> _literatures;
        
        public ProjectInfo() {}

        public ProjectInfo(string title, string theme, string description)
        {
            this._generalInfo = new(title, theme, description);
            this._chapters = new List<Chapter>();
            this._literatures = new List<Literature>();
        }
        
        public ProjectInfo(int id, string title, string theme, string description)
        {
            this._id = id;
            this._generalInfo = new(title, theme, description);
            this._chapters = new List<Chapter>();
            this._literatures = new List<Literature>();
        }

        public void AddChapter(string title, string content)
        {
            this._chapters.Add(new Chapter(title, content));
        }

        public void AttachLiterature(string title, string description, string url)
        {
            this._literatures.Add(new Literature(title, description, url));
        }

        public override string ToString()
        {
            var chapInfo = string.Join(", ", this._chapters);
            var litInfo = string.Join(", ", this._literatures);
            return String.Format(
                "General info:\n{0}\n Chapters:\n{1}\n Literatures:\n{2}\n",
                this._generalInfo,
                chapInfo,
                litInfo
            );
        }
    }

    public class GeneralInfo
    {
        private int    _id;
        private string _title;
        private string _theme;
        private string _description;

        public GeneralInfo(string title, string theme, string description)
        {
            this._title = title;
            this._theme = theme;
            this._description = description;
        }
        
        public GeneralInfo(int id, string title, string theme, string description)
        {
            this._id = id;
            this._title = title;
            this._theme = theme;
            this._description = description;
        }

        public string GetTitle       => this._title;
        public string GetTheme       => this._theme;
        public string GetDescription => this._description;
        
        public override string ToString()
        {
            return String.Format(
                "Title: {0}, theme: {1}, description: {2}",
                this._title,
                this._theme,
                this._description
            );
        }
    }

    public class Chapter
    {
        public int    _id;
        public string _title;
        public string _content;

        public Chapter(string title, string content)
        {
            this._title = title;
            this._content = content;
        }

        public Chapter(int id, string title, string content)
        {
            this._id = id;
            this._title = title;
            this._content = content;
        }

        public string GetTitle   => this._title;
        public string GetContent => this._content;
        
        public override string ToString()
        {
            return String.Format(
                "Title: {0}, content: {1}",
                this._title,
                this._content
            );
        }
    }

    public class Literature
    {
        private int    _id;
        private string _title;
        private string _description;
        private string _url;

        public Literature(string title, string description, string url)
        {
            this._title = title;
            this._description = description;
            this._url = url;
        }
        
        public Literature(int id, string title, string description, string url)
        {
            this._id = id; 
            this._title = title;
            this._description = description;
            this._url = url;
        }

        public string GetTitle       => this._title;
        public string GetDescription => this._description;
        public string GetUrl         => this._url;
        
        public override string ToString()
        {
            return String.Format(
                "Title: {0}, description: {1}, url: {2}",
                this._title,
                this._description,
                this._url
            );
        }
    }
}