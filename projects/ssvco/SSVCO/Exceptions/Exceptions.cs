using System;

namespace SSVCO.Exceptions
{
    public class NotFoundException : Exception
    {
        public NotFoundException() { }
        public NotFoundException(string message) : base(message) { }
        public NotFoundException(string message, Exception inner) : base(message, inner) { }
    }
    
    public class GetException : Exception
    {
        public GetException() { }
        public GetException(string message) : base(message) { }
        public GetException(string message, Exception inner) : base(message, inner) { }
    }
    
    public class InsertException : Exception
    {
        public InsertException() { }
        public InsertException(string message) : base(message) { }
        public InsertException(string message, Exception inner) : base(message, inner) { }
    }
    
    public class SSVCOInitException : Exception
    {
        public SSVCOInitException() { }
        public SSVCOInitException(string message) : base(message) { }
        public SSVCOInitException(string message, Exception inner) : base(message, inner) { }
    }
    
    public class SSVCOUsageException : Exception
    {
        public SSVCOUsageException() { }
        public SSVCOUsageException(string message) : base(message) { }
        public SSVCOUsageException(string message, Exception inner) : base(message, inner) { }
    }
}