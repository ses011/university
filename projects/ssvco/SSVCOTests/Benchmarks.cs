// using BenchmarkDotNet.Attributes;
// using BenchmarkDotNet.Engines;
// using TestClasses;
// using Versioner;
//
// namespace ConsoleApp1
// {
//     [MemoryDiagnoser]
//     [SimpleJob(
//         RunStrategy.Throughput,
//         launchCount: 1,
//         warmupCount: 5,
//         targetCount: 30,
//         baseline: true
//         )]
//     
//     public class BenchmarksVersioner
//     {
//         public Versioner.Versioner Versioner;
//         public User                TestUser1;
//         public User                TestUser2;
//
//         [GlobalSetup]
//         public void SetUp()
//         {
//             this.Versioner = new Versioner.Versioner();
//             this.TestUser1 = new User("123", "666", "yyy");
//             this.TestUser2 = new User("iii", "9910", "kokoko");
//         }
//
//         [GlobalCleanup]
//         public void CleanUp() { this.Versioner.Purge(); }
//
//         [Benchmark]
//         public void SaveInstance()
//         {
//             this.Versioner.Insert(this.TestUser1);
//         }
//         
//         [Benchmark]
//         public void GetVersion()
//         {
//             var ff = this.Versioner.Insert(this.TestUser1);
//             var ll = this.Versioner.Get(typeof(User), ff);
//         }
//
//         [Benchmark]
//         public void SavePrototype()
//         {
//             this.Versioner.LoadSchema(typeof(User));
//         }
//
//         [Benchmark]
//         public void SaveVersion()
//         {
//             var userId = this.Versioner.Insert(this.TestUser1);
//             var commitId = this.Versioner.Update(this.TestUser2, userId);
//         }
//     }
// }