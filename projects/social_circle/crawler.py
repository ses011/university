import vk
import time
import json
import psycopg2
from transliterate import translit
from User import *


with open("config", "r") as read_file:
    f = json.load(read_file)
access_token = f['access_token']
session = vk.Session(access_token)
vk_api = vk.API(session)
con = psycopg2.connect(database=f['database'], user=f['user'],
                       password=f['password'], host=f['host'])
VERSION = f['api_version']
DELAY = 0.6


def main():
    print('working')
    with con.cursor() as cur:
        cur.execute("SELECT * from USERS")
        data = cur.fetchall()
    users = []
    for row in data:
        user = User(row[0], row[1], row[2], row[3], row[4], row[6], row[7])
        users.append(user)
    for user in users:
        print(user.id)
        all_content = 0
        try:
            posts = vk_api.wall.get(owner_id=user.id, v=VERSION)['count']
            time.sleep(DELAY)
            photos = vk_api.photos.getAll(owner_id=user.id, v=VERSION)['count']
            time.sleep(DELAY)
        except:
            posts = photos = 0
        all_content += photos + posts
        posts = get_all_post(user)
        time.sleep(DELAY)
        db = get_comments_on_posts(user, posts)
        time.sleep(DELAY)
        comments = 0
        for i in db.values():
            comments += i
        with con:
            with con.cursor() as cur:
                cur.execute('UPDATE USERS SET all_content={}, all_comments={} WHERE ID={};'.format(all_content, comments, user.id))

        time.sleep(DELAY)
        groups = vk_api.groups.get(user_id=user.id, v=VERSION)['items']
        time.sleep(DELAY)

        for group in groups:
            with con:
                with con.cursor() as cur:
                    cur.execute('INSERT INTO GROUPS (USERID, GROUP_ID) VALUES ({}, {})'.format(user.id, group))
    con.close()


def analise_user(ui):
    print('Analyse user ', ui)
    rep = vk_api.users.get(user_ids=ui, fields='sex,city,photo_100', v=VERSION)[0]
    time.sleep(DELAY)
    try:
        user = User(rep['id'], rep['first_name'], rep['photo_100'], rep['sex'], rep['city']['title'])
    except Exception as e:
        user = User(rep['id'], rep['first_name'], rep['photo_100'], rep['sex'])
    data_l, data_c = get_info(user)
    with con:
        with con.cursor() as cur:
            cur.execute('UPDATE USERS SET checked=True WHERE ID={};'.format(ui))
    new_users = []
    for pair in data_l:
        with con:
            with con.cursor() as cur:
                cur.execute(
                    ("INSERT INTO LIKES (SOURCEID,USERID,NUM_OF_LIKES) VALUES ({}, {}, {});").format(user.id, pair[0], pair[1]))
        new_users.append(pair[0])
    for pair in data_c:
        with con:
            with con.cursor() as cur:
                cur.execute(
                    ("INSERT INTO COMMENTS (SOURCEID,USERID,NUM_OF_COMMENTS) VALUES ({}, {}, {});").format(user.id, pair[0], pair[1]))
        new_users.append(pair[0])
    if not new_users:
        return -1
    req = str(new_users.pop(0))
    for user in new_users:
        req += ','+str(user)
    rep = vk_api.users.get(user_ids=req, fields='sex,city,photo_100', v=VERSION)
    for user in rep:
        try:
            u = User(user['id'], user['first_name'],user['photo_100'], user['sex'], user['city']['title'])
        except Exception as e:
            u = User(user['id'], user['first_name'], user['photo_100'], user['sex'])
        try:
            with con:
                with con.cursor() as cur:
                    cur.execute(("INSERT INTO USERS (ID,NAME,PHOTO,SEX,CITY) VALUES ({}, \'{}\', \'{}\', {}, \'{}\');")\
                                .format(u.id, translit(u.name, reversed=True).replace('\'', ''), u.photo, u.sex, u.city))
        except:
            pass


def get_all_photos(user):
    offset = 0
    photo_ids = []
    while offset >= 0:
        data = vk_api.photos.getAll(owner_id=user.id, offset=offset, count=200, v=VERSION)['items']
        if data:
            for photo in data:
                photo_ids.append(photo['id'])
            offset += 200
            print("photos offset: ", offset)
            time.sleep(DELAY)
        else:
            offset = -1
    return photo_ids


def get_all_post(user):
    offset = 0
    post_ids = []
    while offset >= 0:
        try:
            data = vk_api.wall.get(owner_id=user.id, offset=offset, count=100, v=VERSION)['items']
            if data:
                for post in data:
                    post_ids.append(post['id'])
                offset += 100
                print("posts offset: ", offset)
            else:
                offset = -1
            time.sleep(DELAY)
        except:
            pass
    return post_ids


def get_likes_on_photo(main_user):
    photo_ids = get_all_photos(main_user)
    data_base_likes = {}
    for photo in photo_ids:
        users = vk_api.likes.getList(type='photo', owner_id=main_user.id, item_id=photo, v=VERSION)['items']
        for user in users:
            if user in data_base_likes.keys():
                data_base_likes[user] += 1
            else:
                data_base_likes.update([(user, 1)])
        time.sleep(DELAY)

    data_base_comments = {}
    offset = 0
    while offset >= 0:
        data = vk_api.photos.getAllComments(owner_id=main_user.id, offset=offset, count=100, v=VERSION)['items']
        if data:
            for item in data:
                if item['from_id'] != main_user.id:
                    if item['from_id'] in data_base_comments.keys():
                        data_base_comments[item['from_id']] += 1
                    else:
                        data_base_comments.update([(item['from_id'], 1)])
            offset += 100
            print("comments of photo offset: ", offset)
            time.sleep(DELAY)
        else:
            offset = -1

    print(data_base_comments)

    return data_base_likes, data_base_comments


def get_likes_on_posts(main_user, posts):
    data_base = {}
    for post in posts:
        offset = 0
        time.sleep(DELAY)
        users = vk_api.likes.getList(type='post', owner_id=main_user.id, item_id=post, v=VERSION)['items']
        while users:
            offset += 100
            print('likes on posts offset ', offset)
            for user in users:
                if user in data_base.keys():
                    data_base[user] += 1
                else:
                    data_base.update([(user, 1)])
            time.sleep(DELAY)
            users = vk_api.likes.getList(type='post', owner_id=main_user.id, offset=offset, item_id=post, v=VERSION)['items']
    return data_base


def get_comments_on_posts(main_user, posts):
    data_base = {}
    users = []
    for post in posts:
        try:
            offset = 0
            resp = vk_api.wall.getComments(owner_id=main_user.id, offset=offset, count=100, post_id=post, v=VERSION)['items']
            time.sleep(DELAY)
            while resp:
                offset += 100
                for info in resp:
                    users.append(info['from_id'])
                resp = vk_api.wall.getComments(owner_id=main_user.id, offset=offset, count=100, post_id=post, v=VERSION)['items']
                time.sleep(DELAY)
        except:
            pass

    for user in users:
        if user != main_user.id:
            if user in data_base.keys():
                data_base[user] += 1
            else:
                data_base.update([(user, 1)])

    print('comments: ', data_base)

    return data_base


def get_info_about_posts(main_user):
    posts = get_all_post(main_user)
    db = get_likes_on_posts(main_user, posts)
    try:
        db2 = get_comments_on_posts(main_user, posts)
    except Exception as e:
        print('User {} is private'.format(main_user.id))
        db2 = {}

    return db, db2


def get_info(user):
    try:
        tmp_db = get_likes_on_photo(user)
        data_base_likes, db1 = tmp_db[0], tmp_db[1]
        tmp_db = get_info_about_posts(user)
        db2, data_base_comments = tmp_db[0], tmp_db[1]


        for key, value in db1.items():
            if key in data_base_comments.keys():
                data_base_comments[key] += value*1
            else:
                data_base_comments.update([(key, value)])

        for key, value in db2.items():
            if key in data_base_likes.keys():
                data_base_likes[key] += value*1
            else:
                data_base_likes.update([(key, value)])

        list_d1 = list(data_base_likes.items())
        list_d1.sort(key=lambda i: i[1])
        list_d2 = list(data_base_comments.items())
        list_d2.sort(key=lambda i: i[1])
    except Exception as e:
        list_d1 = list_d2 = []

    return list_d1[::-1][:200:], list_d2[::-1][:200:]


main()
