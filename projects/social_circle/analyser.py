import json
import psycopg2
from pyvis.network import Network
from User import *

with open("config", "r") as read_file:
    f = json.load(read_file)
access_token = f['access_token']
con = psycopg2.connect(database=f['database'], user=f['user'],
                       password=f['password'], host=f['host'])


def main():
    data, total_items = extract_data(146282536)
    main_user = get_user_info(146282536)
    users = []
    net = Network('1000px', '2000px')
    net.add_node(int(main_user.id), label=main_user.name, shape='image', image=main_user.photo, size=25, color="red",
                 title='sex: {}\n city: {}\n vk: vk.com/id{}\n'.format(main_user.sex, main_user.city, main_user.id))
    info = []
    i = 0
    for pair in data:
        i += 1
        user = get_user_info(pair[0])
        dst = extract_data(main_user.id, user.id)
        info.append((user, dst + pair[1]))
        if (user.id != 20):
            net.add_node(int(user.id), label=user.name, shape='image', image=user.photo, size=(pair[1]*35)/10,
                         title='sex: {}\n city: {}\n vk: vk.com/id{}\n'.format(user.sex, user.city, user.id))
        if main_user.id != user.id and user.id != 20:
            net.add_edge(int(main_user.id), user.id, color="blue")
        users.append(user)
    print('146282536: {}'.format(i))
    for main_user in users:
        data, total_items = extract_data(main_user.id)
        i = 0
        for pair in data:
            i += 1
            user = get_user_info(pair[0])
            dst = extract_data(main_user.id, user.id)
            info.append((user, dst+pair[1]))
            if user.id != 20:
                net.add_node(int(user.id), label=user.name, shape='image', image=user.photo, size=(100-pair[1])/10,
                             title='sex: {}\n city: {}\n vk: vk.com/id{}\n'.format(user.sex, user.city, user.id))
            if main_user.id != user.id and user.id != 20:
                net.add_edge(int(main_user.id), user.id, color="blue")
    i = 0
    for pair in info:
        i += 1
        print(pair[1])
    net.show('test.html')
    con.commit()
    con.close()


def extract_data(source, user=-1):
    if user == -1:
        with con:
            with con.cursor() as cur:
                cur.execute("SELECT SOURCEID, USERID, NUM_OF_LIKES from LIKES WHERE SOURCEID={}".format(source))
                rows = cur.fetchall()
        selected_data = {}
        total_items = 0
        for row in rows:
            total_items += row[2]
            if row[1] in selected_data.keys():
                selected_data[row[1]] += row[2]
            else:
                selected_data.update([(row[1], row[2])])

        with con:
            with con.cursor() as cur:
                cur.execute("SELECT SOURCEID, USERID, NUM_OF_COMMENTS from comments WHERE SOURCEID={}".format(source))
                rows = cur.fetchall()
        for row in rows:
            total_items += row[2]*2
            if row[1] in selected_data.keys():
                selected_data[row[1]] += row[2] * 2
            else:
                selected_data.update([(row[1], row[2] * 2)])
        list_d1 = list(selected_data.items())
        list_d1.sort(key=lambda i: i[1])
        return list_d1[::-1], total_items
    else:
        distance = 0
        with con:
            with con.cursor() as cur:
                cur.execute("SELECT SOURCEID, USERID, NUM_OF_LIKES from LIKES WHERE sourceid={}".format(user))
                rows = cur.fetchall()
        for row in rows:
            if row[1] == source:
                distance += row[2]
        with con:
            with con.cursor() as cur:
                cur.execute("SELECT SOURCEID, USERID, NUM_OF_COMMENTS from COMMENTS WHERE sourceid={}".format(user))
                rows = cur.fetchall()
        for row in rows:
            if row[1] == source:
                distance += row[2] * 2
        return distance


def get_user_info(user):
    with con:
        with con.cursor() as cur:
            cur.execute("SELECT ID,NAME,PHOTO,SEX,CITY from USERS WHERE ID={}".format(user))
            f = cur.fetchall()
    if len(f) > 0:
        aux_data = f[0]
    else:
        aux_data = (20, 'a', 'https://sun9-19.userapi.com/c852320/v852320607/1650bf/QW8YAHl1S1E.jpg?ava=1', 1, 'a')
    user = User(aux_data[0], aux_data[1].replace(' ', ''), aux_data[2].replace(' ', ''), aux_data[3], aux_data[4].replace(' ', ''))
    return user


main()
