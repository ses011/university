package webServer;

import Adapters.Adapters;
import Adapters.RedisAdapter;
import io.javalin.Javalin;
import Adapters.KafkaAdapter;
import webServer.api.DimensionsHandler;
import webServer.api.IndexHandler;


public class App {

    public static void main(String[] args) {
        var adapter = new Adapters();

        adapter.logger.info("Logger, kafka and redis init, starting server...");

        Javalin app = Javalin.create(config -> {
            config.addSinglePageHandler("/", IndexHandler::handleGet);
        }).start(8080);
        app.get("/two_dimensional", DimensionsHandler::twoDimensionalGet);
        app.get("/three_dimensional", DimensionsHandler::threeDimensionalGet);
        app.get("/n_dimensional", DimensionsHandler::nDimensionalGet);

        app.post("/two_dimensional", ctx -> {DimensionsHandler.twoDimensionalPost(ctx, adapter);});
        app.post("/three_dimensional", ctx -> {DimensionsHandler.threeDimensionalPost(ctx, adapter);});
        app.post("/n_dimensional", ctx -> {DimensionsHandler.nDimensionalPost(ctx, adapter);});
    }

}
