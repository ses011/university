package webServer.api;

import Adapters.Adapters;
import Adapters.RedisAdapter;
import io.javalin.http.Context;
import lombok.SneakyThrows;
import redis.clients.jedis.Jedis;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.concurrent.CountDownLatch;

public class DimensionsHandler {
    public static void twoDimensionalGet(Context ctx) {
        try {
            String text = new String(Files.readAllBytes(Paths.get(
                    "/resources/html/2D.html")),
                    StandardCharsets.UTF_8);
            ctx.html(text);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public static void threeDimensionalGet(Context ctx) {
        try {
            String text = new String(Files.readAllBytes(Paths.get(
                    "/resources/html/3D.html")),
                    StandardCharsets.UTF_8);
            ctx.html(text);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public static void nDimensionalGet(Context ctx) {
        try {
            String text = new String(Files.readAllBytes(Paths.get(
                    "/resources/html/ND.html")),
                    StandardCharsets.UTF_8);
            ctx.html(text);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    private static void saveData(RedisAdapter rAdapter, String uuid, String data) {
        var jedis = new Jedis(rAdapter.getHost(), rAdapter.getPort());
        jedis.set(uuid, data);
    }

    private static String extractData(RedisAdapter rAdapter, String uuid) {
        var jedis = new Jedis(rAdapter.getHost(), rAdapter.getPort());

        var res = jedis.get(uuid);

        jedis.del(uuid);

        return res;
    }

    public static void handleRequest(Context ctx, Adapters adapter, String topic) {
        var uuid = UUID.randomUUID().toString();
        adapter.logger.info("Generate uuid: " + uuid);
        saveData(adapter.redis, uuid, ctx.body());
        adapter.logger.info("Save data to redis");

        var cdl = new CountDownLatch(1);

        adapter.kafka.putMessage(uuid, cdl, topic);
        adapter.logger.info("Put message into kafka, waiting result...");
        try {
            cdl.await();
        } catch (InterruptedException e) {
            adapter.logger.warning(e.getMessage());
        }

        adapter.logger.info("Sending response...");
        ctx.status(201);
        ctx.json(extractData(adapter.redis, uuid));
    }

    public static void twoDimensionalPost(Context ctx, Adapters adapter) {
        handleRequest(ctx, adapter, adapter.kafka.topic2d);
    }

    public static void threeDimensionalPost(Context ctx, Adapters adapter) {
        handleRequest(ctx, adapter, adapter.kafka.topic3d);
    }

    @SneakyThrows
    public static void nDimensionalPost(Context ctx, Adapters adapter) {
        handleRequest(ctx, adapter, adapter.kafka.topicNd);
    }
}
