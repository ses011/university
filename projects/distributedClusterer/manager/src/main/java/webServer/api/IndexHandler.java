package webServer.api;

import io.javalin.http.Context;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

public class IndexHandler  {
    public static void handleGet(Context ctx) {
        try {
            String text = new String(Files.readAllBytes(Paths.get(
                    "/resources/html/index.html")),
                    StandardCharsets.UTF_8);
            ctx.html(text);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}
