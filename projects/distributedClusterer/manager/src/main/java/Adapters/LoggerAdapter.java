package Adapters;

import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

public class LoggerAdapter {
    public static Logger init() {
        Logger logger;
        try {
            logger = Logger.getLogger("manager");
            var fh = new FileHandler("/logs/manager.txt");
            var simpleFr = new SimpleFormatter();
            fh.setFormatter(simpleFr);
            logger.addHandler(fh);
        } catch (IOException e) {
            return Logger.getLogger("manager");
        }

        return logger;
    }
}
