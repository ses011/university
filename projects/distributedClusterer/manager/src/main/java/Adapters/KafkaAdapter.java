package Adapters;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;

import java.time.Duration;
import java.util.Collections;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CountDownLatch;

public class KafkaAdapter {
    private ConcurrentHashMap<String, CountDownLatch> result;
    private KafkaProducer<String, String> producer;
    private KafkaConsumer<String, String> consumer;
    private String bootstrapServers;
    private String rsTopic;
    public String topic2d;
    public String topic3d;
    public String topicNd;
    private String groupId;
    private Thread consumingThread;

    public KafkaAdapter() {
        result = new ConcurrentHashMap<>();
    }

    private void extractEnvVars() {
        groupId = System.getenv("CL_KAFKA_GROUP_ID");
        if (groupId == null) {
            groupId = "cl.workers";
        }

        rsTopic = System.getenv("CL_KAFKA_RS_TOPIC");
        if (rsTopic == null) {
            rsTopic = "cl.worker.rs_topic";
        }

        bootstrapServers = System.getenv("CL_KAFKA_BOOTSTRAP_SERVERS");
        if (bootstrapServers == null) {
            bootstrapServers = "localhost:29092";
        }

        topic2d = System.getenv("CL_KAFKA_2DTOPIC");
        if (topic2d == null) {
            topic2d = "cl.worker.rq_2d";
        }

        topic3d = System.getenv("CL_KAFKA_3DTOPIC");
        if (topic3d == null) {
            topic3d = "cl.worker.rq_3d";
        }

        topicNd = System.getenv("CL_KAFKA_NDTOPIC");
        if (topicNd == null) {
            topicNd = "cl.worker.rq_nd";
        }
    }

    private KafkaProducer<String, String> generateProducer() {
        Properties properties = new Properties();
        properties.setProperty("bootstrap.servers", bootstrapServers);
        properties.setProperty("key.serializer","org.apache.kafka.common.serialization.StringSerializer");
        properties.setProperty("value.serializer","org.apache.kafka.common.serialization.StringSerializer");
        return new KafkaProducer<>(properties);
    }

    private KafkaConsumer<String, String> generateConsumer() {
        Properties properties = new Properties();
        properties.setProperty("bootstrap.servers", bootstrapServers);
        properties.setProperty("group.id", groupId);
        properties.setProperty("key.deserializer","org.apache.kafka.common.serialization.StringDeserializer");
        properties.setProperty("value.deserializer","org.apache.kafka.common.serialization.StringDeserializer");
        properties.setProperty("auto.offset.reset","latest");
        return new KafkaConsumer<>(properties);
    }

    public void putMessage(String uuid, CountDownLatch cdl, String rqTopic) {
        result.put(uuid, cdl);
        try {
            producer.send(new ProducerRecord<>(rqTopic, uuid));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void init() {
        extractEnvVars();

        consumer = generateConsumer();
        consumer.subscribe(Collections.singleton(rsTopic));
        producer = generateProducer();

        consumingThread = new Thread(() -> {
            while (true) {
                ConsumerRecords<String, String> records = consumer.poll(Duration.ofMillis(1000));
                if (records.count() == 0) {
                    continue;
                }
                for (ConsumerRecord<String, String> record : records) {
                    var uuid = record.value();
                    result.get(uuid).countDown();
                    result.remove(uuid);
                }
            }
        });

        consumingThread.start();

    }

}

