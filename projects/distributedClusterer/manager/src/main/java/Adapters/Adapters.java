package Adapters;

import java.util.logging.Logger;

public class Adapters {
    public KafkaAdapter kafka;
    public Logger logger;
    public RedisAdapter redis;

    public Adapters() {
        kafka = new KafkaAdapter();
        kafka.init();

        logger = LoggerAdapter.init();

        redis = new RedisAdapter();
    }
}
