package Adapters;

public class RedisAdapter {
    private String host;
    private String port;

    public RedisAdapter() {
        host = System.getenv("REDIS_HOST");
        if (host == null) {
            host = "localhost";
        }

        port = System.getenv("REDIS_PORT");
        if (port == null) {
            port = "6379";
        }
    }

    public String getHost() {
        return host;
    }

    public Integer getPort() {
        return Integer.parseInt(port);
    }
}
