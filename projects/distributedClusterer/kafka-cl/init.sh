docker run --rm --entrypoint="" bitnami/kafka:2.8.1  /opt/bitnami/kafka/bin/kafka-topics.sh \
  --create --bootstrap-server 192.168.0.104:29092 --topic cl.worker.rq_2d --partitions 1 --replication-factor 1
docker run --rm --entrypoint="" bitnami/kafka:2.8.1 /opt/bitnami/kafka/bin/kafka-topics.sh \
  --create --bootstrap-server 192.168.0.104:29092 --topic cl.worker.rq_3d --partitions 1 --replication-factor 1
docker run --rm --entrypoint="" bitnami/kafka:2.8.1 /opt/bitnami/kafka/bin/kafka-topics.sh \
  --create --bootstrap-server 192.168.0.104:29092 --topic cl.worker.rq_nd --partitions 1 --replication-factor 1
docker run --rm --entrypoint="" bitnami/kafka:2.8.1 /opt/bitnami/kafka/bin/kafka-topics.sh \
  --create --bootstrap-server 192.168.0.104:29092 --topic cl.worker.rs_topic --partitions 1 --replication-factor 1
