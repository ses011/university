package utils;

import java.util.ArrayList;

public class TwoDRequest {
    public Integer clusterAmount;
    public ArrayList<TwoDPoint> points;
    public String timeElapsed;

    public TwoDRequest() {
        points = new ArrayList<>();
    }

    public ArrayList<ArrayList<Double>> getPoints() {
        var res = new ArrayList<ArrayList<Double>>();
        for (var p: points) {
            var tmp = new ArrayList<Double>();
            tmp.add(p.x);
            tmp.add(p.y);
            res.add(tmp);
        }

        return res;
    }
}
