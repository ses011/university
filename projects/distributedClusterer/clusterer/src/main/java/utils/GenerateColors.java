package utils;

import java.util.HashSet;
import java.util.Random;

public class GenerateColors {
    private HashSet<String> alreadyUsed;

    public GenerateColors() {
        alreadyUsed = new HashSet<>();
        alreadyUsed.add("#000000");
    }

    public String generateRandomColor() {
        var rand = new Random();

        StringBuilder color = new StringBuilder(getBlackColor());

        while (alreadyUsed.contains(color.toString())) {
            color = new StringBuilder("#");
            String[] letters = {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "A", "B", "C", "D", "E", "F"};
            for (var i = 0; i < 6; i++) {
                color.append(letters[rand.nextInt(16)]);
            }
        }

        alreadyUsed.add(color.toString());

        return color.toString();
    }

    public String getBlackColor() {
        return "#000000";
    }
}
