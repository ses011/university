package utils;

import java.util.ArrayList;

public class ThreeDRequest {
    public Integer clusterAmount;
    public ArrayList<ThreeDPoint> points;
    public String timeElapsed;

    public ThreeDRequest() {
        points = new ArrayList<>();
    }

    public ArrayList<ArrayList<Double>> getPoints() {
        var res = new ArrayList<ArrayList<Double>>();
        for (var p: points) {
            var tmp = new ArrayList<Double>();
            tmp.add(p.x);
            tmp.add(p.y);
            tmp.add(p.z);
            res.add(tmp);
        }

        return res;
    }
}
