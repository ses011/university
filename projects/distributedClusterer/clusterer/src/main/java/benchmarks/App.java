package benchmarks;

import org.openjdk.jmh.annotations.*;

import java.util.concurrent.TimeUnit;

public class App {
    @Benchmark
    @Fork(value = 1, warmups = 2)
    @Warmup(iterations = 2, time = 5, timeUnit = TimeUnit.SECONDS)
    @Measurement(iterations = 5, time = 5)
    @OutputTimeUnit(TimeUnit.MILLISECONDS)
    @BenchmarkMode(Mode.SingleShotTime)
    public void benchSplitNonParallel(ExecutionPlan plan) {
        plan.ps.splitOnClusters();
    }

    @Benchmark
    @Fork(value = 1, warmups = 2)
    @Warmup(iterations = 2, time = 5, timeUnit = TimeUnit.SECONDS)
    @Measurement(iterations = 5, time = 5)
    @OutputTimeUnit(TimeUnit.MILLISECONDS)
    @BenchmarkMode(Mode.SingleShotTime)
    public void benchSplitParallel(ExecutionPlanParallel plan) {
        plan.ps.splitOnClusters();
    }

    public static void main(String[] args) throws Exception {
        org.openjdk.jmh.Main.main(args);
    }
}
