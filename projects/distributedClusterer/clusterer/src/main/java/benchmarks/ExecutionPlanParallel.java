package benchmarks;

import clusterer.ParallelPointSystem;
import clusterer.PointSystem;
import org.openjdk.jmh.annotations.*;

@State(Scope.Benchmark)
public class ExecutionPlanParallel {
    public ParallelPointSystem ps;

    @Param({"2", "3", "7", "10", "22"})
    public int dimension;

    @Param({"100", "1000", "10000", "1000000"})
    public int amount;

    @Param({"2", "7", "9", "25", "95"})
    public int clusters;

    @Param({"4", "5", "6", "7", "8"})
    public int threads;

    @Setup(Level.Invocation)
    public void setUp() {
        ps = new ParallelPointSystem(dimension, clusters, threads);
        ps.generatePoints(amount);
    }

    @TearDown(Level.Invocation)
    public void tearDown() {
        ps.shutdown();
    }
}
