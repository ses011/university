package benchmarks;

import clusterer.PointSystem;
import org.openjdk.jmh.annotations.*;

@State(Scope.Benchmark)
public class ExecutionPlan {
    public PointSystem ps;

    @Param({"2", "3", "7", "10", "22"})
    public int dimension;

    @Param({"100", "1000", "10000", "1000000"})
    public int amount;

    @Param({"2", "7", "9", "25", "95"})
    public int clusters;

    @Setup(Level.Invocation)
    public void setUp() {
        ps = new PointSystem(dimension, clusters);
        ps.generatePoints(amount);
    }
}
