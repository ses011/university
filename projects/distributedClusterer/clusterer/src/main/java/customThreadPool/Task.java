package customThreadPool;

import java.util.Comparator;

public class Task {
    private final Runnable r;
    private final long delay;
    private final int type;
    private long nextSubmit;

    public Task(Runnable _r, long _delay, int _type) {
        this.delay = _delay;
        this.r = _r;
        this.type = _type;
        this.nextSubmit = System.currentTimeMillis() + this.delay;
    }

    public void resetNextSubmit() {
        this.nextSubmit = System.currentTimeMillis() + this.delay;
    }

    public long getDelay() {
        return this.delay;
    }

    public long getNextSubmitTime() {
        return this.nextSubmit;
    }

    public int getType() {
        return this.type;
    }

    public Runnable getRunnable() {
        return this.r;
    }
}

class TaskCompare implements Comparator<Task> {
    public int compare(Task t1, Task t2) {
        if (t1.getNextSubmitTime() < t2.getNextSubmitTime()) {
            return -1;
        } else if (t1.getNextSubmitTime() > t2.getNextSubmitTime()) {
            return 1;
        }
        return 0;
    }
}
