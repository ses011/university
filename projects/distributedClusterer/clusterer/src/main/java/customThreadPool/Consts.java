package customThreadPool;

public class Consts {
    // задача обычного типа
    public static final int COMMON   = 0;
    // задача с задержкой
    public static final int DELAYED  = 1;
    // периодическая задача
    public static final int PERIODIC = 2;
    // интервальная задача
    public static final int INTERVAL = 3;
}
