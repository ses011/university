package customThreadPool;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.atomic.AtomicBoolean;

class Worker extends Thread {
    private final BlockingQueue<Task> workerQueue;
    private final PriorityBlockingQueue<Task> schedule;
    private final AtomicBoolean isAlive;

    public Worker(String name, BlockingQueue<Task> queue, PriorityBlockingQueue<Task> schedule) {
        super(name);
        this.workerQueue = queue;
        this.schedule = schedule;
        this.isAlive = new AtomicBoolean(true);
    }

    public void shutdown() {
        this.isAlive.set(false);
    }

    public void run() {
        while (isAlive.get()) {
            Task t = null;

            try {
                t = workerQueue.take();
            } catch (InterruptedException e) {
                // произошло прерывание во время ожидания задачи в очереди
                continue;
            }

            t.getRunnable().run();
            if (t.getType() == Consts.INTERVAL) {
                // если задача определенного типа, добавить ее в очередь на ожидание
                t.resetNextSubmit();
                schedule.add(t);
            }
        }
    }
}
