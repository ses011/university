package customThreadPool;

import java.util.ArrayList;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.PriorityBlockingQueue;

public class CustomThreadPool {
    // отсортированный массив задач с задержкой
    private final PriorityBlockingQueue<Task> schedule;
    // поток для ожидания задаржек
    private final Planner planner;
    // очередь задач
    private final BlockingQueue<Task> workerQueue;
    // используемые потоки
    private final ArrayList<Worker> threads;

    public CustomThreadPool(int threadsAmount) {
        schedule = new PriorityBlockingQueue<>(10, new TaskCompare());
        workerQueue = new LinkedBlockingQueue<>();
        planner = new Planner("Planner", workerQueue, schedule);
        planner.start();
        threads = new ArrayList<>();
        for (int i = 0; i < threadsAmount; i++) {
            var t = new Worker("Worker_" + i, workerQueue, schedule);
            t.start();
            threads.add(t);
        }
    }

    public int getAmountOfTasksInQueue() {
        return this.schedule.size();
    }

    // добавить в очередь на исполнение задачу
    public void submitCommon(Runnable r) throws InterruptedException {
        workerQueue.put(new Task(r, 0, Consts.COMMON));
    }

    // добавить в очередь на ожидание задачу
    public void submitDelayed(Runnable r, long delay) {
        schedule.add(new Task(r, delay, Consts.DELAYED));
    }

    // добавить в очередь на исполнение задачу
    // после добавить в очередь на ожидание задачу
    public void submitPeriodic(Runnable r, long period) throws InterruptedException {
        Task t = new Task(r, period, Consts.PERIODIC);
        schedule.add(t);
        workerQueue.put(t);
    }

    // добавить в очередь на исполнение задачу
    // в очередь на ожидание она добавится после завершения
    public void submitInterval(Runnable r, int interval) throws InterruptedException {
        workerQueue.put(new Task(r, interval, Consts.INTERVAL));
    }

    public void shutdown() {
        planner.shutdown();
        schedule.clear();
        for (var t: threads) {
            t.shutdown();
        }
    }
}
