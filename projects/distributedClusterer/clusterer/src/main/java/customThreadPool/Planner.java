package customThreadPool;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.atomic.AtomicBoolean;

class Planner extends Thread {
    private PriorityBlockingQueue<Task> schedule;
    private BlockingQueue<Task> workerQueue;
    private AtomicBoolean isAlive;

    public Planner(String name, BlockingQueue<Task> queue, PriorityBlockingQueue<Task> schedule) {
        super(name);
        this.schedule = schedule;
        this.workerQueue = queue;
        this.isAlive = new AtomicBoolean(true);
    }

    public void shutdown() {
        this.isAlive.set(false);
    }

    public void run() {
        while (isAlive.get()) {
            Thread.onSpinWait();

            if (schedule.size() == 0) {
                Thread.onSpinWait();
            } else if (System.currentTimeMillis() >= schedule.peek().getNextSubmitTime()) {
                while(System.currentTimeMillis() >= schedule.peek().getNextSubmitTime()) {
                    Task t = schedule.remove();

                    try {
                        workerQueue.put(t);
                    } catch (InterruptedException ex) {
                        // задачу не удалось поместить в очередь на выполнение из-за прерывания
                        // надо попробовать ее добавить еще раз, на следующей итерации
                        break;
                    }

                    if (t.getType() == Consts.PERIODIC) {
                        t.resetNextSubmit();
                        schedule.add(t);
                    }
                    if (schedule.size() == 0) {
                        break;
                    }
                }
            }
        }
    }
}
