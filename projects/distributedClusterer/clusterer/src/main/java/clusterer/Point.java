package clusterer;

import lombok.SneakyThrows;

import java.util.ArrayList;
import java.util.Objects;
import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;

import static java.lang.Math.pow;
import static java.lang.Math.sqrt;

public class Point {
    private Integer id;
    private ArrayList<Double> coords;
    private final AtomicInteger maxC = new AtomicInteger(1000);

    public Point(Integer _id, ArrayList<Double> _coords) {
        id = _id;
        coords = new ArrayList<>(_coords);
    }

    public Point() {
        id = 0;
        coords = new ArrayList<>();
    }

    public int getDimension() {
        return coords.size();
    }

    public int getId() {
        return id;
    }

    @SneakyThrows
    public Double getComponent(int index) {
        if (index >= coords.size()) {
            throw new IndexOutOfBoundsException("Index >= dimension size");
        }
        return coords.get(index);
    }

    @SneakyThrows
    public double distanceTo(Point p) {
        if (p.getDimension() != coords.size() || coords.size() == 0) {
            throw new IndexOutOfBoundsException("Points should have same dimension");
        }

        double res = 0;

        for (var i = 0; i < coords.size(); i++) {
            res += pow(coords.get(i)-p.getComponent(i), 2);
        }

        return sqrt(res);
    }

    public ArrayList<Double> getAllComponents() {
        return coords;
    }

    public boolean equalsTo(Point p) {
        if (p.getDimension() != coords.size()) {
            return false;
        }

        for (var i = 0; i < p.getDimension(); i++) {
            if (!Objects.equals(coords.get(i), p.getComponent(i))) {
                return false;
            }
        }

        return true;
    }

    public String toString() {
        StringBuilder res = new StringBuilder();
        for (var c: coords) {
            res.append(c.toString()).append(" ");
        }
        return res.toString();
    }

    public void fillRandom(Integer dimension) {
        coords = new ArrayList<>();
        var rand = new Random();
        for (var i = 0; i < dimension; i++) {
            coords.add((double) rand.nextInt(2*maxC.get()) - maxC.get());
        }
    }
}
