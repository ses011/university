package clusterer;

import lombok.SneakyThrows;

import java.util.ArrayList;
import java.util.concurrent.ConcurrentHashMap;

public class Cluster {
    private ConcurrentHashMap<Integer, Point> points;
    private Point currentCenter;
    private Point prevCenter;
    private Integer dimension;
    private final double precision = 0.001;

    public Cluster(Integer _dimension) {
        dimension = _dimension;
        points = new ConcurrentHashMap<>();
        currentCenter = generateInitialCenter(dimension);
        prevCenter = generateInitialCenter(dimension);
    }

    public Cluster() {}

    private Point generateInitialCenter(Integer n) {
        var coords = new ArrayList<Double>();
        for (var i = 0; i < n; i++) {
            coords.add(0.0);
        }

        return new Point(0, coords);
    }

    public String toString() {
        return currentCenter.toString();
    }

    @SneakyThrows
    public void attach(Point p) {
        if (p.getDimension() != dimension) {
            throw new Exception("You can put only points with same dimension in one cluster");
        }
        points.put(p.getId(), p);
    }

    public void clear() {
        points = new ConcurrentHashMap<>();
    }

    @SneakyThrows
    public void setCenter(Point newCenter) {
        if (newCenter.getDimension() != dimension) {
            throw new Exception("You can put only points with same dimension in one cluster");
        }
        prevCenter = currentCenter;
        currentCenter = newCenter;
    }

    public ArrayList<Point> getAttachedPoints() {
        return new ArrayList<>(points.values());
    }

    public Point getCurrentCenter() {
        return currentCenter;
    }

    public boolean prevCenterEqToCur() {
        return currentCenter.distanceTo(prevCenter) < precision;
    }

    @SneakyThrows
    public void moveCenter() {
        if (points.size() == 0) {
            this.setCenter(currentCenter);
            return;
        }

        ArrayList<Double> masses = new ArrayList<>();
        for (var i = 0; i < dimension; i++) {
            masses.add(0.0);
        }

        for (var p: points.values()) {
            for (var i = 0; i < dimension; i++) {
                var tmp = masses.get(i) + p.getComponent(i);
                masses.set(i, tmp);
            }
        }

        for (var i = 0; i < dimension; i++) {
            var tmp = masses.get(i);
            masses.set(i, tmp/points.size());
        }

        this.setCenter(new Point(0, masses));
    }
}
