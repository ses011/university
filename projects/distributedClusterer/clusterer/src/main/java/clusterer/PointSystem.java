package clusterer;

import com.google.common.util.concurrent.AtomicDoubleArray;
import lombok.SneakyThrows;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;


public class PointSystem {
    private ArrayList<Cluster> clusters;
    private ArrayList<Point> points;
    private Integer dimension;
    private Integer clustersAmount;
    private final Integer iterations = 5000;

    @SneakyThrows
    public PointSystem(Integer _dimension, Integer _clustersAmount) {
        if (_dimension <= 0) {
            throw new Exception("Dimension should be > 0");
        }
        this.dimension = _dimension;

        if (_clustersAmount <= 0) {
            throw new Exception("Amount of clusters should be > 0");
        }
        this.clustersAmount = _clustersAmount;

        this.clusters = new ArrayList<>();
        this.points = new ArrayList<>();
    }

    @SneakyThrows
    public void initPoints(ArrayList<ArrayList<Double>> inputData) {
        int cId = 0;
        for (var coords: inputData) {
            if (coords.size() != dimension) {
                throw new Exception("Met point with unknown dimension");
            }

            points.add(new Point(cId++, coords));
        }
    }

    @SneakyThrows
    public void generatePoints(Integer amount) {
        if (amount <= 0) {
            throw new Exception("Amount of points to generate should be >= 0");
        }

        points = new ArrayList<>();

        for (var i = 0; i < amount; i++) {
            var tmp = new Point();
            tmp.fillRandom(dimension);
            points.add(tmp);
        }
    }

    @SneakyThrows
    private void initClusters() {
        var rand = new Random();

        for (int i = 0; i < clustersAmount; i ++) {
            var nCluster = new Cluster(dimension);
            nCluster.setCenter(points.get(rand.nextInt(points.size())));
            clusters.add(nCluster);
        }

        System.out.println(clusters.size());
    }

    private int findMinDistance(AtomicDoubleArray distances) {
        var minEl = distances.get(0);
        var ind = 0;

        for (var i = 1; i < clustersAmount; i++) {
            if (minEl > distances.get(i)) {
                minEl = distances.get(i);
                ind = i;
            }
        }

        return ind;
    }

    private void clearClusters() {
        for (var c: clusters) {
            c.clear();
        }
    }

    private boolean clustersCentresArentMoving() {
        for (var c: clusters) {
            if (! c.prevCenterEqToCur()) {
                return false;
            }
        }

        return true;
    }

    public HashMap<String, ArrayList<ArrayList<Double>>> getClustersData() {
        var res = new HashMap<String, ArrayList<ArrayList<Double>>>();

        for (var i = 0; i < clustersAmount; i++) {
            var attachedPoints = clusters.get(i).getAttachedPoints();
            var r = new ArrayList<ArrayList<Double>>();
            for (var p: attachedPoints) {
                r.add(p.getAllComponents());
            }

            res.put(String.valueOf(i), r);
        }

        var tmp = new ArrayList<ArrayList<Double>>();
        for (var c: clusters) {
            tmp.add(c.getCurrentCenter().getAllComponents());
        }

        res.put("centers", tmp);

        return res;
    }

    @SneakyThrows
    public Map<String, ArrayList<ArrayList<Double>>> splitOnClusters() {
        if (points.size() == 0) {
            throw new Exception("No points to cluster");
        }

        initClusters();

        for (var i = 0; i < iterations; i++) {
            clearClusters();
            for (var p: points) {
                var distances = new AtomicDoubleArray(clustersAmount);
                for (var j = 0; j < clustersAmount; j++) {
                    var dst = p.distanceTo(clusters.get(j).getCurrentCenter());
                    distances.set(j, dst);
                }

                var minInd = findMinDistance(distances);
                clusters.get(minInd).attach(p);
            }

            for (var c: clusters) {
                c.moveCenter();
            }

            if (clustersCentresArentMoving()) {
                break;
            }
        }

        return getClustersData();
    }

}
