package clusterer;

import customThreadPool.CustomThreadPool;
import com.google.common.util.concurrent.AtomicDoubleArray;
import lombok.SneakyThrows;
import redis.clients.jedis.Jedis;

import java.util.ArrayList;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.*;

public class ParallelPointSystem {
    private CopyOnWriteArrayList<Cluster> clusters;
    private CopyOnWriteArrayList<Point> points;
    private Integer dimension;
    private Integer clustersAmount;
    private final Integer iterations = 5000;
    private final Integer batchSize = 1000;
    private CustomThreadPool service;

    @SneakyThrows
    public ParallelPointSystem(Integer _dimension, Integer _clustersAmount) {
        if (_dimension <= 0) {
            throw new Exception("Dimension should be > 0");
        }
        this.dimension = _dimension;

        if (_clustersAmount <= 0) {
            throw new Exception("Amount of clusters should be > 0");
        }
        this.clustersAmount = _clustersAmount;

        this.clusters = new CopyOnWriteArrayList<>();
        this.points = new CopyOnWriteArrayList<>();

        service = new CustomThreadPool(6);
    }

    @SneakyThrows
    public ParallelPointSystem(Integer _dimension, Integer _clustersAmount, Integer _threads) {
        if (_dimension <= 0) {
            throw new Exception("Dimension should be > 0");
        }
        dimension = _dimension;

        if (_clustersAmount <= 0) {
            throw new Exception("Amount of clusters should be > 0");
        }
        clustersAmount = _clustersAmount;

        if (_threads <= 0) {
            throw new Exception("Amount of threads should be > 0");
        }

        clusters = new CopyOnWriteArrayList<>();
        points = new CopyOnWriteArrayList<>();

        service = new CustomThreadPool(_threads);
    }

    @SneakyThrows
    public void initPoints(ArrayList<ArrayList<Double>> inputData) {
        int cId = 0;
        for (var coords: inputData) {
            if (coords.size() != dimension) {
                throw new Exception("Met point with unknown dimension");
            }

            points.add(new Point(cId++, coords));
        }
    }

    @SneakyThrows
    public void generatePoints(Integer amount) {
        if (amount <= 0) {
            throw new Exception("Amount of points to generate should be >= 0");
        }

        var auxPoints = new Point[amount];
        var iters = amount / batchSize;
        CountDownLatch cdl;

        if (amount < batchSize) {
            cdl = new CountDownLatch(0);
        } else {
            cdl = new CountDownLatch(iters);
            for (var i = 0; i < iters; i++) {
                int finalI = i;

                service.submitCommon(() -> {
                    for (var j = 0; j < batchSize; j++) {
                        var tmp = new Point();
                        tmp.fillRandom(dimension);
                        auxPoints[finalI * batchSize + j] = tmp;
                    }
                    cdl.countDown();
                });
            }
        }

        for (var i = 0; i < amount % batchSize; i++) {
            var tmp = new Point();
            tmp.fillRandom(dimension);
            auxPoints[iters * batchSize + i] = tmp;
        }

        cdl.await();

        points = new CopyOnWriteArrayList<>(auxPoints);
    }

    @SneakyThrows
    private void initClusters() {
        clusters = new CopyOnWriteArrayList<>();

        CountDownLatch cdl = new CountDownLatch(clustersAmount);

        for (var i = 0; i < clustersAmount; i++) {
            var nCluster = new Cluster(dimension);
            clusters.add(nCluster);

            service.submitCommon(() -> {
                var rand = new Random();
                nCluster.setCenter(points.get(rand.nextInt(points.size())));
                cdl.countDown();
            });
        }

        cdl.await();
    }

    private int findMinDistance(AtomicDoubleArray distances) {
        var minEl = distances.get(0);
        var ind = 0;

        for (var i = 1; i < clustersAmount; i++) {
            if (minEl > distances.get(i)) {
                minEl = distances.get(i);
                ind = i;
            }
        }

        return ind;
    }

    @SneakyThrows
    // should rework
    private void clearClusters() {
        var cdl = new CountDownLatch(clustersAmount);

        for (var c: clusters) {
            service.submitCommon( () -> {
                c.clear();
                cdl.countDown();
            });
        }

        cdl.await();
    }

    private boolean clustersCentresArentMoving() {
        for (var c: clusters) {
            if (! c.prevCenterEqToCur()) {
                return false;
            }
        }

        return true;
    }

    @SneakyThrows
    public ConcurrentHashMap<String, ArrayList<ArrayList<Double>>> getClustersData() {
        var res = new ConcurrentHashMap<String, ArrayList<ArrayList<Double>>>();

        for (var i = 0; i < clustersAmount; i++) {
            ArrayList<Point> attachedPoints;
            attachedPoints = clusters.get(i).getAttachedPoints();
            var r = new ArrayList<ArrayList<Double>>();
            for (var p : attachedPoints) {
                r.add(p.getAllComponents());
            }
            res.put(String.valueOf(i), r);
        }

        var tmp = new ArrayList<ArrayList<Double>>();
        for (var c: clusters) {
            tmp.add(c.getCurrentCenter().getAllComponents());
        }

        res.put("centers", tmp);

        return res;
    }

    @SneakyThrows
    private void attachPoint(int index) {
        var distances = new AtomicDoubleArray(clustersAmount);
        var p = points.get(index);

        for (var m = 0; m < clustersAmount; m++) {
            var dst = p.distanceTo(clusters.get(m).getCurrentCenter());
            distances.set(m, dst);
        }

        var minInd = findMinDistance(distances);

        clusters.get(minInd).attach(p);
    }

    @SneakyThrows
    public Map<String, ArrayList<ArrayList<Double>>> splitOnClusters() {
        if (points.size() == 0) {
            throw new Exception("No points to cluster");
        }

        initClusters();
        var size = points.size();

        for (var i = 0; i < iterations; i++) {
            clearClusters();

            CountDownLatch cdl = new CountDownLatch(size);

            for (var j = 0; j < size; j++) {
                int finalJ = j;

                service.submitCommon(() -> {
                    attachPoint(finalJ);
                    cdl.countDown();
                });
            }

            cdl.await();

            var cldForMove = new CountDownLatch(clustersAmount);

            for (var c: clusters) {
                service.submitCommon(() -> {
                    c.moveCenter();
                    cldForMove.countDown();
                });
            }

            cldForMove.await();

            if (clustersCentresArentMoving()) {
                break;
            }
        }

        return getClustersData();
    }

    public void shutdown() {
        service.shutdown();
    }

}

