package testClusterer;

import clusterer.PointSystem;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;

public class TestPointSystem {
    @Test
    public void testConstructorNegClusterAmount() {
        Assert.assertThrows(Exception.class, () -> {
            var s = new PointSystem(10, -10);
        });
    }

    @Test
    public void testConstructorZeroClusterAmount() {
        Assert.assertThrows(Exception.class, () -> {
            var s = new PointSystem(10, 0);
        });
    }

    @Test
    public void testConstructorNegDim() {
        Assert.assertThrows(Exception.class, () -> {
            var s = new PointSystem(-10, 10);
        });
    }

    @Test
    public void testConstructorZeroDim() {
        Assert.assertThrows(Exception.class, () -> {
            var s = new PointSystem(0, 10);
        });
    }

    private ArrayList<ArrayList<Double>> generatePointArray(Integer amount, Integer dimension) {
        var res = new ArrayList<ArrayList<Double>>();

        for (var i = 0; i < amount; i++) {
            var coords = new ArrayList<Double>();
            for (var j = 0; j < dimension; j++) {
                coords.add((double)j);
            }
            res.add(coords);
        }

        return res;
    }

    @Test
    public void testInitPoint() {
        var ps = generatePointArray(3, 3);
        var noThrow = true;
        PointSystem s;

        try {
            s = new PointSystem(3, 10);
            s.initPoints(ps);
        } catch (Exception ex) {
            noThrow = false;
        }

        Assert.assertTrue(noThrow);
    }

    @Test
    public void testInitPointsDiffDim() {
        var ps = generatePointArray(3, 3);
        ps.get(0).add(9.9);
        var noThrow = true;
        PointSystem s;

        try {
            s = new PointSystem(3, 10);
            s.initPoints(ps);
        } catch (Exception ex) {
            noThrow = false;
        }

        Assert.assertFalse(noThrow);
    }

    @Test
    public void testSplitOnClustersEmpty() {
        var noThrow = true;
        PointSystem s;

        try {
            s = new PointSystem(10, 10);
            var r = s.splitOnClusters();
        } catch (Exception ex) {
            noThrow = false;
        }

        Assert.assertFalse(noThrow);
    }
}
