package testClusterer;

import clusterer.Cluster;
import clusterer.Point;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;

public class TestCluster {
    private Point zeroPoint(Integer n) {
        var coords = new ArrayList<Double>();
        for (var i = 0; i < n; i++) {
            coords.add(0.0);
        }

        return new Point(0, coords);
    }

    @Test
    public void testConstructor() {
        var c = new Cluster(4);
        var zero = zeroPoint(4);

        Assert.assertEquals(0, c.getAttachedPoints().size());
        Assert.assertTrue(c.getCurrentCenter().distanceTo(zero) < 0.00001);
    }

    @Test
    public void testAttach1() {
        var c1 = new Cluster(2);
        var coords = new ArrayList<Double>();
        coords.add(0.0);
        coords.add(1.2);
        var p1 = new Point(12, coords);

        var noThrow = true;

        try {
            c1.attach(p1);
        } catch (Exception ex) {
            noThrow = false;
        }

        Assert.assertTrue(noThrow);
        Assert.assertEquals(1, c1.getAttachedPoints().size());
        for (var i = 0; i < 2; i++) {
            Assert.assertEquals(p1.getComponent(i), c1.getAttachedPoints().get(0).getComponent(i));
        }
    }

    @Test
    public void testAttachIncorrect() {
        var c1 = new Cluster(2);
        var coords = new ArrayList<Double>();
        coords.add(0.0);
        coords.add(1.2);
        coords.add(6.6);
        var p1 = new Point(12, coords);

        var noThrow = true;

        try {
            c1.attach(p1);
        } catch (Exception ex) {
            noThrow = false;
        }

        Assert.assertFalse(noThrow);
        Assert.assertEquals(0, c1.getAttachedPoints().size());
    }

    @Test
    public void testSetCenter() {
        var c = new Cluster(2);
        var zero = zeroPoint(2);
        var noThrow = true;

        try {
            c.setCenter(zero);
        } catch (Exception ex) {
            noThrow = false;
        }

        Assert.assertTrue(noThrow);
        Assert.assertTrue(c.getCurrentCenter().equalsTo(zero));
    }

    @Test
    public void testSetCenterIncorrect() {
        var c = new Cluster(2);
        var zero = zeroPoint(4);
        var noThrow = true;

        try {
            c.setCenter(zero);
        } catch (Exception ex) {
            noThrow = false;
        }

        Assert.assertFalse(noThrow);
    }

    @Test
    public void testGetCenter() {
        var c = new Cluster(4);
        var zero = zeroPoint(4);

        Assert.assertTrue(zero.equalsTo(c.getCurrentCenter()));
    }

    @Test
    public void testSetGetCenter() {
        var c = new Cluster(2);
        var coords = new ArrayList<Double>();
        coords.add(1.0);
        coords.add(3.0);
        var p = new Point(123, coords);
        var noThrow = true;

        try {
            c.setCenter(p);
        } catch (Exception ex) {
            noThrow = false;
        }

        Assert.assertTrue(noThrow);
        Assert.assertTrue(p.equalsTo(c.getCurrentCenter()));
    }

    @Test
    public void testPrevCenterEq() {
        var c = new Cluster(2);
        var coords = new ArrayList<Double>();
        coords.add(1.0);
        coords.add(3.0);
        var p = new Point(123, coords);
        var noThrow = true;

        try {
            c.setCenter(p);
        } catch (Exception ex) {
            noThrow = false;
        }

        try {
            c.setCenter(p);
        } catch (Exception ex) {
            noThrow = false;
        }

        Assert.assertTrue(noThrow);
        Assert.assertTrue(c.prevCenterEqToCur());
    }

    @Test
    public void testPrevCenterUnEq() {
        var c = new Cluster(2);

        var coords1 = new ArrayList<Double>();
        coords1.add(1.0);
        coords1.add(3.0);
        var p1 = new Point(123, coords1);

        var coords2 = new ArrayList<Double>();
        coords2.add(1.0);
        coords2.add(12.0);
        var p2 = new Point(123, coords2);

        var noThrow = true;

        try {
            c.setCenter(p1);
        } catch (Exception ex) {
            noThrow = false;
        }

        try {
            c.setCenter(p2);
        } catch (Exception ex) {
            noThrow = false;
        }

        Assert.assertTrue(noThrow);
        Assert.assertFalse(c.prevCenterEqToCur());
    }

    @Test
    public void testMoveCenterEmpty() {
        var c = new Cluster(2);

        var coords1 = new ArrayList<Double>();
        coords1.add(1.0);
        coords1.add(3.0);
        var p1 = new Point(123, coords1);

        var noThrow = true;

        try {
            c.setCenter(p1);
            c.moveCenter();
        } catch (Exception ex) {
            noThrow = false;
        }

        Assert.assertTrue(noThrow);
        Assert.assertTrue(c.prevCenterEqToCur());
    }

    @Test
    public void testMoveCenter() {
        var c = new Cluster(2);

        var coords1 = new ArrayList<Double>();
        coords1.add(1.0);
        coords1.add(3.0);
        var p1 = new Point(123, coords1);

        var coords2 = new ArrayList<Double>();
        coords2.add(0.0);
        coords2.add(0.0);
        var p2 = new Point(123, coords2);

        var noThrow = true;

        try {
            c.setCenter(p1);
            c.attach(p2);
            c.moveCenter();
        } catch (Exception ex) {
            noThrow = false;
        }

        Assert.assertTrue(noThrow);
        Assert.assertTrue(c.getCurrentCenter().equalsTo(zeroPoint(2)));
    }
}
