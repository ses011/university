package testClusterer;

import clusterer.Point;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;

public class TestPoint {
    private ArrayList<Double> generateCoords(Integer amount) {
        var coords = new ArrayList<Double>();

        for (var i = 0; i < amount; i++) {
            coords.add((double) i);
        }

        return coords;
    }

    @Test
    public void testGetDimension() {
        var coords = generateCoords(3);
        var p = new Point(1, coords);

        Assert.assertEquals(3, p.getDimension());
    }

    @Test
    public void testGetComponentCorrect() {
        var coords = generateCoords(3);
        var p = new Point(1, coords);

        Assert.assertEquals(coords.get(1), p.getComponent(1));
        Assert.assertEquals(coords.get(2), p.getComponent(2));
    }

    @Test
    public void testGetComponentIncorrect() {
        var coords = generateCoords(3);
        var p = new Point(1, coords);

        Assert.assertThrows("Index >= dimension size", IndexOutOfBoundsException.class,() -> {
            p.getComponent(123);
        });
    }

    @Test
    public void testDistanceToCorrect() {
        var c1 = new ArrayList<Double>();
        c1.add(0.0);
        c1.add(3.0);
        var c2 = new ArrayList<Double>();
        c2.add(4.0);
        c2.add(0.0);

        var p1 = new Point(1, c1);
        var p2 = new Point(2, c2);

        Assert.assertTrue(Math.abs(5.0 - p1.distanceTo(p2)) < 0.000001);
        Assert.assertTrue(Math.abs(5.0 - p2.distanceTo(p1)) < 0.000001);
    }

    @Test
    public void testDistanceToOnePoint() {
        var c1 = new ArrayList<Double>();
        c1.add(0.0);
        c1.add(3.0);

        var p1 = new Point(1, c1);

        Assert.assertTrue(Math.abs(0.0 - p1.distanceTo(p1)) < 0.000001);
    }

    @Test
    public void testDistanceToIncorrect() {
        var c1 = new ArrayList<Double>();
        c1.add(0.0);
        c1.add(3.0);
        c1.add(3.0);
        var c2 = new ArrayList<Double>();
        c2.add(4.0);
        c2.add(0.0);

        var p1 = new Point(1, c1);
        var p2 = new Point(2, c2);

        Assert.assertThrows("Points should have same dimension", IndexOutOfBoundsException.class,() -> {
            p1.distanceTo(p2);
        });
        Assert.assertThrows("Points should have same dimension", IndexOutOfBoundsException.class,() -> {
            p2.distanceTo(p1);
        });
    }

    @Test
    public void testDistanceToNotInitialized() {
        var c1 = new ArrayList<Double>();
        c1.add(0.0);
        c1.add(3.0);
        c1.add(3.0);

        var p1 = new Point(1, c1);
        var p2 = new Point();

        Assert.assertThrows("Points should have same dimension", IndexOutOfBoundsException.class,() -> {
            p1.distanceTo(p2);
        });
        Assert.assertThrows("Points should have same dimension", IndexOutOfBoundsException.class,() -> {
            p2.distanceTo(p1);
        });
    }

    @Test
    public void testConstructor() {
        var coords = generateCoords(3);
        var p = new Point(29, coords);

        Assert.assertEquals(29, p.getId());
        Assert.assertEquals(3, p.getDimension());
        Assert.assertEquals(coords.get(0), p.getComponent(0));
        Assert.assertEquals(coords.get(1), p.getComponent(1));
        Assert.assertEquals(coords.get(2), p.getComponent(2));
    }

    @Test
    public void testSimpleConstructor() {
        var p = new Point();

        Assert.assertEquals(0, p.getId());
        Assert.assertEquals(0, p.getDimension());
    }

    @Test
    public void testEqualsTo() {
        var coords = generateCoords(3);
        var p1 = new Point(29, coords);
        var p2 = new Point(99, coords);

        Assert.assertTrue(p1.equalsTo(p2));
        Assert.assertTrue(p2.equalsTo(p1));
    }

    @Test
    public void testEqualsToOnePoint() {
        var coords = generateCoords(3);
        var p1 = new Point(29, coords);

        Assert.assertTrue(p1.equalsTo(p1));
    }

    @Test
    public void testEqualsToIncorrect() {
        var coords1 = generateCoords(3);
        var p1 = new Point(29, coords1);
        var coords2 = generateCoords(3);
        coords2.set(1, 123.0);
        var p2 = new Point(99, coords2);

        Assert.assertFalse(p1.equalsTo(p2));
        Assert.assertFalse(p2.equalsTo(p1));
    }

    @Test
    public void testEqualsToWrongDimension() {
        var coords1 = generateCoords(3);
        var p1 = new Point(29, coords1);
        var coords2 = generateCoords(4);
        var p2 = new Point(99, coords2);

        Assert.assertFalse(p1.equalsTo(p2));
        Assert.assertFalse(p2.equalsTo(p1));
    }

    @Test
    public void testToString() {
        var coords = generateCoords(3);
        var p1 = new Point(29, coords);
        var res = new StringBuilder();
        for (var c: coords) {
            res.append(c.toString()).append(" ");
        }

        Assert.assertEquals(res.toString(), p1.toString());
    }
}
